import axios from "axios";
import { store } from "./public";
import { setLpc } from "./redux/lpc";
import { setAuth } from "./redux/user";
console.log('set axios');

axios.interceptors.response.use((response) => {
    axios.defaults.headers.common['X-CSRF-TOKEN'] = response.headers['tokencsrf'];
    axios.defaults.responseType = 'json';
    let val = response.data.lpc;
    let valFormat = val.toFixed(4);
    console.log('val', val, valFormat);
    store.dispatch(setLpc(valFormat));
    store.dispatch(setAuth(response.data.guest));
    return response;
});


axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
const token = document.head.querySelector('meta[name="csrf-token"]');
if (token) {
    axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
    console.error("CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token");
}


export default axios;
