import React, { PureComponent } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { logout } from "../redux/user";

class ButtonExit extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      guest: null
    };
    this.click = this.click.bind(this);
  }

  click() {
    this.props.logout();
  }

  componentWillUnmount() {
    window.removeEventListener("click", this.click, false);
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //     return nextProps.user.guest !== this.state.user.guest;
  // }

  render() {
    if (this.props.guest === true || this.props.guest === null) {
      return false;
    }

    return (
      <div>
        <button onClick={this.click} type="button" className="btn btn-outline-light">
          Выход
        </button>
      </div>
    );
  }
}

ButtonExit.propTypes = {
  guest: PropTypes.bool,
  logout: PropTypes.func
};

const mapStateToProps = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch(logout())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ButtonExit);
