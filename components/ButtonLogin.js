import * as React from "react";
import { Link } from "react-router-dom";

class ButtonLogin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      guest: null
    };
  }

  render() {
    console.log("button login props", this.props);
    if (window.location.pathname.indexOf("/login") !== -1) {
      return false;
    }

    return (
      <div>
        <Link className="btn btn-outline-light" to="/login">
          Войти
        </Link>
      </div>
    );
  }
}

export default ButtonLogin;
