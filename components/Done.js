import * as React from 'react';
import './Done.scss';

export default function Done(props) {
    return (
        <div className='wrap-done'>
            <svg xmlns="http://www.w3.org/2000/svg" width="50" height="40" viewBox="0 0 24 24">
                <path fill="none" d="M0 0h24v24H0z" />
                <path id='done' stroke="#0288d1" strokeWidth='2' fill='transparent' d="M5 13 L10 19 L22 4" />
            </svg>
        </div>
    );
}
