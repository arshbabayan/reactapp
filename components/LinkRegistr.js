import * as React from "react";
import { Link } from "react-router-dom";

class LinkRegistr extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    if (window.location.pathname.indexOf("/register") !== -1) {
      return false;
    }

    return (
      <div id="LinkRegistr" className="mr-3">
        <Link className="text-light" to="/register">
          Регистрация
        </Link>
      </div>
    );
  }
}

export default LinkRegistr;
