import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

class Lpc extends Component {
  render() {
    return (
      <div id="lpc-count">
        <Link to="/cabinet/payment_history">{this.props.lpc} LPC</Link>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    lpc: state.lpc.val
  };
};

export default connect(mapStateToProps)(Lpc);
