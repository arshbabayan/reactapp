import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Mining.scss";
import axios from "../axios";
import { Process } from "../helpers/types";

class Mining extends Component {
  constructor(props) {
    super(props);
    this.state = {
      val: "0.0000000",
      loading: null
    };
    this.getLpcMining = this.getLpcMining.bind(this);
  }

  getLpcMining() {
    if (this.state.loading === null) {
      this.setState({ loading: Process.Start });
    }
    return axios.post("/lpc/mining").then(data => {
      if (data.data.mining > 0) {
        this.setState(state => {
          state.val = data.data.mining;
          if (state.loading === Process.Start) {
            state.loading = Process.Success;
          }
          return state;
        });
      }
    });
  }

  componentDidMount() {
    clearInterval(this.interval);
    clearInterval(this.interval2);
    this.getLpcMining()
      .then(() => {
        if (this.interval2 === undefined) {
          console.log('start interval');
          this.interval2 = setInterval(() => {
            this.setState(state => {
              let val = +state.val;
              let diff = 0.0001 / 600;
              val = val + diff;
              val = (Math.round(val * 10e7) / 10e7).toFixed(7);
              state.val = val;
              return state;
            });
          }, 100);
        }
      })
      .then(() => {
        this.interval = setInterval(this.getLpcMining, 5 * 60 * 1000);
      });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
    clearInterval(this.interval2);
  }

  render() {
    return <div id="mining">{this.state.val}</div>;
  }
}

Mining.propTypes = {
  guest: PropTypes.bool
};

export default Mining;
