import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <>{this.props.children}</>;
  }
}

const mapState2Props = state => {
  return {
    loading: state.user.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

Page.propTypes = {
  children: PropTypes.node
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(Page);
