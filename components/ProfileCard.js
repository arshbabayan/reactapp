import React, { Component } from "react";
import PropTypes from "prop-types";
import Card from "../ui/Card";
import { setPathImg } from "../helpers/functions";
import { Link } from "react-router-dom";
import { profileEdit } from "../moduleSocNet/redux/profileEdit";

class ProfileCard extends Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.msg = this.msg.bind(this);
  }

  msg() {
    this.props.msg(this.props.id);
  }

  removeFromFriends() {
    this.props.removeFromFriends(this.props.id);
  }

  render() {
    return (
      <Card>
        <div className="row">
          <div className="col-3">
            <div className="profile-wrap">
              <img src={setPathImg(this.props.img)} />
            </div>
          </div>
          <div className="col-9">
            <div>
              <Link to={"/profile/info/" + this.props.id}>
                <h4>{this.props.title}</h4>
              </Link>
              <div className="profile-type">{this.props.type}</div>
            </div>

            {this.props.info &&
              this.props.info.map((item, index) => {
                return (
                  <div className="row" key={index}>
                    <div className="col">{item.title}</div>
                    <div className="col">{item.value}</div>
                  </div>
                );
              })}

            <div className="mt-3">
              <button onClick={this.msg} className="btn btn-primary">
                Написать сообщение
              </button>
              <button onClick={this.removeFromFriends} className="btn btn-primary ml-3">
                Убрать из друзей
              </button>
            </div>
          </div>
        </div>
      </Card>
    );
  }
}

ProfileCard.defaultProps = {
  img: '/profile.png'
};

ProfileCard.propTypes = {
  title: PropTypes.string,
  info: PropTypes.arrayOf(PropTypes.object),
  type: PropTypes.string,
  img: PropTypes.string,
  removeFromFriends: PropTypes.func,
  msg: PropTypes.func,
  id: PropTypes.number
};

export default ProfileCard;
