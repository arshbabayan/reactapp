import * as React from 'react';
import { connect } from 'react-redux';
import InputFile from '../../ui/InputFile';
import Loader from '../../ui/Loader';
import { setPathImg } from '../../helpers/functions';
import { isNull } from 'util';
import { uploadImg } from '../../moduleSocNet/redux/profile';
import {Process} from "../../helpers/types";

class FormAvatar extends React.Component {
    constructor(props) {
        super(props);
        this.fileChange = this.fileChange.bind(this);
        this.upload = this.upload.bind(this);
    }

    upload(e) {
        e.preventDefault();
        console.log('upload avatar');
        const { loadingAvatar } = this.props;
        if (loadingAvatar === Process.Start) {
            return false;
        }

        let data = document.getElementById('avatar_input').files[0];
        this.props.uploadAvatar(data);
    }

    fileChange(data) {
        this.setState({ fileName: data });
    }

    render() {
        let avatar = isNull(this.props.avatar) || this.props.avatar === '' ? setPathImg('/profile.png') : this.props.avatar;
        console.log('avatar', avatar);
        return (
            <form ref={form => this.formUpload = form} onSubmit={this.upload}>
                <div className='row mt-3 mb-3'>
                    <div className='col-md-4 text-right'>
                        {
                            this.props.loadingAvatar === Process.Start || this.props.loading === Process.Start ?
                                <Loader width={120} /> :
                                <div className='img-profile-wrap'>
                                    <img className='img-profile' src={avatar} />
                                </div>
                        }

                    </div>
                    <div className='col-md-4'>
                        <InputFile id='avatar_input' name='avatar' change={this.fileChange} />
                        <div className='mt-3'>
                            <button className='btn btn-primary'>Сохранить</button>
                        </div>

                    </div>
                </div>
            </form>
        );
    }
}

const mapState2Props = (state) => {
    return {
        loadingAvatar: state.profile.loadingAvatar,
        loading: state.profileEdit.loading,
        avatar: state.profileEdit.avatar
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        uploadAvatar: (data) => dispatch(uploadImg(data))
    };
};

export default connect(mapState2Props, mapDispatchToProps)(FormAvatar);
