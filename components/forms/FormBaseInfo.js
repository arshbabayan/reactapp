import * as React from 'react';
import { connect } from 'react-redux';
import { isNull } from 'util';
import Input from '../../ui/Input';
import Done from '../Done';
import Loader from '../../ui/Loader';
import InputDate from '../../ui/InputDate';
import TextArea from '../../ui/TextArea';
import { getDataForm } from '../../helpers/functions';
import { saveBaseInfo, clearBaseInfo } from '../../moduleSocNet/redux/profileEdit';
import {Process} from "../../helpers/types";

class FormBaseInfo extends React.Component {

    constructor(props) {
        super(props);
        this.baseInputChange = this.baseInputChange.bind(this);
        this.submitFormBaseInfo = this.submitFormBaseInfo.bind(this);
    }

    submitFormBaseInfo(e) {
        e.preventDefault();
        let data = getDataForm(this.formBaseInfo);
        console.log('data save base info', data);
        this.props.saveBaseInfo(data);
    }

    baseInputChange() {
        this.props.clearLoading();
    }

    componentWillUnmount() {
        this.props.clearLoading();
    }

    render() {
        if (this.props.fields.length === 0) {
            return null;
        }
        return (
            <form className='mx-auto mt-4' ref={form => this.formBaseInfo = form} onSubmit={this.submitFormBaseInfo}>
                {
                    this.props.fields.map((field, index) => {
                        let value = field.value === null ? '' : field.value;
                        if (field.name === 'login') {
                            value = this.props.user.name;
                        }
                        if (field.name === 'email') {
                            value = this.props.user.email;
                        }

                        let filter = {};
                        if (!isNull(field.filter) && field.filter !== '') {
                            filter = JSON.parse(field.filter);
                        }

                        let attr = {};
                        if (!isNull(field.attr) && field.attr !== '') {
                            attr = JSON.parse(field.attr);
                        }
                        if (field.type === 'string') {
                            return (
                                <Input change={this.baseInputChange} key={index}
                                    filter={filter}
                                    {...attr} label={field.caption} name={field.name}
                                    id={field.name} value={value} />
                            );
                        }

                        if (field.type === 'textarea') {
                            return (
                                <TextArea key={index} label={field.caption}
                                    name={field.name} id={field.name}
                                    filter={filter} {...attr}
                                    value={value} />
                            );
                        }

                        if (field.type === 'date') {
                            return (
                                <InputDate key={index} label={field.caption}
                                    name={field.name} id={field.name}
                                    filter={filter} {...attr}
                                    value={value} />
                            );
                        }
                    })
                }
                <div className='col-md-6 offset-md-4'>
                    {this.props.loadingSave !== Process.Start &&
                        <button className='btn btn-primary' type='submit'>Сохранить</button>
                    }
                    {this.props.loadingSave === Process.Start &&
                        <Loader width={40} />
                    }
                    {
                        this.props.loadingSave === Process.Success &&
                        <Done />
                    }
                </div>
            </form>
        );
    }
}

const mapState2Props = (state) => {
    return {
        loadingSave: state.profileEdit.saveBaseInfo.loading,
        fields: state.profileEdit.fields,
        user: state.profileEdit.user
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveBaseInfo: (data) => { dispatch(saveBaseInfo(data)); },
        clearLoading: () => { dispatch(clearBaseInfo()); }
    };
};

export default connect(mapState2Props, mapDispatchToProps)(FormBaseInfo);
