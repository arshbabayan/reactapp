import * as React from 'react';
import { connect } from 'react-redux';
import { saveFieldsContact, clearLoading } from '../../moduleSocNet/redux/profile';
import { isNull } from 'util';
import { getDataForm } from '../../helpers/functions';
import TextArea from '../../ui/TextArea';
import Input from '../../ui/Input';
import Done from '../Done';
import Loader from '../../ui/Loader';
import {Process} from "../../helpers/types";

class FormContact extends React.Component {
    constructor(props) {
        super(props);
        this.baseInputChange = this.baseInputChange.bind(this);
        this.submitContactInfo = this.submitContactInfo.bind(this);
    }
    fileChange(data) {
        this.setState({ fileName: data });
    }

    submitContactInfo(e) {
        e.preventDefault();
        let data = getDataForm(this.formContactInfo);
        this.props.saveContactFields(data);
    }

    baseInputChange() {
        this.props.clearLoading();
    }

    render() {
        return (
            <form className='mx-auto mt-4' ref={form => this.formContactInfo = form} onSubmit={this.submitContactInfo}>
                {this.props.contactFields.map((field, index) => {
                    field.value = isNull(field.value) ? '' : field.value;
                    let attr = {};
                    if (!isNull(field.attr) && field.attr !== '') {
                        attr = JSON.parse(field.attr);
                    }
                    if (field.type === 'string') {
                        return <Input change={this.baseInputChange} key={index}
                            {...attr} label={field.caption} name={field.name}
                            id={field.name} value={field.value} />;
                    }

                    if (field.type === 'textarea') {
                        return <TextArea key={index} label={field.caption} name={field.name} id={field.name} value={field.value && ''} />;
                    }
                })}
                <div className='col-md-6 offset-md-4'>
                    <button className='btn btn-primary' type='submit'>Сохранить</button>
                    {this.props.loadingSaveContact === Process.Start &&
                        <Loader width={40} />
                    }
                    {
                        this.props.loadingSaveContact === Process.Success &&
                        <Done />
                    }
                </div>
            </form>
        );
    }
}

const mapState2Props = (state) => {
    return {
        contactFields: state.profile.contactFields,
        loadingSaveContact: state.profile.loadingSaveContact
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        saveContactFields: (data) => { dispatch(saveFieldsContact(data)); },
        clearLoading: () => { dispatch(clearLoading()); }
    };
};

export default connect(mapState2Props, mapDispatchToProps)(FormContact);
