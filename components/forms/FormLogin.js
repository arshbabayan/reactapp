import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, CardHeader, CardBody, Button, Form, Row  } from "reactstrap";
import { Link } from "react-router-dom";
import Input from "../../ui/Input";
import { getDataForm } from "../../helpers/functions";
import { loginPost, clearLoading } from "../../redux/login";
import Loader from "../../ui/Loader";
import { Process } from "../../helpers/types";
import ReCAPTCHA from "react-google-recaptcha";
import PropTypes from "prop-types";
import { Redirect } from "react-router";

class FormLogin extends Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {};
    this.onChange = this.onChange.bind(this);
  }

  submit(e) {
    e.preventDefault();
    let data = getDataForm(this.form);
    this.props.login(data);
  }

  componentDidMount() {
    this.props.clear();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("login nextProps", nextProps);
    return prevState;
  }

  onChange(value) {
    console.log("captcha value", value);
    this.setState({ captcha: value });
  }

  inputChange(name, value) {
    console.log("input", name, value);
  }

  render() {
    console.log("loading login", this.props.loadingLogin);
    if (this.props.guest === null || this.props.guest === undefined) {
      return null;
    }
    if (this.props.loadingLogin === Process.Start) {
      return <Loader />;
    }
    if (this.props.loadingLogin === Process.Success) {
      console.log("process login success");
      return <Redirect to="/cabinet" />;
    }
    return (
      <Card>
        <CardHeader>
          <strong>Вход:</strong>
        </CardHeader>
        <CardBody>
          {(this.props.loadingLogin === null || this.props.loadingLogin === Process.Error) && (
            <Form onSubmit={this.submit} innerRef={form => (this.form = form)}>
              <Input change={this.inputChange("email")} id="email" type="text" name="email" label="Email" />
              <Input change={this.inputChange("password")} id="password" type="password" name="password" label="Пароль" />
              <Row className="mb-4">
                {/* <ReCAPTCHA className="mx-auto" sitekey={recaptcha} onChange={this.onChange} /> */}
              </Row>
              <Row>
                <div className="mx-auto d-inline-box">
                  <Button color="primary">Войти</Button>
                  <Link className="ml-5" to="/resetemail">
                    Восстановить пароль
                  </Link>
                </div>
              </Row>
              <Row>
                <div className="mx-auto mt-3">
                  Нет аккаунта? <Link to="/registr">Зарегистрироваться</Link>
                </div>
              </Row>
            </Form>
          )}
        </CardBody>
      </Card>
    );
  }
}

const mapState2Props = state => {
  return {
    guest: state.user.guest,
    loading: state.login.loading,
    loadingLogin: state.login.loadingLogin
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: data => dispatch(loginPost(data)),
    clear: () => dispatch(clearLoading())
  };
};

FormLogin.propTypes = {
  loadingLogin: PropTypes.number,
  guest: PropTypes.bool,
  login: PropTypes.func
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(FormLogin);
