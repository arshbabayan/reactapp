import * as React from "react";
import { connect } from "react-redux";
import TextArea from "../../ui/TextArea";
import { messageSend, clearMessage } from "../../moduleSocNet/redux/messages";
import Loader from "../../ui/Loader";
import {Process} from "../../helpers/types";

class FormMsg extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: "",
            msg: ''
        };
        this.sendMsg = this.sendMsg.bind(this);
        this.change = this.change.bind(this);
    }

    change(e) {
        let msg = e;
        this.setState({ value: msg });
    }

    sendMsg(e) {
        e.preventDefault();
        console.log('send msg submit', this.state.value);
        if (this.state.value.length > 2) {
            this.props.sendMsg(this.state.value);
        } else {
            //
        }
    }

    render() {
        if (this.props.loading === Process.Start) {
            return <Loader />;
        }
        if (this.props.loading === Process.Success) {
            return <div className='alert alert-info'>Сообщение отправлено</div>;
        }
        return (
            <form onSubmit={this.sendMsg}>
                <TextArea required={true} rows={7} change={this.change} name="msg" id="msgInput" label="Сообщение" value={this.state.value} />
                <input type="hidden" name="recipient" value={this.props.recipient} />
                <div className='row'>
                    <div className="col-md-6 text-right">
                        <button className="btn btn-success">Отправить</button>
                    </div>
                </div>
            </form>
        );
    }
}

const mapState2Props = (state) => {
    return {
        loading: state.messages.loading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        sendMsg: (msg) => dispatch(messageSend(msg))
    };
};

export default connect(mapState2Props, mapDispatchToProps)(FormMsg);
