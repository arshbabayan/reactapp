import { createStore, applyMiddleware, compose, combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import thunk from "redux-thunk";
import { createBrowserHistory } from "history";
import { composeWithDevTools } from "redux-devtools-extension";
import { routerMiddleware } from "connected-react-router";
import reducerRegistry from "./redux/reducerRegister";

const history = createBrowserHistory();

export default function configureStore(initialState) {
  const combine = reducers => {
    const reducerNames = Object.keys(reducers);
    Object.keys(initialState).forEach(item => {
      if (reducerNames.indexOf(item) === -1) {
        reducers[item] = (state = null) => state;
      }
    });
    return combineReducers(reducers);
  };

  const reducer = combine(reducerRegistry.getReducers());
  const store = createStore(
    reducer,
    initialState,
    compose(
      composeWithDevTools(applyMiddleware(thunk)),
      applyMiddleware(routerMiddleware(history))
    )
  );
  console.log('store', store);
  reducerRegistry.setChangeListener(reducers => {
    store.replaceReducer(combine(reducers));
  });
  reducerRegistry.register("router", connectRouter(history));
  return store;
}
