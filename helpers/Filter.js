let errorInfo = {
  minStr: "Минимум %value символов",
  minNumber: "Минимум %value",
  maxStr: "Максимум %value символов",
  maxNum: "Максимум %value символов",
  mail: "Не верный почтовый адрес",
  regex: "Не верный формат строки",
  phone: "Не верный формат номера телефона",
  fb: "Не верный формат для facebook",
};

export function filter(value, f) {
  if (f === undefined) {
    return true;
  }
  for (let filtr in f) {
    switch (filtr) {
      case 'min': {
        if (typeof value === 'string' && value.length < f.min) {
          return errorInfo.minStr.replace('%value', '' + f.min);
        }
        if (typeof value === 'number' && value < f.min) {
          return errorInfo.minNumber.replace('%value', ('' + f.max));
        }
        break;
      }
      case 'max': {
        if (typeof value === 'string' && value.length > f.max) {
          return errorInfo.maxStr.replace('%value', '' + f.max);
        }
        if (typeof value === 'number' && value > f.max) {
          return errorInfo.maxNum.replace('%value', ('' + f.min));
        }
        break;
      }
      case 'mail': {
        let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (typeof value === 'number' || !re.test(value)) {
          return errorInfo.mail;
        }
        break;
      }
      case 'phone': {
        let re = /^(\+7|7|8)?[\s\-]?\(?[489][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
        if (typeof value === 'number' || !re.test(value)) {
          return errorInfo.phone ;
        }
        break;
      } //
      case 'fb': {
        let re =/^(https?:\/\/)?(www\.)?facebook.com\/[a-zA-Z0-9]/;
        if (!re.test(value)) {
          return errorInfo.regex;
        }
        break;
      }
      case 'vk': {
        let re = /^(https?:\/\/)?(www\.)?vk.com\/[a-zA-Z0-9]/;
        if (!re.test(value)) {
          return errorInfo.regex;
        }
        break;
      }
      case 'web': {
        let re =/^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
        if (!re.test(value)) {
          return errorInfo.regex;
        }
        break;
      }
      case 'regex': {

        const regexp = new RegExp(f.regex);
        const regg = /\/(.*)\/(.*)/.exec(f.regex)
        // console.log(f)
        // console.log(regexp)
        // console.log(regg[0])
        // console.log(typeof value === 'string' || !(regexp).exec(value))


        if (typeof value === 'string' || !(regexp).exec(value)) {
          return errorInfo.regex;
        }
        break;
      }
      case 'reg': {
        if (typeof value === 'number' || !(f.reg).test(value)) {
          return errorInfo.regex;
        }
        break;
      }
    }
  }
}
