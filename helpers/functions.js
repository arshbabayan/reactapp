import * as $ from 'jquery';

export function getDataForm(form) {
    let data = {};
    $(form).find('input, textarea').each(function() {
        data[this.name] = this.value;
    });
    return data;
}

export function setPath(url) {
    let pathname = window.location.origin;
    return pathname + url;
}

export function setPathImg(url){
    let path = setPath('/img');
    return path + url;
}

export function parseErrors(errorStr) {
    if (errorStr.indexOf('format is invalid')) {
        return "Не верный формат строки";
    }
}
