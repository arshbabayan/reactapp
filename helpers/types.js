export const Process = {
	Start: 0, Success: 1, Error: 2
}

export class Node {
  constructor(data) {
    this.id = data.id;
    this.parent_id = data.parent_id;
    this.lft = data.lft;
    this.rgt = data.rgt;
    this.level = data.level;
    this.data = data;
    this.child = null;
    this.left = null;
    this.right = null;
  }
}

export class Tree {
  constructor(data) {
    this.data = [];
    this._root = new Node(data);
    this._root.col = 0;
    this._root.row = 0;
    this._current = this._root;
    this.data = {};
    this.data[data.id] = this._root;
  }

  nextLeft() {
    if (this._current.left !== null) {
      this._current = this._current.left;
      return this._current;
    }
    return false;
  }

  nextRight() {
    if (this._current.right !== null) {
      this._current = this._current.right;
      return this._current;
    }
    return false;
  }

  add(data) {
    let node = new Node(data);
    if (node.parent_id in this.data) {
      let parentNode = this.data[node.parent_id];
      if (parentNode.left === null) {
        parentNode.left = node;
        node.col = parentNode.col * 2;
        node.row = parentNode.row + 1;
        this.data[node.id] = node;
      } else if(parentNode.right === null) {
        parentNode.right = node;
        node.col = parentNode.col * 2 + 1;
        node.row = parentNode.row + 1;
        this.data[node.id] = node;
      } else {
        return false;
      }
    }
  }

}
