import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';

const withAuth = (WrappedComponent) => {
    class Hoc extends React.Component {
        constructor(props) {
            super(props);
            this.state = { ...props };
        }

        // componentDidMount() {
        //     console.log('with auth props', this.props);
        // }

        // shouldComponentUpdate(nextProps, nextState) {
        //     return nextProps.guest !== this.props.guest;
        // }

        static getDerivedStateFromProps(nextProps, prevState) {
            return nextProps;
        }

        render() {
            console.log('with auth state', this.state);
            if (this.props.guest === true) {
                return <Redirect to='/login' />;
            }
            return (
                <WrappedComponent {...this.state} />
            );
        }
    }

    return Hoc;
};

export default withAuth;
