import * as React from 'react';
import './Footer.scss';


export default class Footer extends React.Component {
    render() {
        return (
            <footer>
                <div className="container">
                    <div className='row'>
                        <div className="col-md-4">
                            <div className="text-center">
                                <div className='font-uppercase'>Соц. сети:</div>
                                <div><a href='#'>Twitter</a></div>
                                <div><a href='#'>Facebook</a></div>
                                <div><a href='#'>Telegram</a></div>
                                <div><a href='#'>Instagram</a></div>
                                <div><a href='#'>Vkontakte</a></div>
                                <div><a href='#'>Medium</a></div>
                                <div><a href='#'>Golos</a></div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="text-center">
                                <div className='font-uppercase'>Документы:</div>
                                <div><a href="#">Белая бумага</a></div>
                                <div><a href="#">Дорожная карта</a></div>
                                <div><a href="#">Промо компании</a></div>
                                <div><a href="#">Тема на Bitcointalk</a></div>
                            </div>
                        </div>
                        <div className="col-md-4">
                            <div className="text-center">
                                <div className='font-uppercase'>Ссылки</div>
                                <div><a href="#">Соглашение</a></div>
                                <div><a href="#">Политика конфиденциальности</a></div>
                                <div><a href="#">Новости</a></div>
                                <div><a href="#">Регистрация в социальной сети</a></div>
                                <div><a href="#">Бизнес кабинет</a></div>
                                <div><a href="#">Реферальная ссылка</a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        );
    }
}
