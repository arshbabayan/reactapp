import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import ButtonMenu from "../ui/ButtonMenu";
import "./Header.scss";
import { setPath } from "../helpers/functions";
import ButtonExit from "../components/ButtonExit";
import ButtonLogin from "../components/ButtonLogin";
import { Link } from "react-router-dom";
import LinkRegistr from "../components/LinkRegistr";
import axios from "../axios";
import Mining from "../components/Mining";
import Lpc from "../components/Lpc";

class Header extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  logout() {
    axios.post("/logout").then(data => console.log("logout", data.data));
  }

  componentWillUnmount() {
    console.log("header unmount");
  }

  static getDerivedStateFromProps() {
    return false;
  }

  render() {
    console.log("header props", this.props);
    return (
      <header className="navbar navbar-expand-lg navbar-dark bg-primary mb-5 bd-navbar">
        {this.props.path !== "/" && <ButtonMenu />}
        <div id="logo_wrap">
          <Link to="/">
            <img src={setPath("/img/logo.png")} />
          </Link>
        </div>
        <div className="collapse navbar-collapse" id="navbarText">
          <ul className="navbar-nav mr-auto">
            {!this.props.guest && (
              <>
                <li className="nav-item active">
                  <Link className="nav-link" to="/socnet/news">
                    Соц. сеть <span className="sr-only">(current)</span>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/cabinet">
                    Бизнес кабинет
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">
                    Рекламный кабинет
                  </a>
                </li>
              </>
            )}
            <li className="nav-item">
              <a className="nav-link" href="#">
                Магазин
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="#">
                Блог
              </a>
            </li>
          </ul>
          <span className="navbar-text" />
          {this.props.guest === false && (
            <>
              <Lpc />
              {false && <Mining />}
              <ButtonExit />
            </>
          )}
          {this.props.guest && (
            <>
              <LinkRegistr />
              <ButtonLogin />
            </>
          )}
        </div>
      </header>
    );
  }
}

Header.propTypes = {
  path: PropTypes.string,
  guest: PropTypes.bool
};

const mapStateToProps = state => {
  return {
    guest: state.user.guest,
    path: state.router.location.pathname
  };
};

export default connect(mapStateToProps)(Header);
