import * as React from "react";
import { connect } from "react-redux";
import MenuItemHeader from "../ui/accordeonMenu/MenuItemHeader";
import MenuItemsContainer from "../ui/accordeonMenu/MenuItemsContainer";
import { Link } from "react-router-dom";
import "./LeftMenu.scss";

class LeftMenu extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    console.log('left menu props guest', this.props.guest);
    return (
      <div id="left-menu" className="active">
        <div className="left-menu-overflow">
          <div id="accordion">
            {/* <div className=''>
                            <Link to='/cabinet/reflinks'>
                                <i className={'oi oi-person'}></i>
                                <span>Реферальные ссылки</span>
                            </Link>
                        </div> */}
            {!this.props.guest && (
              <>
                <MenuItemHeader icon="person" id="1" title="Кабинет" />
                <MenuItemsContainer id="1">
                  <Link to="/cabinet">
                    <i className={"oi oi-person"} />
                    <span>Бизнес кабинет</span>
                  </Link>
                  <Link to="/cabinet/investor">
                    <i className="fas fa-shopping-cart" />
                    <span>Купить токены LPC</span>
                  </Link>
                  <Link to="/cabinet/income">
                    <i className="fas fa-piggy-bank" />
                    <span>Финансовый кабинет</span>
                  </Link>
                  <Link to="/cabinet/team">
                    <i className="fas fa-sitemap" />
                    <span>Моя команда</span>
                  </Link>
                  <Link to="/cabinet/binar">
                    <i className="fas fa-campground" />
                    <span>Старт - супербинар</span>
                  </Link>
                  <Link to="/cabinet/vip">
                    <i className="fas fa-chess-rook" />
                    <span>VIP - мегабинар</span>
                  </Link>
                  <Link to="/cabinet/reflinks">
                    <i className={"fas fa-retweet"} />
                    <span>Реферальные ссылки</span>
                  </Link>
                  <Link to="#">Ссылка2</Link>
                  <Link to="#">Ссылка3</Link>
                </MenuItemsContainer>
              </>
            )}
            <div>
              <Link to="/socnet/profile/edit">
                <i className={"oi oi-person"} />Профиль
              </Link>
            </div>
            <MenuItemHeader icon="person" id="2" title="Общие настройки" />
            <MenuItemsContainer id="2">
              <Link to="#">Ссылка1</Link>
              <Link to="#">Ссылка2</Link>
              <Link to="#">Ссылка3</Link>
            </MenuItemsContainer>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    guest: state.user.guest
  };
};

export default connect(mapStateToProps)(LeftMenu);
