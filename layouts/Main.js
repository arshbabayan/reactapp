import * as React from "react";
import * as ReactDOM from "react-dom";
import Header from "./Header";
import LeftMenu from "./LeftMenu";
import './Main.scss';

export default class Main extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <Header />
                <LeftMenu />
                <div className='container mainContainer' id={this.props.id}>
                    {this.props.children}
                </div>
            </div>
        );
    }
}
