import * as React from "react";
import * as ReactDOM from "react-dom";
import Header from "./Header";

export default class MainAdmin extends React.Component<any, any> {
    constructor(props) {
        super(props);
    }

    render() {

        return (
            <div>
                <div className="container">
                    {this.props.children}
                </div>
            </div>
            );
    }
}
