import * as React from "react";
import {Route} from "react-router-dom";
import Main from './pages/Main';
import Forms from './pages/Forms';
import Form from './pages/Form';
import Users from './pages/users/Users';
import User from './pages/users/User';
import CreateUser from './pages/users/CreateUser';
import EditUser from './pages/users/EditUser';
import ListOfLogs from './pages/settings/ListOfLogs';

export default class IndexComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
      console.log('admin did mount');
  }

  render() {
    return (
      <>
        <Route path="/admin/main" component={Main}/>
        <Route path="/admin/forms/:id" component={Form}/>
        <Route path="/admin/forms" component={Forms}/>
        <Route path="/admin/users-list" component={Users}/>
        <Route path="/admin/users/user/:id" component={User}/>
        <Route path="/admin/users/create-user" component={CreateUser}/>
        <Route path="/admin/users/edit-user/:id" component={EditUser}/>
        <Route path="/admin/settings/list-of-logs" component={ListOfLogs}/>
      </>
    );
  }
}
