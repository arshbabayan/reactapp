import * as React from 'react';
import {Header} from './Header';
import LeftMenu from './LeftMenu';
import './Base.scss';

export default class Base extends React.Component {
  componentDidMount() {
    console.log('admin base component did mount');
  }

  render() {
    return (
      <>
        <Header/>
        <LeftMenu/>
        <div className='container mainContainer  toggledContainer' id={this.props.id}>
          <div className={`font-weight-bold text-truncate m-0 `}>{this.props.title}</div>
          {this.props.children}
        </div>
        <footer className="footer-wrap mt-5 mb-5 mt-5 mb-5 base-footer toggledFooter">© 2018 LeoPays. Все права защищены.</footer>
      </>
    );
  }
}
