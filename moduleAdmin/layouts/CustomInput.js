import * as React from "react";
import filter, {filter as f} from "../../helpers/Filter";

import {setFormIsValid, setAvatar} from "../redux/fields"
import {connect} from "react-redux";

class CustomInput extends React.Component {
  constructor() {
    super();
    this.state = {
      error: "",
      borderError: "",
      file: "",
      imagePreviewUrl: "",
      errorsCount: 0
    }
    this.onBlur = this.onBlur.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  onBlur(e) {
    let {filter} = this.props;
    if (filter !== undefined) {
      let error = f(e.target.value, filter);
      if (error === undefined && this.state.error === "")
        this.props.setFormIsValid(true)
      if (typeof error === "string") {
        this.setState({error: error, borderError: "border border-danger"});
        this.props.setFormIsValid(false)
      }
    }
  }

  onChange(event) {
    this.props.onChange(event);
    let {filter} = this.props;
    let reader = new FileReader();
    let file;
    if (filter !== undefined) {
      let error = f(event.target.value, filter);
      if (error === undefined && this.state.error === "")
        this.props.setFormIsValid(true)
    }
    if (event.target.files) {
      file = event.target.files[0];
      reader.onloadend = () => {
        this.setState({
          file: file,
          imagePreviewUrl: reader.result
        });
      }
      if (file) {
        reader.readAsDataURL(file)
        this.props.setAvatar(file)

      }
    }
    this.setState({error: "", borderError: ""})
    // this.props.setFormIsValid(true)
  }

  render() {
    const {id, label, type, value, name} = this.props;
    let {imagePreviewUrl, error, borderError} = this.state;

    return (
      <div className="form-group row">
        <label htmlFor={id}
               className="col-md-3 col-lg-4 col-form-label font-weight-bold">{label}</label>
        <div className="col-md-9 col-lg-8">
          {
            type === "file" && <div className="avatar-wrap mb-2">
              {
                imagePreviewUrl && <img alt="avatar" id="avatarr" className="avatar-img mt-2" src={imagePreviewUrl}/>
              }

            </div>
          }
          <div className={`${type === "phone" ? "input-group" : ""}`}>
            {
              type === "phone" && <div className="input-group-prepend">
                <button className="btn btn-outline-secondary dropdown-toggle btn-sm" type="button"
                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">+7
                </button>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#">+7</a>
                </div>
              </div>
            }
            <input
              className={`${type === "file" ? "form-control-file" :
                "form-control form-control-sm"} ${borderError}`}
              type={type} id={id}
              name={name}
              value={value}
              // defaultValue="some value"
              onChange={this.onChange}
              onBlur={this.onBlur}
              accept={`${type === "file" ? "image/*" : ""}`} //
            />
          </div>
          <div className="form-text text-danger">{error}</div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    fields: state.fields.isFormValid,
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    setFormIsValid: (isFormValid) => {
      dispatch(setFormIsValid(isFormValid))
    },
    setAvatar: (avatar) => {
      dispatch(setAvatar(avatar))
    },
  }

};

export default connect(mapStateToProps, mapDispatchToProps)(CustomInput);
