import * as React from 'react';
import { Link } from 'react-router-dom';
import ButtonExit from '../../components/ButtonExit';
import ButtonLogin from '../../components/ButtonLogin';
import ButtonMenu from '../../ui/ButtonMenu';
import { setPath } from '../../helpers/functions';
import './Header.scss';

export class Header extends React.Component {
    render() {
        return (
            <header className="navbar navbar-expand-lg navbar-dark bd-navbar">
                {/* <a className="navbar-brand" href="#">Navbar</a> */}
                {/* <button className="navbar-toggler hidden-lg-up" type="button" data-toggle="collapse"
                    data-target="#collapsibleNavId" aria-controls="collapsibleNavId"
                    aria-expanded="false" aria-label="Toggle navigation"></button> */}
                <ButtonMenu />
                <div id='logo_wrap'><Link to='/'><img src={setPath('/img/logo.png')} /></Link></div>
                <div className="collapse navbar-collapse" id="navbarText">
                    <ul className="navbar-nav mr-auto">
                        <li className="nav-item active">
                            <Link className="nav-link" to='/news'>Соц. сеть
                              <span className="sr-only">(current)</span>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Бизнес кабинет</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Рекламный кабинет</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Магазин</a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Блог</a>
                        </li>
                    </ul>
                    <span className="navbar-text">

                    </span>
                    <ButtonExit />
                    <ButtonLogin />
                </div>
            </header>);
    }
}
