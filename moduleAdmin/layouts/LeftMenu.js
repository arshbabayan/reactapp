import * as React from 'react';
import {Link} from 'react-router-dom';

import MenuItemHeader from '../../ui/accordeonMenu/MenuItemHeader';
import MenuItemsContainer from '../../ui/accordeonMenu/MenuItemsContainer';
import './LeftMenu.scss';


export default class LeftMenu extends React.Component {

  render() {
    return (
      <div className="module-admin-left-menu">
        <div id='left-menu' className='active'>
          <div className='left-menu-overflow'>
            <div id="accordion">
              <MenuItemHeader icon='fas fa-chart-bar fa-sm' id='stat' title='Статистика'/>
              <MenuItemsContainer id='stat'>
                <Link to='#'>Общая статистика</Link>
                <Link to='#'>Социальная сеть (мониторинг)</Link>
                <Link to='#'>Биржа</Link>
                <Link to='#'>Бизнес кабинет (статистика из бизнес кабинета)</Link>
                <Link to='#'>Рекламный кабинет</Link>
                <Link to='#'>Аукцион</Link>
                <Link to='#'>Интернет магазин</Link>
              </MenuItemsContainer>

              <MenuItemHeader icon='fas fa-cogs fa-sm' id='settings' title='Настройки'/>
              <MenuItemsContainer id='settings'>
                <Link to='#'>Основные настройки</Link>
                <Link to='#'>Профиль</Link>
                <Link to='#'>Социальная сеть</Link>
                <Link to='#'>Рекламный кабинет</Link>
                <Link to='#'>Биржа</Link>
                <Link to='#'>Аукцион</Link>
                <Link to='#'>Интернет-магазин</Link>
                <Link to='#'>Платежная информация</Link>
                <Link to='#'>Платежная приватности</Link>
                <Link to='#'>Уведомления</Link>
                <Link to='#'>Категории</Link>
                <Link to='#'>Медиа менеджер</Link>
                <Link to='/admin/settings/list-of-logs'>Логи</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-user-friends fa-sm' id='users' title='Пользователи'/>
              <MenuItemsContainer id='users'>
                <Link to='#'>Группы пользователе</Link>
                <Link to='/admin/users-list'>Список пользователей</Link>
                <Link to='#'>Добавить | Редактировать группу</Link>
                <Link to='/admin/users/create-user'>Добавить | Редактировать пользователя</Link>
                <Link to='#'>Настройки доступа</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-wrench fa-sm' id='tools' title='Инструменты платформы'/>
              <MenuItemsContainer id='tools'>
                <Link to='#'>Хостинг</Link>
                <Link to='#'>Авто Instagram</Link>
                <Link to='#'>Авто Facebook</Link>
                <Link to='#'>Постинг Facebook</Link>
                <Link to='#'>Email автореспондер</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-users fa-sm' id='soc' title='Социальная сеть'/>
              <MenuItemsContainer id='soc'>
                <Link to='#'>Жалобы (всевозможные жалобы пользователей)</Link>
                <Link to='#'>Публикации (общая лента)</Link>
                <Link to='#'>Комментарии</Link>
                <Link to='#'>Группы (список)</Link>
                <Link to='#'>Мероприятия (список)</Link>
                <Link to='#'>Фото (загруженные пользователями, не относятся к медиа-файлам)</Link>
                <Link to='#'>Видео (добавленные пользователями, не относятся к медиа-файлам)</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-mug-hot fa-sm' id='relaxation' title='Развлечения | отдых'/>
              <MenuItemsContainer id='relaxation'>
                <Link to='#'>Игры на токен LPC</Link>
                <Link to='#'>Стимулирующая лотерея</Link>
                <Link to='#'>Игры онлайн</Link>
                <Link to='#'>Онлайн кинотеатр</Link>
                <Link to='#'>Книги онлайн</Link>
                <Link to='#'>Музыка</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-user-tie fa-sm' id='bizkab' title='Бизнес кабинет'/>
              <MenuItemsContainer id='bizkab'>
                <Link to='#'>Тарифы пользователей</Link>
                <Link to='#'>Реферальные привязки</Link>
                <Link to='#'>Заявки на покупку LPC</Link>
                <Link to='#'>Заявки на вывод (объединены заявки на вывод и заявки ICO)</Link>
                <Link to='#'>Контракты</Link>
                <Link to='#'>Стоплист</Link>
                <Link to='#'>Бонусы</Link>
                <Link to='#'>Промоушен</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-magic fa-sm' id='promo' title='Промо | Конкурсы'/>
              <MenuItemsContainer id='promo'>
                <Link to='#'>Основной промоушен</Link>
                <Link to='#'>Лидер недели</Link>
                <Link to='#'>Еженедельный конкурс</Link>
              </MenuItemsContainer>
              <MenuItemHeader icon='fas fa-money-bill-alt fa-sm' id='exchange' title='Биржа'/>
              <MenuItemsContainer id='exchange'>
                <Link to='#'>История торговли</Link>
                <Link to='#'>Статистика кошельков</Link>
                <Link to='#'>Ввод / Вывод</Link>
                <Link to='#'>Заявки на замену кошельков</Link>
              </MenuItemsContainer>
              <div>
                <Link to='#'><i className='fas fa-hands-helping fa-sm'></i>Рекламный кабинет</Link>
              </div>

              <MenuItemHeader icon='fas fa-shopping-basket fa-sm' id='inetstore' title='Интернет-магазин'/>
              <MenuItemsContainer id='inetstore'>
                <Link to='#'>Покупатели</Link>
                <Link to='#'>Претензии</Link>
                <Link to='#'>Отзывы</Link>
                <Link to='#'>Заказы на доставку</Link>
              </MenuItemsContainer>
              <div>
                <Link to='#'><i className='fas fa-percentage fa-sm'></i>Аукцион</Link>
              </div>

              <div>
                <Link to='#'><i className='fas fa-plus-square fa-sm'></i>Медиа менеджер</Link>
              </div>

              <div>
                <Link to='#'><i className='fas fa-headset fa-sm'></i>Техническая поддержка</Link>
              </div>


              <div>
                <Link to='#'><i className='fas fa-envelope fa-sm'></i>Шаблоны E-mail сообщений</Link>
              </div>
              <div>
                <Link to='#'><i className='fas fa-laptop fa-sm'></i>API</Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
