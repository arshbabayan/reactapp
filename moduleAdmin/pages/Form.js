import * as React from "react";
import { connect } from "react-redux";
import Base from "../layouts/Base";
import { getFormFields } from "../redux/forms";
import { RouteComponentProps } from "react-router";
import Switch from "../../ui/Switch";
import { Link } from "react-router-dom";
import {Process} from "../../helpers/types";


class Form extends React.Component{
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        const { id } = this.props.match.params;
        this.props.getFormFields(+id);
    }

    // static getDerivedStateFromProps(nextProps: FormProps, prevState) {
    //     console.log('form fields nextprops', nextProps);
    //     return false;
    // }

    render() {
        console.log('forms', this.props.forms);
        let form = this.props.forms.filter((form) => form.id === this.props.match.params.id);
        console.log('form', form);
        return (
            <Base id="form-edit">
                <h1>
                    Редактирование формы: <span className="test-secondary" >{form.title}</span>
                </h1>
                {this.props.loading === Process.Success && this.props.fields !== null && (
                    <table className="table mt-3">
                        <thead>
                            <tr>
                                <th>Алиас</th>
                                <th>Заголовок</th>
                                <th>Тип</th>
                                <th>Отключить</th>
                                <th />
                                <th />
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.fields.map((field, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{field.name}</td>
                                        <td>{field.caption}</td>
                                        <td>{field.type}</td>
                                        <td>
                                            <Switch checked={field.active === 1 ? true : false} />
                                        </td>
                                        <td>
                                            <Link title="Редактировать" className="btn btn-primary btn-sm" to={"/forms/fields/" + field.id}>
                                                <i className="fas fa-edit text-light" />
                                            </Link>
                                        </td>
                                        <td>
                                            <button title="Удалить" className="btn btn-danger btn-sm">
                                                <i className="fas fa-trash text-light" />
                                            </button>
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                )}
            </Base>
        );
    }
}

const mapState2Props = (state) => {
    return {
        forms: state.forms.forms,
        fields: state.forms.fields,
        loading: state.forms.loading
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getFormFields: (id) => dispatch(getFormFields(id))
    };
};

export default connect(
    mapState2Props,
    mapDispatchToProps
)(Form);
