import * as React from "react";
import { connect } from "react-redux";

import { getForms } from "../redux/forms";
import Base from "../layouts/Base";
import Loader from "../../ui/Loader";
import { Link } from "react-router-dom";
import {Process} from "../../helpers/types";

class Forms extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        console.log("forms props", this.props);
        // this.props.getForms();
    }

    render() {
        if (this.props.loading === Process.Start) {
            return <Loader />;
        }
        if (this.props.loading === Process.Success) {
            return (
                <Base id="forms">
                    <h1>Формы</h1>
                    <table className="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Алиас</th>
                                <th>Заголовок</th>
                                <th>Дата создания</th>
                                <th />
                                <th />
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.forms.map((item, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{item.id}</td>
                                        <td>{item.name}</td>
                                        <td>{item.title}</td>
                                        <td>{item.created_at}</td>
                                        <td>
                                            <Link title='Редактировать' className='btn btn-primary btn-sm' to={'/forms/' + item.id}>
                                                <i className='fas fa-edit text-light'></i>
                                            </Link>
                                        </td>
                                        <td>
                                            <button title='Удалить' className='btn btn-danger btn-sm'>
                                                <i className="fas fa-trash text-light"></i>
                                            </button>
                                        </td>
                                    </tr>
                                );
                            })}
                        </tbody>
                    </table>
                </Base>
            );
        }
        return null;
    }
}

Forms.defaultProps = {

}

const mapState2Props = (state) => {
    return {
        guest: state.user.props,
        loading: state['admin.forms'].loading,
        forms: state['admin.forms'].forms
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getForms: dispatch(getForms())
    };
};

export default connect(
    mapState2Props,
    mapDispatchToProps
)(Forms);
