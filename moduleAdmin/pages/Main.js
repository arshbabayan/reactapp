import * as React from "react";
import { connect } from "react-redux";
import Base from "../layouts/Base";
import "./Main.scss"

class Main extends React.Component {

    componentDidMount() {
    }

    render() {
        return (
          <div className="module-admin-main-page">
            <Base id="main" title="Главная">
                <span>Главная страница</span>
            </Base>
          </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);
