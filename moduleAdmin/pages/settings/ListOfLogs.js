import * as React from "react";
import {connect} from "react-redux";

import Base from "../../layouts/Base";
import './ListOfLogs.scss';

class ListOfLogs extends React.Component {
  constructor() {
    super()
    this.state = {};
  }

  componentDidMount() {
    console.log("list of logs component")
  }

  render() {
    return (
      <div className="module-admin-logs">
        <Base id="create-users" title="Список логов">
          <div className="card card-wrap">
            <div className="card-body">
              <form>
                <div className="row">
                  <div className="col-md-3 mt-1 mb-1">
                    <div className="input-group input-group-sm">
                      <input type="search" className="form-control btn-shadow"/>
                      <div className="input-group-append">
                        <button className="btn btn-primary btn-sm btn-shadow" type="button"><i className="fas fa-search"
                                                                                               aria-hidden="true"/>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-3 mt-1 mb-1">
                    <select className="custom-select form-control-sm" >
                      <option>Дата начала</option>
                      <option>Значение 1</option>
                      <option>Значение 2</option>
                      <option>Значение 3</option>
                      <option>Значение 4</option>
                    </select>
                  </div>
                  <div className="col-md-3 mt-1 mb-1">
                    <select className="custom-select form-control-sm" >
                      <option>Дата конца</option>
                      <option>Значение 1</option>
                      <option>Значение 2</option>
                      <option>Значение 3</option>
                      <option>Значение 4</option>
                    </select>
                  </div>
                  <div className="col-md-3 mt-1 mb-1">
                    <select className="custom-select form-control-sm" >
                      <option>Тип</option>
                      <option>Значение 1</option>
                      <option>Значение 2</option>
                      <option>Значение 3</option>
                      <option>Значение 4</option>
                    </select>
                  </div>
                  <div className="col-md-3 mt-1 mb-1">
                    <select className="custom-select form-control-sm" >
                      <option>Пользователь</option>
                      <option>Значение 1</option>
                      <option>Значение 2</option>
                      <option>Значение 3</option>
                      <option>Значение 4</option>
                    </select>
                  </div>
                </div>
              </form>
            </div>
            <div className="card-body">
              <div className="table-wrap">
                {/*Таблица*/}
                <table className="table">
                  <thead>
                  <tr>
                    <th scope="col"><input type="checkbox" id="check-all" name="check-all"/></th>
                    <th scope="col"  className="text-center">id
                    </th>
                    <th scope="col" >
                      <a href="#">Сообщение</a>
                    </th>
                    <th scope="col" >
                      <a href="#">id пользователя</a>
                    </th>
                    <th scope="col" >
                      <a href="#">Элемент</a>
                    </th>
                    <th scope="col" >
                      <a href="#">Текст</a>
                    </th>
                    <th scope="col" >
                      <a href="#">Дата</a>
                    </th>
                  </tr>
                  </thead>
                  <tbody>
                  <tr>
                    <td><input type="checkbox" id="check-user-1"/></td>
                    <td  className="text-center">1</td>
                    <td >сообщеие</td>
                    <td >id</td>
                    <td >элемент</td>
                    <td >текст</td>
                    <td >18.02.19 11:00</td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" id="check-user-1"/></td>
                    <td  className="text-center">1</td>
                    <td >сообщеие</td>
                    <td >id</td>
                    <td >элемент</td>
                    <td >текст</td>
                    <td >18.02.19 11:00</td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" id="check-user-1"/></td>
                    <td  className="text-center">1</td>
                    <td >сообщеие</td>
                    <td >id</td>
                    <td >элемент</td>
                    <td >текст</td>
                    <td >18.02.19 11:00</td>
                  </tr>
                  </tbody>
                </table>
                {/*Таблица*/}
              </div>
              <div className="row mt-1">
                <div className="col-sm-6">
                  <nav aria-label="...">
                    <ul className="pagination pagination-wrap">
                      <li className="page-item disabled">
                        <span className="page-link">&lt;</span>
                      </li>
                      <li className="page-item active">
                        <span className="page-link">
                          1
                          <span className="sr-only">(current)</span>
                        </span>
                      </li>
                      <li className="page-item"><a className="page-link" href="#">2</a></li>
                      <li className="page-item"><a className="page-link" href="#">3</a></li>
                      <li className="page-item">
                        <a className="page-link" href="#">&gt;</a>
                      </li>
                    </ul>
                  </nav>
                </div>
                <div className="col-sm-6 text-sm-right">
                  Показано с 1 по 10 из 20 (страниц: 2)
                </div>
              </div>
            </div>
          </div>
        </Base>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  // console.log(state.users.count)
  return {};
};


const mapDispatchToProps = (dispatch) => {
  return {}

};

export default connect(mapStateToProps, mapDispatchToProps)(ListOfLogs);

