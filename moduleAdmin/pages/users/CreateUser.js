import * as React from "react";
import {connect} from "react-redux";

import Base from "../../layouts/Base";
import Loader from "../../../ui/Loader";
import {setFields, setFormIsValid} from "../../redux/fields"
import CustomInput from "../../layouts/CustomInput";
import {getDataForm} from "../../../helpers/functions";
import axios from "../../../axios";
import './CreateUser.scss';

class CreateUser extends React.Component {
  constructor() {
    super()
    this.state = {

      firstname: "",
      secondname: "",
      middlename: "",
      login: "",
      email: "",
      education: "",
      password: "",
      passwordConfirmation: "",
      birthday: "",
      residence: "",
      work: "",
      about: "",
      tel: "",
      web: "",
      vk: "",
      fb: "",
      avatar: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.strToObj = this.strToObj.bind(this);
    this.createUser = this.createUser.bind(this);
  }

  componentDidMount() {
    console.log("create user component")
    this.props.setFields();
    // this.props.setFormIsValid(true)
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value,
      error: ""
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const {isFormValid} = this.props
    let isValid = true;
    let data = getDataForm(this.form);
    if (data.password !== data.passwordConfirmation) {
      this.setState({error: "Пароли не совпадают"});
      isValid = false;
      this.props.setFormIsValid(false)
      return false
    }

    for (let name in data) {
      if (data[name] === "") {
        this.setState({error: `Поле ${name} не может быть пустым`});
        isValid = false
        this.props.setFormIsValid(false)
        return false
      }
    }

    if (isValid && isFormValid) {
      this.createUser()

    } else {
      alert("correct all fields")
    }

  }

  createUser() {
    const {avatar} = this.props;
    const {
      firstname, secondname, middlename, login, email, education,
      password, passwordConfirmation, birthday, residence, work, about, tel,
      web, vk, fb
    } = this.state

    let formdata = new FormData();
    formdata.append("avatar", avatar);

    const data = {
      fields: {
        1: firstname, 2: secondname, 3: middlename, 4: login,
        5: email, 6: education, 7: password, 8: passwordConfirmation,
        9: birthday, 10: residence, 11: work, 12: about,
        13: tel, 14: web, 15: vk, 16: fb,
      },
      name: login, email: email, password, password_confirmation: passwordConfirmation, file: formdata
    }

    axios.post('/admin/user/create/user', data)
      .then(res => res.data)
      .then(data => {
        console.log(data)
      })
      .catch(err => console.log("ERROR ======= " + err))
  }

  strToObj(str) {
    var obj = {};
    if (str || typeof str === 'string') {
      var objStr = str.match(/\{(.)+\}/g);
      eval("obj =" + objStr);
    }
    return obj
  }

  render() {
    const {error} = this.state;
    const {fields} = this.props;

    if (!fields) return <Loader/>

    return (
      <div className="module-admin-create-user">
        <Base id="create-users" title="Добавить пользователя">
          <div className="card card-wrap">
            {
              error && <div className="alert alert-danger" role="alert">{error}</div>
            }

            <form ref={form => (this.form = form)}>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-6">
                    {
                      fields && fields[1].map(field => {
                        let type = field.type;
                        let name = field.name;
                        if (field.name === "password-confirmation") name = "passwordConfirmation";
                        if (field.type === "string") {
                          type = "text";
                          if (field.name === "password" || field.name === "password-confirmation") {
                            type = "password"
                          }
                        }

                        let filter = field.filter;

                        return <CustomInput
                          key={field.id}
                          value={this.state[name]}
                          filter={
                            name === "tel" ? {phone: ""} :
                              name === "fb" ? {fb: ""} :
                                name === "vk" ? {vk: ""} :
                                  name === "web" ? {web: ""} :
                                    name === "middlename" ? {min: 4} :
                                      name === "email" ? {mail: ""} :
                                        filter && (this.strToObj(filter))
                          }
                          id={name}
                          name={name} label={field.caption} type={type}
                          onChange={this.handleChange}/>
                      })
                    }
                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button
                  onClick={this.handleSubmit}
                  type="submit" className="btn btn-primary btn-sm btn-shadow">
                  Сохранить
                </button>
              </div>
            </form>
          </div>
        </Base>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    fields: state.fields.fields,
    isFormValid: state.fields.isFormValid,
    avatar: state.fields.avatar,
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    setFields: () => {
      dispatch(setFields())
    },
    setFormIsValid: (isFormValid) => {
      dispatch(setFormIsValid(isFormValid))
    },
  }

};

export default connect(mapStateToProps, mapDispatchToProps)(CreateUser);

