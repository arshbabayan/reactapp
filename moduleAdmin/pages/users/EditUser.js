import * as React from "react";
import {connect} from "react-redux";

import Base from "../../layouts/Base";
import Loader from "../../../ui/Loader";
import {setFields, setFormIsValid} from "../../redux/fields"
import {setUserInfo} from "../../redux/userInfo";
import CustomInput from "../../layouts/CustomInput";
import {getDataForm} from "../../../helpers/functions";
import axios from "../../../axios";
import './CreateUser.scss';

class EditUser extends React.Component {
  constructor() {
    super()
    this.state = {

      firstname: "",
      secondname: "",
      middlename: "",
      login: "",
      email: "",
      education: "",
      password: "",
      passwordConfirmation: "",
      birthday: "",
      residence: "",
      work: "",
      about: "",
      tel: "",
      web: "",
      vk: "",
      fb: "",
      avatar: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.strToObj = this.strToObj.bind(this);
    this.editUser = this.editUser.bind(this);
  }

  componentDidMount() {
    console.log("edit user component")
    this.props.setFields();
    this.getUserInfo()
  }

  getUserInfo() {
    const id = this.props.match.params.id;

    axios.post('/admin/user/fields', {id})
      .then(res => res.data)
      .then(data => {
        // console.log(data)
        this.props.setUserInfo(data)
      })
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value,
      error: ""
    });
    this.props.setFormIsValid(true)
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.setFormIsValid(true)
    const {isFormValid} = this.props
    let isValid = true;
    let data = getDataForm(this.form);
    if (data.password !== data.passwordConfirmation) {
      this.setState({error: "Пароли не совпадают"});
      isValid = false;
      this.props.setFormIsValid(false)
    }

    for (let name in data) {
      if (data[name] === "") {
        this.setState({error: `Поле ${name} не может быть пустым`});
        isValid = false
        this.props.setFormIsValid(false)
      }
    }
    if (isValid && isFormValid) {
      this.editUser()

    } else {
      alert("correct all fields")
    }

  }

  editUser() {
    const id = this.props.match.params.id;
    const {userInfo, fields} = this.props
    const user = userInfo["user"];
    const userFields = userInfo[1]
    const {
      firstname, secondname, middlename, login, email, education,
      password, passwordConfirmation, birthday, residence, work, about, tel,
      web, vk, fb
    } = this.state

    const info = user.map(u => {
      return userFields.filter(field => field.id === u.fields_id)
    })

    let data = { name: "", email:"", password:"", password_confirmation:""}
    let obj = []

    info.map((i, ind) => {
      // console.log(this.state[i[0].name] || user[ind].value)
      // console.log({fields: {[ind+1]: this.state[i[0].name] || user[ind].value}})
      // data.fields.concat({[ind+1]: this.state[i[0].name] || user[ind].value})
      // obj = Object.assign({}, ...i);
      // console.log(Object.assign({}, {[ind+1]: this.state[i[0].name] || user[ind].value}))
      obj.push({[ind+1]: this.state[i[0].name] || user[ind].value})
      // console.log(i)
    })
    console.log(obj)

    // console.log(user[0].value)
    // fields[1].map((v,i ) => {
    //   console.log(v)
    // })
    data = {
      fields: {

      },
      name: login||user[3].value, email: email||user[4].value, password: password||user[6].value, password_confirmation: passwordConfirmation||user[7].value, file: "formdata"
    }

    // data = {
    //   fields: {
    //     1: firstname||user[0].value,
    //     2: secondname||user[1].value,
    //     3: middlename||user[2].value,
    //     4: login||user[3].value,
    //     5: email||user[4].value, 6: education||user[5].value, 7: password||user[6].value, 8: passwordConfirmation||user[7].value,
    //     9: birthday||user[8].value, 10: residence||user[9].value, 11: work||user[10].value, 12: about||user[11].value,
    //     13: tel||user[12].value, 14: web||user[13].value, 15: vk||user[14].value, 16: fb||user[15].value,
    //   },
    //   name: login||user[3].value, email: email||user[4].value, password: password||user[6].value, password_confirmation: passwordConfirmation||user[7].value, file: "formdata"
    // }
    // console.log(data)


    // axios.post(`/admin/user/edit/${id}`, data)
    //   .then(res => res.data)
    //   .then(data => {
    //     console.log(data)
    //   })
    //   .catch(err => console.log("ERROR ======= " + err))
  }

  strToObj(str) {
    var obj = {};
    if (str || typeof str === 'string') {
      var objStr = str.match(/\{(.)+\}/g);
      eval("obj =" + objStr);
    }
    return obj
  }

  render() {
    const {error} = this.state;
    const {fields} = this.props;

    const {userInfo} = this.props
    if (!userInfo || !fields) return <Loader/>

    const userFields = userInfo[1]
    const user = userInfo["user"];

    const info = user.map(u => {
      return userFields.filter(field => field.id === u.fields_id)
    })

    return (
      <div className="module-admin-create-user">
        <Base id="create-users" title="Редактировать пользователя">
          <div className="card card-wrap">
            {
              error && <div className="alert alert-danger" role="alert">{error}</div>
            }

            <form ref={form => (this.form = form)}>
              <div className="card-body">
                <div className="row">
                  <div className="col-lg-6">
                    {
                      info && info.map((field, i) => {
                        let type = field[0].type;
                        let name = field[0].name;

                        if (field[0].name === "password-confirmation") name = "passwordConfirmation";
                        if (field[0].type === "string") {
                          type = "text";
                          if (field[0].name === "password" || field[0].name === "password-confirmation") {
                            type = "password"
                          }
                        }
                        const value = this.state[name] || user[i].value;
                        // console.log(value)

                        let filter = field[0].filter;

                        return <CustomInput
                          key={field[0].id}
                          value={value}
                          filter={
                            name === "tel" ? {phone: ""} :
                              name === "fb" ? {fb: ""} :
                                name === "vk" ? {vk: ""} :
                                  name === "web" ? {web: ""} :
                                    name === "middlename" ? {min: "4"} :
                                      name === "email" ? {mail: ""} :
                                        filter && (this.strToObj(filter))
                          }
                          id={name}
                          name={name} label={field[0].caption} type={type}
                          onChange={this.handleChange}/>
                      })
                    }

                  </div>
                </div>
              </div>
              <div className="card-footer">
                <button
                  onClick={this.handleSubmit}
                  type="submit" className="btn btn-primary btn-sm btn-shadow">
                  Редактировать
                </button>
              </div>
            </form>
          </div>
        </Base>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  return {
    fields: state.fields.fields,
    isFormValid: state.fields.isFormValid,
    avatar: state.fields.avatar,
    userInfo: state.userInfo.info

  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    setFields: () => {
      dispatch(setFields())
    },
    setFormIsValid: (isFormValid) => {
      dispatch(setFormIsValid(isFormValid))
    },
    setUserInfo: (info) => {
      dispatch(setUserInfo(info))
    },

  }

};

export default connect(mapStateToProps, mapDispatchToProps)(EditUser);

