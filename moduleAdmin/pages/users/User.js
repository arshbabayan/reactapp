import * as React from "react";
import {connect} from "react-redux";

import {setUserInfo} from "../../redux/userInfo";
import Loader from "../../../ui/Loader";

import Base from "../../layouts/Base";
import axios from "../../../axios";
import './User.scss';

class User extends React.Component {
  componentDidMount() {
    this.getUserInfo()

  }

  getUserInfo() {
    const id = this.props.match.params.id;

    axios.post('/admin/user/fields', {id})
      .then(res => res.data)
      .then(data => {
        console.log(data)
        this.props.setUserInfo(data)
      })
  }

  render() {
    const {userInfo} = this.props
    if (!userInfo) return <Loader/>

    const fields = userInfo[1]
    const user = userInfo["user"];

    const info = user.map(u => {
      return fields.filter(field => field.id === u.fields_id)
    })

    return (
      <div className="module-admin-user">
        <Base id="test" title="Информация пользователя">
          <div className="row">
            <div className="col-lg-6">
              <div className="card card-wrap">
                <div className="card-header">Информация пользоватля</div>
                <div className="card-body">
                  <div className="row">
                    <div className="card-body">
                      <div className="float-left text-center">
                        <a href="#">
                          <img className="avatar-img" style={{width: '100px', height: 'auto'}}
                               src="http://notforsale.store/leopays/avatar.jpg"/>
                        </a>
                        <div className="mt-1">
                          <i className="fas fa-star checked" id="true"/>
                          <i className="fas fa-star checked" id="true"/>
                          <i className="fas fa-star checked" id="true"/>
                          <i className="fas fa-star" id="true"/>
                          <i className="fas fa-star" id="true"/>
                        </div>
                        <div className="text-muted mb-2">Рейтинг: 3 из 5</div>
                        <button type="button" className="btn btn-primary btn-sm w-100 mb-1">Написать</button>
                      </div>
                      <div style={{marginLeft: '120px'}}>
                        {
                          info && info.map((i, ind) => {
                            return <div key={ind}>{i[0].caption}: {user[ind].value}</div>

                          })
                        }
                        {/*<div className="font-weight-bold">Фамилия Имя Отчество</div>*/}
                        <div>Логин:</div>
                        <div>Дата регистрации:</div>
                        <div>Блокировка:</div>
                        {/*<div>Спонсор: <a href="#">логин спонсора</a></div>*/}
                        <div>Спонсор:</div>
                        <div>Группа пользователя:</div>
                        <div>Баланс на вывод:</div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Кнтактные данне</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-4">Почта:</div>
                    <div className="col-8">e-mail</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Телефон:</div>
                    <div className="col-8">телефон</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Веб-сайт:</div>
                    <div className="col-8">веб-сайт</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Вконтакте:</div>
                    <div className="col-8">вконтакте</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Facebook:</div>
                    <div className="col-8">facebook</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Telegram:</div>
                    <div className="col-8">telegram</div>
                  </div>
                  <div className="row">
                    <div className="col-4">YouTube:</div>
                    <div className="col-8">youtybe</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Bitcointalk:</div>
                    <div className="col-8">bitcointalk</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Twitter:</div>
                    <div className="col-8">twitter</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Medium:</div>
                    <div className="col-8">medium</div>
                  </div>
                  <div className="row">
                    <div className="col-4">Golos:</div>
                    <div className="col-8">golos</div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Команда</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-4">Первая линия:</div>
                    <div className="col-8"><a href="#">10</a></div>
                  </div>
                  <div className="row">
                    <div className="col-4">Всего пртнёров:</div>
                    <div className="col-8">
                      <div>Первая линия</div>
                      <div>Партнёр 1</div>
                      <div>Партнёр 2</div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Покупки тарифиов</div>
                <div className="card-body">
                  <div className="font-weight-bold">Старт</div>
                  <div className="row">
                    <div className="col-6">Первая линия:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">1</span>
                      <span className="badge badge-pill badge-success">2</span>
                      <span className="badge badge-pill badge-success">3</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Аккаунтов:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Клонов:</div>
                    <div className="col-6"><span className="badge badge-pill badge-success">10</span></div>
                  </div>
                  <div className="font-weight-bold mt-3">Вип</div>
                  <div className="row">
                    <div className="col-6">Месяц:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">1 месяц</span>
                      <span className="badge badge-pill badge-success">2 месяца</span>
                      <span className="badge badge-pill badge-success">3 месяца</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Активен до:</div>
                    <div className="col-6">дата</div>
                  </div>
                  <div className="row">
                    <div className="col-6">Аккаунтов:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Клонов:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Покупка услуг</div>
                <div className="card-body">
                  <div className="font-weight-bold">Реклама</div>
                  <div className="row">
                    <div className="col-6">Рекламный баланс:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10 LPC</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">общая сумма потраченных токенов:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">100 LPC</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">История покупок:</div>
                    <div className="col-6">история</div>
                  </div>
                  <div className="font-weight-bold mt-3">Сервисы автоамтизации</div>
                  <div className="row">
                    <div className="col-6">Ставки:</div>
                    <div className="col-6">ставки</div>
                  </div>
                  <div className="font-weight-bold mt-3">Ставки в аукционе</div>
                  <div className="row">
                    <div className="col-6">Баланс ставок:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10 LPC</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Всего купленных ставок:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Бонусные ставки:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Истрия покупок ставок:</div>
                    <div className="col-6">история</div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Кошельки</div>
                <div className="card-body">
                  <div className="table-wrap">
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Валюта</th>
                        <th scope="col">Номер кошелька</th>
                        <th scope="col">Баланс</th>
                        <th scope="col">История</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td className="font-weight-bold">LPCa</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">LPC</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">RUB</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">USD</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">EUR</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">ETH</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">BTC</td>
                        <td>11111</td>
                        <td>10</td>
                        <td>история</td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="card card-wrap">
                <div className="card-header">Контракты</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">Сумма контрактов:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">100</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Процент:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Количество в день:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Всего получено:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Заработок</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">Всего доход:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Старт:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Вип:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Майнинг:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Реферальные с майнинга:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Промоушен</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">Основной промоушен:</div>
                    <div className="col-6">основной промоушен</div>
                  </div>
                  <div className="row">
                    <div className="col-6">Лидер недели:</div>
                    <div className="col-6">лидер недели</div>
                  </div>
                  <div className="row">
                    <div className="col-6">Еженедельный:</div>
                    <div className="col-6">еженедельный</div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Социальная сеть</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">Количество друзей:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Группы пользователя:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Мероприятия:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Фотографии:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Видео:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">Жалобы:</div>
                    <div className="col-6">
                      <span className="badge badge-pill badge-success">10</span>
                    </div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Аукцион</div>
                <div className="card-body">
                  <div className="row">
                    <div className="col-6">История ставок:</div>
                    <div className="col-6">история</div>
                  </div>
                  <div className="row">
                    <div className="col-6">Победы:</div>
                    <div className="col-6">победы</div>
                  </div>
                  <div className="row">
                    <div className="col-6">Покупки за вычетом ставки:</div>
                    <div className="col-6">покупки</div>
                  </div>
                </div>
              </div>
              <div className="card card-wrap">
                <div className="card-header">Игры за токен</div>
                <div className="card-body">
                  <div className="table-wrap">
                    <table className="table">
                      <thead>
                      <tr>
                        <th scope="col">Игра</th>
                        <th scope="col">Количество ставок</th>
                        <th scope="col">Победы</th>
                        <th scope="col">Ничья</th>
                        <th scope="col">Проигрыш</th>
                        <th scope="col">CTR (%)</th>
                      </tr>
                      </thead>
                      <tbody>
                      <tr>
                        <td className="font-weight-bold">Камень Ножницы Бумага</td>
                        <td>10</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>100</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">Орёл-решка</td>
                        <td>10</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>100</td>
                      </tr>
                      <tr>
                        <td className="font-weight-bold">Минёр</td>
                        <td>10</td>
                        <td>0</td>
                        <td>0</td>
                        <td>0</td>
                        <td>100</td>
                      </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Base>
      </div>
    );
  }
};
const mapStateToProps = (state) => {
  // console.log(state)
  return {
    userInfo: state.userInfo.info
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setUserInfo: (info) => {
      dispatch(setUserInfo(info))
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(User);

