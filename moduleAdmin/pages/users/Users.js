import * as React from "react";
import {connect} from "react-redux";
import {Link} from "react-router-dom";

import Base from "../../layouts/Base";
import Loader from "../../../ui/Loader";
import axios from "../../../axios";
import {setUsersList, setUserId, removeUser, setUsers} from "../../redux/users";
import './Users.scss';
import "../../../ui/Switch.scss";

class Users extends React.Component {
  constructor() {
    super()
    this.state = {
      isFieldActive: false,
      searchValue: "",
      activeButton: 1,
      activeField: null,
      algorithm: "ASC",
      isSearch: false,
    }
    this.handleSearchSubmit = this.handleSearchSubmit.bind(this);
    this.onSearchChange = this.onSearchChange.bind(this);
    this.removeUser = this.removeUser.bind(this);
    this.getUsers = this.getUsers.bind(this);
    this.next = this.next.bind(this);
    this.prev = this.prev.bind(this);
  }

  componentDidMount() {
    console.log("users component")
    this.props.setUsers()
  }

  getUsers(page) {
    axios.post('/admin/users', {page})
      .then(res => res.data)
      .then(data => {
        this.props.setUsersList(data.users)
      })
  }

  handlePageClick(pageNumber) {
    this.getUsers(pageNumber)
    this.setState({activeButton: pageNumber})

  }

  next(len) {
    const {activeButton} = this.state
    const nextPage = activeButton + 1
    if (nextPage > len) return
    this.getUsers(nextPage)
    this.setState({activeButton: nextPage})
  }

  prev() {
    const {activeButton} = this.state
    const prevPage = activeButton - 1;
    if (prevPage <= 0) return
    this.getUsers(prevPage)
    this.setState({activeButton: prevPage})
  }

  orderBy(field) {
    let algorithm = "DESC";
    let isFieldActive = true;
    if (this.state.algorithm === "DESC") {
      algorithm = "ASC";
      isFieldActive = false
    }
    this.setState(prevState => ({
      isFieldActive,
      algorithm,
      activeField: field,
      isSearch: false
    }));

    axios.post('/admin/users', {order: {[field]: this.state.algorithm}})
      .then(res => res.data)
      .then(data => {
        this.props.setUsersList(data.users)
      })
  }

  onSearchChange(event) {
    event.preventDefault()
    const value = event.target.value;
    let isSearch = this.state.isSearch

    if (value !== "") {
      isSearch = true
    } else {
      isSearch = false
    }

    this.setState({searchValue: value, isSearch})

    axios.post('/admin/users', {search: value})
      .then(res => res.data)
      .then(data => {
        this.props.setUsersList(data.users)
      })
  }

  handleSearchSubmit(event) {
    event.preventDefault()

    axios.post('/admin/users', {search: this.state.searchValue})
      .then(res => res.data)
      .then(data => {
        this.props.setUsersList(data.users)
      })
  }

  handleInputChange(id, event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;

    if (value === true) {
      this.props.setUserId(id)
    } else {
      this.props.setUserId(null)
    }

  }

  removeUser() {
    const {id, usersCount} = this.props

    if (this.props.id !== null) {
      this.props.removeUser(id, usersCount)
      axios.post(`/admin/user/delete/${id}`)
        .then(res => res.data)
        .then(data => {
          this.props.removeUser(id, usersCount)
        })
        .catch(err => console.log(err))
    }
  }

  render() {
    const {users, count} = this.props.users;
    const {activeField, isFieldActive, searchValue, isSearch, activeButton} = this.state;
    if (!users) return <Loader/>

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(count / 10); i++) {
      pageNumbers.push(i);
    }

    return (
      <div className="module-admin-users">
        <Base id="users" title="Список пользователей">
          <div className="card card-wrap">
            <div className="card-body">
              <form>
                <div className="row">
                  <div className="col-md-4">
                    <div className="input-group input-group-sm">
                      <input
                        onChange={this.onSearchChange}
                        type="text"
                        value={searchValue}
                        className="form-control btn-shadow"
                      />
                      <div className="input-group-append">
                        <button onClick={this.handleSearchSubmit}
                                className="btn btn-primary btn-sm btn-shadow" type="button">
                          <i className="fas fa-search" aria-hidden="true"/>
                        </button>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-4">
                    <button className="btn btn-primary btn-sm btn-shadow" type="button" data-toggle="collapse"
                            data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                      Расширенный поиск
                    </button>
                  </div>
                </div>
                <div className="collapse" id="collapseExample">
                  <div className="row">
                    <div className="col-md-4 mt-1 mb-1">
                      <select className="custom-select form-control-sm">
                        <option>Выберите статус</option>
                        <option value>Включён</option>
                        <option value>Выключен</option>
                      </select>
                    </div>
                    <div className="col-md-4 mt-1 mb-1">
                      <select className="custom-select form-control-sm">
                        <option>Выберите группу</option>
                        <option value>Супер Админ</option>
                        <option value>Админ</option>
                        <option value>Менеджер</option>
                        <option value>Техподдержка</option>
                        <option value>Модератор</option>
                        <option value>Пользователь</option>
                        <option value>Старт</option>
                        <option value>ВИП</option>
                        <option value>Покупатель</option>
                        <option value>Продавец</option>
                        <option value>Рекламодатель</option>
                        <option value>Блогер</option>
                      </select>
                    </div>
                    <div className="col-md-4 mt-1 mb-1">
                      <select className="custom-select form-control-sm">
                        <option>Выберите</option>
                        <option>Значение 1</option>
                        <option>Значение 2</option>
                        <option>Значение 3</option>
                        <option>Значение 4</option>
                      </select>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="card-body">
              <Link to='/admin/users/create-user'>
                <button type="button" className="btn btn-primary btn-sm btn-shadow mb-1">Добавить</button>
              </Link>
              <button onClick={this.removeUser}
                      type="button" className="btn btn-danger btn-sm btn-shadow mb-1">
                Удалить
              </button>
            </div>

            <div className="card-body">
              <div className="table-wrap">

                <table className="table">
                  <thead>
                  <tr>
                    <th scope="col">
                      <input type="checkbox" id="check-all" name="check-all"/></th>

                    <th scope="col" className="text-center">
                      <a onClick={this.orderBy.bind(this, "id")}
                         className={`${activeField === "id" && isFieldActive ? 'active' : ''}`}>id
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>

                    <th scope="col">
                      <a onClick={this.orderBy.bind(this, "name")}
                         className={`${activeField === "name" && isFieldActive ? 'active' : ''}`}>Логин
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>
                    <th scope="col" className="">
                      <a onClick={this.orderBy.bind(this, "status")}
                         className={`${activeField === "status" && isFieldActive ? 'active' : ''}`}>Вкл
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>
                    <th scope="col">
                      <a onClick={this.orderBy.bind(this, "group")}
                         className={`${activeField === "group" && isFieldActive ? 'active' : ''}`}>Группа
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>
                    <th scope="col">
                      <a onClick={this.orderBy.bind(this, "email")}
                         className={`${activeField === "email" && isFieldActive ? 'active' : ''}`}>E-mail
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>
                    <th scope="col">
                      <a onClick={this.orderBy.bind(this, "created_at")}
                         className={`${activeField === "created_at" && isFieldActive ? 'active' : ''}`}>Дата
                        регистрации
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>
                    <th scope="col">
                      <a onClick={this.orderBy.bind(this, "last_activity")}
                         className={`${activeField === "last_activity" && isFieldActive ? 'active' : ''}`}>Дата
                        последнего входа
                        <i className="fas fa-arrow-up ml-2" aria-hidden="true"/>
                      </a>
                    </th>

                    <th scope="col"/>
                  </tr>
                  </thead>
                  <tbody>

                  {
                    users && users.length > 0 && users.map(user => {
                      return <tr key={user.id}>
                        <td>
                          <input onChange={(e) => this.handleInputChange(user.id, e)}
                                 type="checkbox"
                                 id={`check-user-${user.id}`}/>
                        </td>
                        <td className="text-center">{user.id}</td>
                        <td>
                          <Link to={`/admin/users/user/${user.id}`}>
                            {user.name}
                          </Link>
                        </td>
                        <td className="text-center">
                          <label className="switch">
                            <input type="checkbox" id={`check-user-${2}`} name={`check-user-${2}`}/>
                            <span className="slider round"/>
                          </label>
                        </td>
                        <td>Пользователь</td>
                        <td>{user.email}</td>
                        <td>{user.created_at}</td>
                        <td>27.10.18 15:22</td>
                        <td className="text-right">
                          <button type="button" className="btn btn-success btn-sm btn-shadow" data-toggle="modal"
                                  data-target={`#user-${user.id}`}><i className="fas fa-eye" aria-hidden="true"/>
                          </button>
                          {/* Modal */}
                          <div className="modal fade" id={`user-${user.id}`} tabIndex={-1} role="dialog"
                               aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div className="modal-dialog modal-dialog-centered" role="document">
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5 className="modal-title" id="exampleModalLongTitle">Информация о
                                    пользователе {user.id}</h5>
                                  <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                  </button>
                                </div>
                                <div className="modal-body text-left">
                                  Информация {user.name}
                                  <div>id {user.id}</div>
                                  <div>Логин {user.name}</div>
                                  <div>E-mail {user.email}</div>
                                  <div>Дата регистрации {user.created_at}</div>
                                </div>
                                <div className="modal-footer">
                                  <button type="button" className="btn btn-secondary btn-sm"
                                          data-dismiss="modal">Закрыть
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <Link to={`/admin/users/edit-user/${user.id}`}>
                            <button type="button" className="btn btn-primary btn-sm btn-shadow">
                              <i className="fas fa-edit" aria-hidden="true"/>
                            </button>
                          </Link>
                        </td>
                      </tr>
                    })
                  }

                  </tbody>
                </table>
                {/*Таблица*/}
              </div>
              <div className="row mt-1">

                <div className="col-sm-6">
                  <nav aria-label="...">
                    {
                      isSearch === false && <ul className="pagination pagination-wrap">
                        <li className="page-item">
                          <span className="page-link" onClick={this.prev}>&lt;</span>
                        </li>
                        {
                          isSearch === false && pageNumbers.map(number => {
                            return <li className={`page-item ${this.state.activeButton === number ? 'active' : ''}`}
                                       key={number}>
                              <a className="page-link" onClick={this.handlePageClick.bind(this, number)}>{number}</a>
                            </li>
                          })
                        }
                        <li className="page-item">
                          <a className="page-link" onClick={this.next.bind(this, pageNumbers.length)}>&gt;</a>
                        </li>
                      </ul>
                    }
                  </nav>
                </div>
                {
                  isSearch === false && <div className="col-sm-6 text-sm-right">
                    Показано с 1
                    по {users && users.length} из {count} (страниц: {pageNumbers && pageNumbers.length})
                  </div>
                }

              </div>
            </div>
          </div>
        </Base>
      </div>
    );
  }
};

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    users: state.users,
    usersCount: state.users.count,
    id: state.users.id
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
    setUsersList: users => {
      dispatch(setUsersList(users))
    },
    setUsers: () => {
      dispatch(setUsers())
    },
    removeUser: (id, count) => {
      dispatch(removeUser(id, count))
    },
    setUserId: id => {
      dispatch(setUserId(id))
    }
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Users);

