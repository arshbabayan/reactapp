import reducerRegistry from "../../redux/reducerRegister";
import axios from "../../axios";

const SET_FIELDS = 'SET_FIELDS';
const SET_IS_FORM_VALID = 'SET_IS_FORM_VALID';
const SET_AVATAR = 'SET_AVATAR';

export const setFields = () => {
  return dispatch => {
    axios.post('/admin/user/create/fields')
      .then(res => res.data)
      .then(data => {
        dispatch({type: SET_FIELDS, fields: data})
      })
  };
};

export const setFormIsValid = (isFormValid) => {
  return { type: SET_IS_FORM_VALID, isFormValid: isFormValid };
}

export const setAvatar = (avatar) => {
  return { type: SET_AVATAR, avatar };
}

const initialState = {
  fields: null,
  isFormValid: false,
  avatar: null
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_FIELDS:
      return { ...state, fields: action.fields }
    case SET_IS_FORM_VALID:
      return { ...state, isFormValid: action.isFormValid }
    case SET_AVATAR:
      return { ...state, avatar: action.avatar }
    default: {
      // console.log(state)
      return state;
    }
  }
}

reducerRegistry.register('fields', reducer);

