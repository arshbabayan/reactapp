import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";
import axios from "../../axios";

const initialState = {
    loading: null,
    error: null,
    forms: null,
    fields: null
};


const FORMS_GET_START = 'FORMS_GET_START';
const FORMS_GET_SUCCESS = 'FORMS_GET_SUCCESS';
const FORMS_GET_ERROR = '';
const FORM_FIELDS_GET_START = 'FORM_FIELDS_GET_START';
const FORM_FIELDS_GET_SUCCESS = 'FORMS_GET_SUCCESS';
const FORM_FIELDS_GET_ERROR = 'FORM_FIELDS_GET_ERROR';

export function getForms() {
    return (dispatch) => {
        dispatch({ type: FORMS_GET_START });
        axios.post('/admin/forms')
            .then(data => data.data)
            .then(data => {
                dispatch(setAuth);
                dispatch({ type: FORMS_GET_SUCCESS, data });
            })
            .catch(e => dispatch({ type: FORMS_GET_ERROR, data: e }));
    };
}

export function getFormFields(id) {
    console.log('get form fields');
    return (dispatch) => {
        dispatch({ type: FORM_FIELDS_GET_START });
        axios.post(`/admin/forms/${id}`)
            .then(data => data.data)
            .then(data => {
                dispatch(setAuth);
                dispatch({ type: FORM_FIELDS_GET_SUCCESS, data });
            }).catch(e => {
                dispatch({ type: FORM_FIELDS_GET_ERROR, data: e });
            });
    };
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case FORMS_GET_START: {
            return { ...state, loading: Process.Start };
        }
        case FORMS_GET_SUCCESS: {
            return { ...state, loading: Process.Success, forms: action.data };
        }
        case FORMS_GET_ERROR: {
            return { ...state, loading: Process.Error, error: action.data };
        }
        case FORM_FIELDS_GET_START: {
            return { ...state, loading: Process.Start };
        }
        case FORM_FIELDS_GET_SUCCESS: {
            return { ...state, loading: Process.Success, fields: action.data };
        }
        case FORM_FIELDS_GET_ERROR: {
            return { ...state, loading: Process.Error, error: action.data };
        }
        default: {
            return state;
        }
    }
}

reducerRegistry.register('admin.forms', reducer);
