import reducerRegistry from "../../redux/reducerRegister";

const SET_USER_INFO = 'SET_USER_INFO';

export const setUserInfo = (info) => {
  return { type: SET_USER_INFO, info: info };
}

const initialState = {
  userInfo: null,
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_USER_INFO:
      return { ...state, info: action.info }

    default: {
      return state;
    }
  }
}

reducerRegistry.register('userInfo', reducer);

