import reducerRegistry from "../../redux/reducerRegister";
import axios from "../../axios";

const SET_USERS_LIST = 'SET_USERS_LIST';
const SET_USERS_COUNT = 'SET_USERS_COUNT';
const REMOVE_USER = 'REMOVE_USER';
const SET_USER_ID = 'SET_USER_ID';

export const setUsers = () => {
  console.log("set users action")
  return dispatch => {
    axios.post('/admin/users', {page: 1})
      .then(res => res.data)
      .then(data => {
        dispatch({type: SET_USERS_LIST, users: data.users})
        dispatch({type: SET_USERS_COUNT, count: data.count})
      })
  };
};

export const setUsersList = (users) => {
  return { type: SET_USERS_LIST, users: users };
}
export const removeUser = (id, count) => {
  return { type: REMOVE_USER, id: id, count: count };
}

export const setUserId = (id) => {
  return { type: SET_USER_ID, id: id };
}

const initialState = {
  users: null,
  id: null
};

const reducer = (state = initialState, action) => {
  switch(action.type) {
    case SET_USERS_LIST:
      return { ...state, users: action.users }
    case SET_USERS_COUNT:
      return { ...state, count: action.count }
    case REMOVE_USER:
      return { ...state, users: state.users.filter(user => user.id !== action.id), count: action.count-1 }
    case SET_USER_ID:
      return { ...state, id: action.id }
    default: {
      // console.log(state)
      return state;
    }
  }
}

reducerRegistry.register('users', reducer);

