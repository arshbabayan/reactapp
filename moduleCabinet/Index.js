import * as React from "react";
import { Route } from "react-router-dom";
import Cabinet from "./pages/Cabinet";
import Income from "./pages/Income";
import Investor from "./pages/Investor";
import Team from "./pages/Team";
import RefLinks from "./pages/RefLinks";
import Vip from "./pages/Vip";
import Binar from "./pages/Binar";
import PaymentHistory from "./pages/PaymentHistory";

export default class IndexComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Route exact path="/cabinet" component={Cabinet} />
        <Route path="/cabinet/income" component={Income} />
        <Route path="/cabinet/investor" component={Investor} />
        <Route path="/cabinet/reflinks" component={RefLinks} />
        <Route path="/cabinet/team" component={Team} />
        <Route path="/cabinet/binar" component={Binar} />
        <Route path="/cabinet/vip/:id" component={Vip} />
        <Route exact path="/cabinet/vip" component={Vip} />
        <Route path="/cabinet/payment_history" component={PaymentHistory} />
      </>
    );
  }
}
