import React, { Component } from "react";
import { connect } from "react-redux";
import { Badge, Button } from "reactstrap";

class AddsAccount extends Component {
  render() {
    return (
      <>
        <div className="text-bold">
          <span>Рекламный счет: </span>
          <Button outline className="question ml-1" size="sm" color="secondary">
            ?
          </Button>
        </div>
        <Badge color="primary">342</Badge>
      </>
    );
  }
}

export default AddsAccount;
