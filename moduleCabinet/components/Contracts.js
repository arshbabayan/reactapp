import React, { Component } from 'react';
import { Badge, Button } from "reactstrap";

class Contracts extends Component {
  render() {
    return (
      <>
        <div className="text-bold">
          <span>Контракты SW: </span>
          <Button outline className="question ml-1" size="sm" color="secondary">
            ?
          </Button>
        </div>
        <Badge color="primary">523</Badge>
        <div>
          <a href="https://leopays.com/earn/staking-wallet">Калькулятор</a>
        </div>
      </>
    );
  }
}

export default Contracts;
