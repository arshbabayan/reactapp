import React, { Component } from 'react';
import { Input } from 'reactstrap';
import './PanelVip.scss';

class PanelVip extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dropdownOpen: false
    }
    this.change = this.change.bind(this);
  }

  change() {
    console.log('accounts change', this.accaunt.value);
    this.props.change(this.account.value)
  }

  render() {
    return (
      <div className='panel-vip'>
        <Input innerRef={account => this.account = account } onChange={this.change} className="accaunts" type="select">
          <option>Аккаунты</option>
          {this.props.accounts.filter(item => item.type === 0).map((item, index) => {
            return (
              <option key={index} value={item.id}>{`${this.props.name} ${item.id}`}</option>
            );
          })}
        </Input>
        <Input innerRef={accaunt => this.accaunt = accaunt } onChange={this.change} className="accaunts" type="select">
          <option>Клоны</option>
          {this.props.accounts.filter(item => item.type === 1).map((item, index) => {
            return (
              <option key={index} value={item.id}>{`${this.props.name} ${item.id}`}</option>
            );
          })}
        </Input>
      </div>
    );
  }
}

PanelVip.defaultProps = {
  accounts: []
}

export default PanelVip;
