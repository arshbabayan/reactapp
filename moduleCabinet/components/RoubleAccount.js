import * as React from "react";
import { connect } from "react-redux";
import { Badge, Button } from "reactstrap";

/**
 * Рублевый счет
 */
class RoubleAccount extends React.Component {
    render() {
        return (
            <>
                <div className="text-bold">
                    <span>Рублевый счет: </span>
                    <Button outline className="question ml-1" size="sm" color="secondary">?</Button>
                </div>
                <Badge color="primary">342</Badge>
            </>
        );
    }
}

const mapState2Props = (state) => {
    return {};
};

export default connect(mapState2Props)(RoubleAccount);
