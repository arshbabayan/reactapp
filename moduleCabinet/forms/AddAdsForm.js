import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class AddAdsForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <>
        <form>
          <Label for="addAds">
            На рекламный
            <Button onClick={this.toggle} type="button" outline className="question ml-1" size="sm" color="secondary">
              ?
            </Button>
          </Label>
          <Input id="addAds" className="form-control" name="add_ads" />
          <Button className="mt-3" color="primary">
            ПЕРЕВЕСТИ
          </Button>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>На рекламный</ModalHeader>
          <ModalBody>
            <p>Перевод токенов на счет для рекламы 1 к 1</p>
            <p>Рекламная площадка находится в процессе разработки</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default AddAdsForm;
