import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class AddLpcForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <>
        <form>
          <Label for="addLpc">Пополнить копилку
            <Button onClick={this.toggle} outline className="question ml-1" size="sm" color="secondary">?</Button>
          </Label>
          <Input id="addLpc" className="form-control" name="add_lpc" />
          <Button className="mt-3" color="primary">
            ПОПОЛНИТЬ
          </Button>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Пополнить копилку</ModalHeader>
          <ModalBody>
            <p>Вы можете пополнить копилку</p>
            <p>по ставке 1 к 2</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default AddLpcForm;
