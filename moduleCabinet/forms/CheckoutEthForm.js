import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class CheckoutEthForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <>
        <form>
          <Label for="checkoutEth">
            Вывести на ETH
            <Button onClick={this.toggle} outline className="question ml-1" size="sm" color="secondary">
              ?
            </Button>
          </Label>
          <Input id="checkoutEthWallet" className="form-control" name="checkout_eth_wallet" placeholder="Кошелек ETH" />
          <Input id="checkoutEth" className="form-control mt-2" name="checkout_eth" />
          <Button className="mt-3" color="primary">
            ВЫВЕСТИ
          </Button>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Вывести на ETH</ModalHeader>
          <ModalBody>
            <p>Укажите кошелек ETH, который поддерживает токены ERC-20 и сумму токенов.</p>
            <p>При выводе менее 100 LPC комиссия 0,3 LPC.</p>
            <p>От 100 LPC комиссия отсутствует</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default CheckoutEthForm;
