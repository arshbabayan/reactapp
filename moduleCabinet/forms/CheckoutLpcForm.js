import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class CheckoutLpc extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({ modal: !this.state.modal });
  }

  render() {
    return (
      <>
        <form id="lpcCheckoutForm">
          <Label for="lpcCheckout">
            ВЫВЕСТИ
            <Button onClick={this.toggle} outline className="question ml-1" size="sm" color="secondary">
              ?
            </Button>
          </Label>
          <Input id="lpcCheckout" className="form-control" name="lpc_checkout" />
          <Button className="mt-3" color="primary">
            ВЫВЕСТИ
          </Button>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>ВЫВЕСТИ</ModalHeader>
          <ModalBody>
            <p>Вывод токенов на ваш кошелек Payeer</p>
            <p>по курсу 1 LPC = 16 рублей</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default CheckoutLpc;
