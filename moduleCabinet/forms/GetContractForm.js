import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class GetContractForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <>
        <form>
          <Label for="getContract">
            КУПИТЬ КОНТРАКТ SW
            <Button onClick={this.toggle} outline className="question ml-1" size="sm" color="secondary">
              ?
            </Button>
          </Label>
          <Input id="getContract" className="form-control" name="get_contract" />
          <div>
            <small className="text-muted">Доступно всего 4571</small>
          </div>
          <div>
            <Button className="mt-3" color="primary">
              КУПИТЬ КОНТРАКТ SW
            </Button>
          </div>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Вывести на ETH</ModalHeader>
          <ModalBody>
            <p>Для покупки контракта в копилке должно быть равно или кратно 500 LPC, что соответствует 0.5 контракта SW</p>
            <p>После покупки контракт автоматически поступает на счет Контракты SW. </p>
            <p>Общее количество контрактов 5000 и новых выпускаться не будет.</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default GetContractForm;
