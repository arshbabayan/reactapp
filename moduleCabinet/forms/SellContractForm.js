import React, { Component } from "react";
import { Label, Input, Button, Modal, ModalHeader, ModalBody } from "reactstrap";

class SellContractForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    }
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
  }

  render() {
    return (
      <>
        <form>
          <Label for="sellContract">
            Продать контракт SW
            <Button onClick={this.toggle} outline className="question ml-1" size="sm" color="secondary">
              ?
            </Button>
          </Label>
          <Input id="sellContract" className="form-control" name="sell_contract" />
          <Button className="mt-3" color="primary">
            Продать контракт SW
          </Button>
        </form>
        <Modal className="text-center income-modal" isOpen={this.state.modal} toggle={this.toggle}>
          <ModalHeader toggle={this.toggle}>Продать контракт SW</ModalHeader>
          <ModalBody>
            <p>ВНИМАНИЕ! При продаже контрактов на вторую и следующие продажи в один месяц взимается комиссия 2%</p>
          </ModalBody>
        </Modal>
      </>
    );
  }
}

export default SellContractForm;
