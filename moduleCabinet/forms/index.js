import * as React from "react";
import CheckoutLpc from "./CheckoutLpcForm";
import AddLpc from "./AddLpcForm";
import AddAds from "./AddAdsForm";
import CheckoutEth from "./CheckoutEthForm";
import GetContract from "./GetContractForm";
import CheckoutIcoAds from "./CheckoutIcoAdsForm";
import SellContract from "./SellContractForm";

export const CheckoutLpcForm = () => <CheckoutLpc />;
export const AddLpcForm = () => <AddLpc />;
export const AddAdsForm = () => <AddAds />;
export const CheckoutEthForm = () => <CheckoutEth />;
export const GetContractForm = () => <GetContract />;
export const CheckoutIcoAdsForm = () => <CheckoutIcoAds />;
export const SellContractForm = () => <SellContract />
