import * as React from 'react'
import { connect } from 'react-redux'
import ReactJson from 'react-json-view';
import Page from '../../components/Page'
import Main from '../../layouts/Main'
import { getCabinetInfo } from '../redux/cabinet'
import withAuth from '../../hoc/withAuth'
import { Redirect } from 'react-router'
import Loader from '../../ui/Loader'
import TableObject from '../../ui/TableObject'
import { Process } from '../../helpers/types'
import './Cabinet.scss'

class Cabinet extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      data: [
        {id: 5, name: 'sdfsdd'},
        {id: 6, name: 'asdfsadf'},
        {id: 7, name: 'swerwerdfsdd'},
        {id: 8, name: 'fsdfsdrw'},
        {id: 9, name: 'sdfwersfxz'}
      ]
    }
  }

  componentDidMount () {
    this.props.getCabinetInfo()
  }

  render () {
    if (this.props.guest === true) {
      return <Redirect to="/login"/>
    }
    console.log('cabinet props', this.props)
    return (
      <Page>
        <h1>Кабинет</h1>
        {this.props.loading === Process.Start &&
        <Loader/>}
      </Page>
    )
  }
}

const mapState2Props = (state) => {
  console.log('state store cabinet', state)
  return {
    guest: state.user.guest,
    loading: state.cabinet.loading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    getCabinetInfo: () => dispatch(getCabinetInfo())
  }
}

export default connect(
  mapState2Props,
  mapDispatchToProps
)(Cabinet)
