import * as React from "react";
import { connect } from "react-redux";
import Page from "../../components/Page";
import { Row, Col, Badge, Card, CardHeader, CardBody, Button } from "reactstrap";
import { CheckoutLpcForm, AddLpcForm, AddAdsForm, CheckoutEthForm, GetContractForm, CheckoutIcoAdsForm, SellContractForm } from "../forms";
import RoubleAccount from "../components/RoubleAccount";
import "./Income.scss";
import AddsAccount from "../components/AddsAccount";
import Contracts from "../components/Contracts";

class Income extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Page>
        <div id="income">
          <h1>Финансовый кабинет</h1>
          <Card className="mt-4">
            <CardBody>
              <Row className="text-center">
                <Col>
                  <RoubleAccount />
                </Col>
                <Col>
                  <AddsAccount />
                </Col>
                <Col>
                  <Contracts />
                </Col>
              </Row>
            </CardBody>
          </Card>
          <Card className="mt-4">
            <CardHeader className="text-center">
              <h3 className="mx-auto">
                Баланс на вывод (LPC):<Badge color="primary">9.46579887</Badge>
              </h3>
            </CardHeader>
            <CardBody>
              <Row className="text-center">
                <Col>
                  <CheckoutLpcForm />
                </Col>
                <Col>
                  <AddLpcForm />
                </Col>
                <Col>
                  <AddAdsForm />
                </Col>
              </Row>
            </CardBody>
          </Card>
          <Card className="mt-4">
            <CardHeader className="text-center">
              <h3>
                Копилка (LPC): <Badge color="primary">4323.234</Badge>
              </h3>
            </CardHeader>
            <CardBody>
              <Row className="text-center">
                <Col>
                  <CheckoutEthForm />
                </Col>
                <Col>
                  <GetContractForm />
                </Col>
                <Col>
                  <CheckoutIcoAdsForm />
                </Col>
              </Row>
              <Row className="text-center">
                <Col />
                <Col>
                  <SellContractForm />
                </Col>
                <Col />
              </Row>
            </CardBody>
          </Card>
          <Card className="mt-4">
            <CardHeader className="text-center">
              <h3>
                Услуги за рекламные токены{" "}
                <Button outline className="question ml-1" size="sm" color="secondary">
                  ?
                </Button>
              </h3>
            </CardHeader>
            <CardBody />
          </Card>
        </div>
      </Page>
    );
  }
}

const mapState2Props = state => {
  return {};
};

export default connect(mapState2Props)(Income);
