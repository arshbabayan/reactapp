import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { getPaymentHistory } from "../redux/paymentsHistory";
import { Table } from "reactstrap";
import moment from "moment";

import "./PaymentHistory.scss";

class PaymentHistory extends Component {
  componentDidMount() {
    let dateEnd = moment().format("YYYY-MM-DD");
    let dateBegin = moment().subtract(3, 'days').format("YYYY-MM-DD");
    this.props.getPaymentHistory(dateBegin, dateEnd);
  }
  render() {
    if (this.props.guest === true) {
      return <Redirect to="/login" />;
    }
    console.log('payments history props', this.props);
    return (
      <div id="payment-history">
        <h1>История платежей</h1>
        <Table style={{ width: 800 }}>
          <thead>
            <tr>
              <th>Id</th>
              <th>Количество</th>
              <th>Тип</th>
              <th>Дата</th>
            </tr>
          </thead>
          <tbody>
            {this.props.paymentHistory.map((item, i) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.val}</td>
                  <td>{item.type}</td>
                  <td>{moment(item.created_at).format("DD.MM.YY HH:mm")}</td>
                </tr>
              );
            })}
          </tbody>
        </Table>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getPaymentHistory: (dateBegin, dateEnd) => {
      dispatch(getPaymentHistory(dateBegin, dateEnd));
    }
  };
};

const mapStateToProps = state => {
  return {
    guest: state.user.guest,
    paymentHistory: state.paymentHistory.history
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PaymentHistory);
