import * as React from 'react'
import * as ReactDOM from 'react-dom'
import { isNull } from 'util'
import axios from '../../axios'
import Card from '../../ui/Card'
import { setPath } from '../../helpers/functions'
import { connect } from 'react-redux'
import Page from '../../components/Page'
import Main from '../../layouts/Main'

class RefLinks extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      guest: null,
      user: {
        name: ''
      }
    }
    this.copy = this.copy.bind(this)
  }

  copy () {
    let range = document.createRange()
    range.selectNode(this.str)
    window.getSelection().addRange(range)
    try {
      document.execCommand('copy')
    } catch (err) {
      console.log('Can`t copy, boss')
    }
    window.getSelection().removeAllRanges()
  }

  componentDidMount () {
    if (!isNull(this.props.page) && this.props.page.hasOwnProperty('/cabinet/reflinks')) {
      this.setState((state) => {
        state.user.name = this.props.page['/cabinet/reflinks'].user.name
        return state
      })
    } else {
      axios.post('/cabinet/reflinks').then(data => data.data)
        .then(data => {
          console.log('reflinks', data)
          this.setState((state) => {
            state.user.name = data.user.name
            return state
          })
          this.props.setPageData({path: '/cabinet/reflinks', data})
        })
    }
  }

  render () {
    console.log('state reflinks', this.state, this.props)
    return (
      <Page>
        <h1 className='page-header'>Реферальные ссылки</h1>
        <div className='row'>
          <div className="col-12 col-lg-7 mx-auto mt-4">
            <Card title={<strong>Основная реферальная ссылка</strong>} footer=''>
              <span ref={span => { this.str = span }} id="url">{setPath('/partner/' + this.state.user.name)}</span>
              <button onClick={this.copy} className='btn btn-outline-secondary btnCopy'>
                <img src={setPath('/img/icons/copy.svg')}/>
              </button>
            </Card>
          </div>
        </div>
  </Page>
  )

  }
}

const mapState2Props = (state) => {
  return {
    guest: state.user.guest,
    page: state.page
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    // auth: (data) => dispatch(setAuthUser(data)),
  }
}

export default connect(mapState2Props, mapDispatchToProps)(RefLinks)
