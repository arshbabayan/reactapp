import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { Process, Node, Tree } from "../../helpers/types";
import Page from "../../components/Page";
import { Table, Button, Alert, Modal, ModalHeader, ModalBody } from "reactstrap";
import Profile from "../../ui/Profile";
import Loader from "../../ui/Loader";
import { getBinar, getBinarById, findPlace, refreshBinar } from "../redux/vip";
import withAuth from "../../hoc/withAuth";
import PanelVip from "../components/PanelVip";
import "./Vip.scss";

class Vip extends Component {
  constructor(props) {
    super(props);
    let data = [];
    this.state = { data: Vip.dataInit(), modal: false, id: null };
    console.clear();
    this.createBinar = this.createBinar.bind(this);
    this.selectProfile = this.selectProfile.bind(this);
    this.findPlace = this.findPlace.bind(this);
    this.refresh = this.refresh.bind(this);
    this.change = this.change.bind(this);
    this.modal = this.modal.bind(this);
  }

  componentDidMount() {
    console.log("vip props", this.props);
    if (this.props.match.params.id === undefined) {
      this.props.getBinar();
    } else {
      this.props.getBinarById(this.props.match.params.id);
    }
    let { binar } = this.props;
    console.log(this.props);
  }

  static dataInit() {
    let data = [];
    for (let i = 0; i <= 5; i++) {
      data[i] = [];
      for (let j = 0; j < Math.pow(2, i); j++) {
        data[i].push(null);
      }
    }
    return data;
  }

  modal() {
    this.setState({
      modal: !this.state.modal
    });
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    let data = Vip.dataInit();
    let id = null;
    if (nextProps.match.params.id !== undefined && nextProps.match.params.id !== prevState.id) {
      id = nextProps.match.params.id;
      nextProps.getBinarById(id);
      prevState = { ...prevState, id };
    }
    if (nextProps.loading === Process.Success && nextProps.binar.length > 0) {
      let tree;
      nextProps.binar.forEach((item, index) => {
        if (index === 0) {
          tree = new Tree(item);
        } else {
          tree.add(item);
        }
      });

      for (let key in tree.data) {
        let node = tree.data[key];
        data[node.row][node.col] = { name: node.data.name, id: node.data.id, type: node.data.type, payments: node.data.payments, user_id: node.data.user_id };
      }
      return { ...prevState, data };
    }
    return prevState;
  }

  selectProfile(id) {
    this.props.getBinarById(id);
  }

  refresh() {
    this.props.refresh();
  }

  createBinar() {
    const rotate = this.props.loadingRefresh === Process.Start ? "rotate" : "";
    const danger = this.props.loadingRefresh === Process.Error ? "text-danger" : "";
    let data = this.state.data;
    let table = [];
    for (let i = 0; i < 5; i++) {
      let rows = [];
      for (let j = 0; j < Math.pow(2, i); j++) {
        let item = data[i][j];
        let name = item === null ? "empty" : item.name;
        let id = item === null ? null : item.id;
        let type = item === null ? null : +item.type;
        let payments = item === null ? null : item.payments;
        let uid = item === null ? null : item.user_id;
        let point = (
          <td className="text-center" key={i + "" + j} colSpan={16 / Math.pow(2, i)}>
            <Profile uid={uid} type={type} id={id} name={name} payments={payments} />
            {i === 0 && (
              <div title="Обновить" onClick={this.refresh} className="refresh">
                <i className={`fas fa-sync-alt ${rotate} ${danger}`} />
              </div>
            )}
          </td>
        );
        rows.push(point);
      }
      table.push(<tr key={"row" + i}>{rows}</tr>);
    }
    return table;
  }

  findPlace() {
    this.props.findPlace();
  }

  change(value) {
    console.log("vip accounts change", value);
    value = parseInt(value);
    this.props.getBinarById(value);
  }

  render() {
    console.log("user payments vip", this.props.userPayments);
    if (this.props.guest === true) {
      return <Redirect to="/login" />;
    }
    return (
      <Page>
        <h1>VIP - МегаБинар</h1>
        {this.props.loading === Process.Start && <Loader />}
        {this.props.loading === Process.Error &&
          <Alert color="danger">{}</Alert>
        }
        {this.props.loading === Process.Success ? (
          <>
            {this.props.binar.length === 0 && <h3 className="text-center">Вы еще не купили тариф</h3>}
            <PanelVip change={this.change} name={this.props.name} accounts={this.props.accounts} />
            <Table id="vip-binar">
              <tbody>{this.props.loading === Process.Success && this.createBinar()}</tbody>
            </Table>
            <Table borderless style={{ width: 300 }}>
              <thead>
                <tr>
                  <th>id</th>
                  <th>name</th>
                  <th>val</th>
                </tr>
              </thead>
              <tbody>
                {this.props.userPayments.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>{item.user_id}</td>
                      <td>{item.name}</td>
                      <td>{item.val}</td>
                    </tr>
                  );
                })}
              </tbody>
            </Table>
          </>
        ) : null}
        {this.props.loading === Process.Error && <Alert color="danger">Произошла ошибка, попробуйте позже</Alert>}
        <Button onClick={this.findPlace} color="secondary">
          Find place
        </Button>
        {this.props.payments && (
          <>
            <Button style={{ marginLeft: 10 }} color="primary" onClick={this.modal}>
              Платежи
            </Button>
            <Modal isOpen={this.state.modal} toggle={this.modal}>
              <ModalHeader toggle={this.modal}>Платежи</ModalHeader>
              <ModalBody>
                <Table borderless striped>
                  <tbody>
                    <tr>
                      <td>Реферальные</td>
                      <td>
                        {this.props.payments.ref.name}(uid={this.props.payments.ref.uid})
                      </td>
                    </tr>
                    {this.props.payments.levels.map((item, i) => {
                      return (
                        <tr key={i}>
                          <td>Уровень {item.level}</td>
                          <td>
                            type = {item.type}, id = {item.id}, {item.name}({item.uid}) level = {item.level}{" "}
                          </td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              </ModalBody>
            </Modal>
          </>
        )}
      </Page>
    );
  }
}

Vip.propTypes = {
  binar: PropTypes.arrayOf(PropTypes.object),
  getBinar: PropTypes.func,
  getBinarById: PropTypes.func,
  guest: PropTypes.bool,
  findPlace: PropTypes.func
};

Vip.defaultProps = {
  binar: [],
  guest: null
};

const mapState2Props = (state, ownProps = { binar: [], userPayments: [] }) => {
  return {
    binar: state.vip.data || [],
    loading: state.vip.loading,
    guest: state.user.guest,
    loadingRefresh: state.vip.loadingRefresh,
    accounts: state.vip.accounts,
    name: state.vip.name,
    payments: state.vip.payments,
    userPayments: state.vip.userVipPayments || []
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getBinar: () => dispatch(getBinar()),
    refresh: () => dispatch(refreshBinar()),
    getBinarById: id => dispatch(getBinarById(id)),
    findPlace: () => dispatch(findPlace())
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(withAuth(Vip));
