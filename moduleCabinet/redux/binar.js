import { setAuth } from "../../redux/user";
import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";

const BINAR_GET_START = "BINAR_GET_START";
const BINAR_GET_SUCCESS = "BINAR_GET_SUCCESS";
const BINAR_GET_ERROR = "BINAR_GET_ERROR";

const initialState = {
  loading: null,
  error: null,
  binar: null
}

const reducer = (state = initialState, action) => {
  switch(action.type) {
    default: {
      return state;
    }
  }
}

reducerRegistry('binar', reducer);
