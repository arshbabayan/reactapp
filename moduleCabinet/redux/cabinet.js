import axios from "../../axios";
import reducerRegistry from "../../redux/reducerRegister";
import { Process } from "../../helpers/types";

export const CABINET_GET_START = "CABINET_GET_START";
export const CABINET_GET_SUCCESS = "CABINET_GET_SUCCESS";
export const CABINET_GET_ERROR = "CABINET_GET_ERROR";

export function getCabinetInfo() {
  return dispatch => {
    dispatch({ type: CABINET_GET_START });
    axios
      .post("/cabinet")
      .then(data => data.data)
      .then(data => {
        dispatch({ type: CABINET_GET_SUCCESS });
      })
      .catch(e => {
        dispatch({ type: CABINET_GET_ERROR, data: e });
      });
  };
}

const initialState = {
  loading: null,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case CABINET_GET_START: {
      return { ...state, loading: Process.Start };
    }
    case CABINET_GET_SUCCESS: {
      return { ...state, loading: Process.Success };
    }
    case CABINET_GET_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    default:
      return state;
  }
};

reducerRegistry.register("cabinet", reducer);
