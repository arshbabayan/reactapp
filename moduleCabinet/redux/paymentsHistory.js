import axios from "../../axios";
import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";

const PAYMENT_HISTORY_GET_START = "PAYMENT_HISTORY_GET_START";
const PAYMENT_HISTORY_GET_SUCCESS = "PAYMENT_HISTORY_GET_SUCCESS";
const PAYMENT_HISTORY_GET_ERROR = "PAYMENT_HISTORY_GET_ERROR";

export const getPaymentHistory = (dateBegin, dateEnd) => {
  return dispatch => {
    dispatch({ type: PAYMENT_HISTORY_GET_START });
    axios
      .post("/cabinet/payment_history", { dateBegin, dateEnd })
      .then(data => data.data)
      .then(data => {
        dispatch({ type: PAYMENT_HISTORY_GET_SUCCESS, data });
        // console.log("payment history data", data);
      })
      .catch(e => {
        dispatch({ type: PAYMENT_HISTORY_GET_ERROR, data: e.response.data.message });
      });
  };
};

const initialState = { process: null, error: null, history: null };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PAYMENT_HISTORY_GET_START:
      return { ...state, process: Process.Start };
    case PAYMENT_HISTORY_GET_SUCCESS:
      return { ...state, process: Process.Success, history: action.data.paymentHistory };
    case PAYMENT_HISTORY_GET_ERROR:
      return { ...state, process: Process.Error, error: action.data }
    default: {
      return state;
    }
  }
};

reducerRegistry.register("paymentHistory", reducer);
