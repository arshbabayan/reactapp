import axios from "../../axios";
import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";

const VIP_GET_START = "VIP_GET_START";
const VIP_GET_SUCCESS = "VIP_GET_SUCCESS";
const VIP_GET_ERROR = "VIP_GET_ERROR";

const VIP_GET_ID_START = "VIP_GET_ID_START";
const VIP_GET_ID_SUCCESS = "VIP_GET_ID_SUCCESS";
const VIP_GET_ID_ERROR = "VIP_GET_ID_ERROR";

const VIP_FIND_PLACE_GET = "VIP_FIND_PLACE_GET";
const VIP_FIND_PLACE_SUCCESS = "VIP_FIND_PLACE_SUCCESS";
const VIP_FIND_PLACE_ERROR = "VIP_FIND_PLACE_ERROR";

const VIP_REFRESH_START = "VIP_REFRESH_START";
const VIP_REFRESH_SUCCESS = "VIP_REFRESH_SUCCESS";
const VIP_REFRESH_ERROR = "VIP_REFRESH_ERROR";

let repeat = false;

export const getBinar = () => {
  repeat = false;
  return dispatch => {
    dispatch({ type: VIP_GET_START });
    axios
      .post("/cabinet/superbinar")
      .then(data => data.data)
      .then(data => {
        dispatch({ type: VIP_GET_SUCCESS, data });
      })
      .catch(e => {
        dispatch({
          type: VIP_GET_ERROR,
          data: e
        });
        if (repeat === false) {
          getBinar();
        }
        repeat = true;
      });
  };
};

export const refreshBinar = () => {
  repeat = false;
  return dispatch => {
    dispatch({ type: VIP_REFRESH_START });
    axios
      .post("/cabinet/superbinar")
      .then(data => data.data)
      .then(data => {
        dispatch({ type: VIP_REFRESH_SUCCESS, data });
      })
      .catch(e => {
        dispatch({
          type: VIP_REFRESH_ERROR,
          data: e
        });
        repeat = true;
      });
  };
};

export const getBinarById = id => {
  return dispatch => {
    dispatch({ type: VIP_GET_ID_START });
    axios
      .post(`/cabinet/superbinar/${id}`)
      .then(data => data.data)
      .then(data => {
        dispatch({ type: VIP_GET_ID_SUCCESS, data });
      })
      .catch(e => dispatch({ type: VIP_GET_ID_ERROR, data: e }));
  };
};

export const findPlace = () => {
  return dispatch => {
    console.log("find place start");
    dispatch({ type: VIP_FIND_PLACE_GET });
    axios
      .post("/cabinet/findPlace")
      .then(data => data.data)
      .then(data => {
        console.log("find place data", data);
        dispatch({ type: VIP_FIND_PLACE_SUCCESS, data });
      })
      .catch(e => {
        console.error("error", e);
        dispatch({ type: VIP_FIND_PLACE_ERROR, data: e.response.data.message });
      });
  };
};

const initialState = { data: [], error: null, loading: null, loadingRefresh: null, name: null, accounts: [], payments: null, userVipPayments: [] };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case VIP_FIND_PLACE_GET:
      return { ...state, loading: Process.Start };
    case VIP_GET_START:
      return { ...state, loading: Process.Start };
    case VIP_GET_SUCCESS:
      return {
        ...state,
        loading: Process.Success,
        data: action.data.binar,
        accounts: action.data.accounts,
        name: action.data.name,
        userVipPayments: action.data.userVipPayments
      };
    case VIP_GET_ERROR:
      return { ...state, loading: Process.Error, error: action.data };
    case VIP_GET_ID_START:
      return { ...state, loading: Process.Start, data: [] };
    case VIP_GET_ID_SUCCESS:
      state.data = [];
      return { ...state, loading: Process.Success, data: action.data.binar };
    case VIP_GET_ID_ERROR:
      return { ...state, loadin: Process.Error, data: action.data };
    case VIP_FIND_PLACE_SUCCESS:
      return { ...state, loading: Process.Success, data: action.data.binar, payments: action.data.payments, userVipPayments: action.data.userVipPayments };
    case VIP_FIND_PLACE_ERROR:
      return { ...state, error: action.data, loading: Process.Error };
    case VIP_REFRESH_START:
      return { ...state, loadingRefresh: Process.Start, data: [] };
    case VIP_REFRESH_SUCCESS:
      state.data = [];
      return { ...state, loadingRefresh: Process.Success, data: action.data.binar };
    case VIP_REFRESH_ERROR:
      return { ...state, loadingRefresh: Process.Error, error: action.data };
    default:
      return state;
  }
};

reducerRegistry.register("vip", reducer);
