import * as React from "react";
import { Route, Switch } from "react-router-dom";
import News from "./pages/News";
import Friends from "./pages/Friends";
import Messages from "./pages/Messages";
import ProfileInfo from "./pages/ProfileInfo";
import ProfileEdit from "./pages/ProfileEdit";
import Profile from "./pages/Profile";

export default class IndexComponent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Switch>
        <Route exact path="/socnet/news" component={News} />
        <Route path="/socnet/friends/:page?" component={Friends} />
        <Route path="/socnet/messages" component={Messages} />
        <Route path="/socnet/profile/info/:id" component={ProfileInfo} />
        <Route path="/socnet/profile/edit" component={ProfileEdit} />
        <Route path="/socnet/profile" component={Profile} />
      </Switch>
    );
  }
}
