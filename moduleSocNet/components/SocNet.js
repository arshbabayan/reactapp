import * as React from "react";
import { connect } from "react-redux";
import { NavLink } from "react-router-dom";
import "./SocNet.scss";
import { setPathImg } from "../../helpers/functions";
import Page from "../../components/Page";

class SocNet extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profileImg: setPathImg("/profile.png"),
      query: false
    };
  }

  render() {
    let active = {};

    let path = window.location.pathname;
    return (
      <Page>
        <div id="socnet" className="container">
          <div className="left-column">
            <div className="left-column-menu">
              <div>
                <NavLink activeClassName="active" to="/socnet/profile">
                  Мой профиль
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/socnet/news">
                  Новости
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/socnet/messages">
                  Сообщения
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/socnet/friends">
                  Друзья
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/socnet/groups">
                  Группы
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/socnet/photo">
                  Фото
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/video">
                  Видео
                </NavLink>
              </div>
              <div>
                <NavLink activeClassName="active" to="/settings">
                  Настройки
                </NavLink>
              </div>
            </div>
          </div>
          <div id={this.props.id} className="center-column">
            {this.props.children}
          </div>

          <div className="right-column">{this.props.rightColumn}</div>
        </div>
      </Page>
    );
  }
}

const mapState2Props = state => {
  return {};
};

export default connect(mapState2Props)(SocNet);
