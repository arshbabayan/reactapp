import * as React from "react";
import { connect } from "react-redux";
import SocNet from "../components/SocNet";
import InputCheckBox from "../../ui/InputCheckbox";
import "./Friends.scss";
import ProfileCard from "../../components/ProfileCard";
import { Input, Label, FormGroup, Col } from "reactstrap";
import Paginator from "../../ui/Paginator";
import { withRouter, RouteComponentProps, Redirect } from "react-router";
import Modal from "../../ui/Modal";
import FormMsg from "../../components/forms/FormMsg";
import { getFriends } from "../redux/friends";
import Loader from "../../ui/Loader";
import { userSelect, clearMessage } from "../redux/messages";
import withAuth from "../../hoc/withAuth";
import { Process } from "../../helpers/types";

class Friends extends React.Component {
  constructor(props) {
    super(props);
    console.clear();
    this.state = {
      listFriends: [],
      friends: [],
      search: "",
      filterParams: { mentor: true, partner: true },
      count: null,
      recipient: 0,
      send: false
    };
    this.search = this.search.bind(this);
    this.filter = this.filter.bind(this);
    this.check = this.check.bind(this);
    this.enterMsg = this.enterMsg.bind(this);
    this.modalTitle = this.modalTitle.bind(this);
    this.modalMsgClose = this.modalMsgClose.bind(this);
  }

  componentDidMount() {
    this.props.getFriends();
  }

  search(e) {
    this.setState({ search: e.target.value });
  }

  check(id, checked) {
    console.log("check", id, checked);
    this.setState(state => {
      let filterParams = { ...this.state.filterParams, [id]: checked };
      return { ...state, filterParams };
    });
  }

  rightColumn() {
    return (
      <div className="position-fixed">
        <FormGroup row>
          <Label for="search" sm={2}>Поиск</Label>
          <Col sm={10}>
            <Input onChange={this.search} id="search" label="Имя" />
          </Col>
        </FormGroup>
        <InputCheckBox change={this.check} checked={true} label="Партнер" id="partner" />
        <InputCheckBox change={this.check} checked={true} label="Наставник" id="mentor" />
      </div>
    );
  }

  filter() {
    let friends = this.state.listFriends;
    let mentorFilter = this.state.filterParams.mentor;
    let partnerFilter = this.state.filterParams.partner;
    let curPage = parseInt(this.props.match.params["page"], 10);
    curPage = isNaN(curPage) ? 1 : curPage;
    console.log("state filter");
    friends = friends.filter(item => {
      if (item.name === null) {
        return false;
      }
      if (mentorFilter === false && item.type === "mentor") {
        return false;
      }

      if (partnerFilter === false && item.type === "partner") {
        return false;
      }

      if (this.state.search.length > 2) {
        return item.name.toLowerCase().indexOf(this.state.search.toLowerCase()) > -1;
      }
      return true;
    });
    let count = friends.length;
    friends = friends.slice((curPage - 1) * 20, curPage * 20);
    return { friends, count };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    // console.log('get derived state from props', nextProps, prevState);
    if (prevState.listFriends.length === 0) {
      return {
        listFriends: nextProps.friends,
        friends: nextProps.friends.slice(0, 19)
      };
    }

    return false;
  }

  enterMsg(user) {
    this.setState({ recipient: user });
    console.log("enter msg user", user);
    $("#modalMsg").modal();
  }

  modalTitle() {
    return (
      <div className="row">
        <div className="col-md-6">
          <h5>Написать сообщение</h5>
        </div>
        <div className="col-md-6">
          <h5>{this.state.recipient.name}</h5>
        </div>
      </div>
    );
  }

  /**
   * Закрытие окна сообщений
   */
  modalMsgClose() {
    this.props.clearMsg();
  }

  render() {
    if (this.props.guest === true) {
      return <Redirect to="/login" />;
    }
    let friends = this.filter();
    return (
      <SocNet id="friends" rightColumn={this.rightColumn()}>
        {this.props.loading === Process.Start && <Loader />}
        {this.props.loading && (
          <>
            <div>
              {friends.friends.map((item, index) => {
                let type = "";
                if (item.type === "partner") {
                  type = "партнер";
                }
                if (item.type === "mentor") {
                  type = "Наставник";
                }
                return <ProfileCard id={item.id} msg={() => this.enterMsg(item)} key={index} title={item.name} type={type} />;
              })}
            </div>
            <Paginator path="/friends" count={friends.count} limit={20} />
            <Modal close={this.modalMsgClose} isFooter={false} okBtn={false} id="modalMsg" save="Отправить" title={this.modalTitle()}>
              <FormMsg recipient={this.state.recipient.id} />
            </Modal>
          </>
        )}
      </SocNet>
    );
  }
}

Friends.defaultProps = {
  friends: []
};

const mapState2Props = state => {
  return {
    guest: state.user.guest,
    friends: state.friends.data,
    loading: state.friends.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getFriends: () => dispatch(getFriends()),
    userMsgSelect: user => dispatch(userSelect(user)),
    clearMsg: () => dispatch(clearMessage())
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(withAuth(Friends));
