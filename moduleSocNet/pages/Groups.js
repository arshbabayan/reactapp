import * as React from "react";
import { connect } from "react-redux";
import SocNet from "../../moduleSocNet/components/SocNet";
import Tabs from "../../ui/tabs/Tabs";
import TabContent from "../../ui/tabs/TabContent";
import withAuth from "../../hoc/withAuth";
import { Link } from "react-router-dom";

import './Groups.scss';

class Groups extends React.Component {
    rightColumn() {
        return null;
    }

    tabsContent() {
        return (
            <>
                <TabContent key='tab1' id="tab1" active={true}>
                    <div>tab1</div>
                </TabContent>
                <TabContent key='tab2' id="tab2">
                    <div>tab 2</div>
                </TabContent>
            </>
        );
    }
    render() {
        return (
            <SocNet id="groups" rightColumn={this.rightColumn()}>
                <h1>Группы<Link to='/create' id='createGroup' className='btn btn-primary btn-sm'>Создать</Link></h1>
                <Tabs id="group-tabs" content={this.tabsContent()}
                    tabs={[{ title: "Группы", id: "tab1" }, { title: "Мои группы", id: "tab2" }]} />
            </SocNet>
        );
    }
}

const mapState2Props = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapState2Props)(withAuth(Groups));
