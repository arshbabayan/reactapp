import * as React from "react";
import { connect } from "react-redux";
import { messagesGet } from "../redux/messages";
import Loader from "../../ui/Loader";
import SocNet from "../components/SocNet";
import withAuth from "../../hoc/withAuth";
import {Process} from "../../helpers/types";

class Messages extends React.Component {
    componentDidMount() {
        this.props.getMsg();
    }

    rightColumn() {
        return null;
    }

    render() {
        console.log('message props', this.props);
        if (this.props.loading === Process.Start) {
            return <Loader />;
        }
        if (this.props.loading === Process.Success) {
            return (
                <SocNet id='messages' rightColumn={this.rightColumn()}>
                    <h1>Сообщения</h1>
                </SocNet>
            );
        }
        return null;
    }
}

const mapState2Props = (state) => {
    return {
        loading: state.messages.loadingGet
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        getMsg: () => dispatch(messagesGet())
    };
};

export default connect(
    mapState2Props,
    mapDispatchToProps
)(withAuth(Messages));
