import * as React from "react";
import { connect } from "react-redux";
import SocNet from "../components/SocNet";
import { getNews } from "../redux/news";
import Loader from "../../ui/Loader";
import { Process } from '../../helpers/types'
import withAuth from "../../hoc/withAuth";

class INews extends React.Component {
  constructor(props) {
    super(props)
  }

  componentDidMount() {
    this.props.getNews()
  }

  rightColumn() {
    return <div />
  }

  render() {
    if (this.props.loading === Process.Start) {
      return <Loader />;
    }
    if (this.props.loading === Process.Success) {
      return (
        <SocNet id={this.props.id} rightColumn={this.rightColumn()}>
          <h1>Новости</h1>
        </SocNet>
      );
    }
    return null;
  }
}

const mapState2Props = state => {
  return {
    guest: state.user.guest,
    loading: state.news.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getNews: () => dispatch(getNews())
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(withAuth(INews));
