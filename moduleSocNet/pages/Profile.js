import * as React from 'react';
import {connect} from 'react-redux';
import SocNet from '../components/SocNet';
import {setPathImg} from '../../helpers/functions';
import './Profile.scss';
import Card from '../../ui/Card';
import axios from '../../axios';

class Profile extends React.Component {
	constructor(props) {
		super(props);
		// this.setState({firstname: data.user[1].value, secondname: data.user[2].value});
	}

	rightColumn() {
		return (
			<div>
				<div className='text-center'>
					{/* <img height='200' width='200' src={this.state.profileImg}/> */}
				</div>
			</div>
		);
	}

	title() {
		return (
			<div>
				{/* {this.state.firstname} {this.state.secondname} */}
			</div>
		);
	}

	footer() {
		return (
			<div>
				footer
			</div>
		);
	}

	render() {
		if (this.props.guest === true) {
			return false;
		}
		let panoStyle = {
			// backgroundImage: `url(${this.state.imgPano})`,
			backgroundRepeat: 'no-repeat',
			backgroundSize: 'cover',
			backgroundPosition: 'center'
		};
		return (
			<SocNet id='profile' rightColumn={this.rightColumn()}>
				<div className='pano-wrap' style={panoStyle}>
				</div>
				<Card title={this.title()} footer={this.footer()}>

				</Card>
			</SocNet>
		);
	}
}

const mapState2Props = (state) => {
	console.log('profile map state to props', state);
	return {
		guest: state.user.guest
	};
};

export default connect(mapState2Props)(Profile);
