import * as React from "react";
import { connect } from "react-redux";
import { getFields } from "../redux/profile";
import "./ProfileEdit.scss";
import FormBaseInfo from "../../components/forms/FormBaseInfo";
import FormAvatar from "../../components/forms/FormAvatar";
import FormContacts from "../../components/forms/FormContacts";
import Page from "../../components/Page";
import Main from "../../layouts/Main";
import Loader from "../../ui/Loader";
import { Redirect } from "react-router";
import { profileEdit } from "../redux/profileEdit";
import withAuth from "../../hoc/withAuth";
import { Process } from "../../helpers/types";

class ProfileEdit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.profileEdit();
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    console.log("profile edit next props", nextProps);
    return prevState;
  }

  render() {
    console.log("props profile edit", this.props, this.state);

    if (this.props.loading === Process.Success) {
      return (
        <Page>
          <div id="profile-edit">
            <nav className="mx-auto">
              <div className="nav nav-tabs" role="tablist">
                <a className="nav-item nav-link active" href="#base_info" role="tab" data-toggle="tab">
                  Основная информация
                </a>
                <a className="nav-item nav-link " href="#contact_info" role="tab" data-toggle="tab">
                  Контактная информация
                </a>
              </div>
            </nav>
            <div className="tab-content">
              <div className="tab-pane fade show active" id="base_info" role="tabpanel">
                <FormAvatar />
                <FormBaseInfo />
              </div>
              <div className="tab-pane fade" id="contact_info" role="tabpanel">
                <FormContacts />
              </div>
            </div>
          </div>
        </Page>
      );
    }
    return <Loader />;
  }
}

const mapState2Props = state => {
  return {
    loading: state.profileEdit.loading,
    guest: state.user.guest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    profileEdit: () => dispatch(profileEdit()),
    getFields: () => dispatch(getFields())
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(withAuth(ProfileEdit));
