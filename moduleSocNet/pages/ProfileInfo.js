import * as React from "react";
import { connect } from "react-redux";
import SocNet from "../components/SocNet";
import { Process } from "../../helpers/types";
import { getProfileInfo } from "../redux/profileInfo";
import Loader from "../../ui/Loader";
import { Card, CardBody, Table } from "reactstrap";

class ProfileInfo extends React.Component {
  componentDidMount() {
    console.log("profile info id", this.props);
    this.props.getProfileInfo(this.props.match.params.id);
  }

  rightColumn() {
    return <div />;
  }

  render() {
    console.log("profile info", this.props.info);

    return (
      <SocNet id="profile-info" rightColumn={this.rightColumn()}>
        {this.props.loading === Process.Start && <Loader />}
        {this.props.loading === Process.Success && (
          <Card style={{marginTop: 30}}>
            <CardBody>
              <Table borderless striped>
                <tbody>
                  {this.props.info.map((item, i) => {
                    return (
                      <tr key={i}>
                        <td>{item.caption}</td>
                        <td>{item.value}</td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </CardBody>
          </Card>
        )}
      </SocNet>
    );
  }
}

const mapState2Props = state => {
  return {
    loading: state.profileInfo.loading,
    info: state.profileInfo.info
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getProfileInfo: id => dispatch(getProfileInfo(id))
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(ProfileInfo);
