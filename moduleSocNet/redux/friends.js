import axios from "../../axios";
import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";

export const FRIENDS_GET_START = "FRIENDS_GET_START";
export const FRIENDS_GET_SUCCESS = "FRIENDS_GET_SUCCESS";
export const FRIENDS_GET_ERROR = "FRIENDS_GET_ERROR";

export function getFriends() {
  return dispatch => {
    dispatch({ type: FRIENDS_GET_START });
    axios
      .post("/friends")
      .then(data => {
        return data.data;
      })
      .then(data => {
        dispatch({ type: FRIENDS_GET_SUCCESS, data });
      })
      .catch(e => {
        dispatch({ type: FRIENDS_GET_ERROR, data: e });
      });
  };
}

const initialState = {
  friends: [],
  loading: null,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FRIENDS_GET_START: {
      return { ...state, loading: Process.Start };
    }
    case FRIENDS_GET_SUCCESS: {
      return { ...state, loading: Process.Success, data: action.data.friends };
    }
    case FRIENDS_GET_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    default:
      return state;
  }
};

reducerRegistry.register("friends", reducer);
