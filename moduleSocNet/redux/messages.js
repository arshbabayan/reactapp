import axios from "../../axios";
import reducerRegistry from "../../redux/reducerRegister";
import { Process } from "../../helpers/types";

export const MESSAGES_SEND_START = "MESSAGES_SEND_START";
export const MESSAGES_SEND_SUCCESS = "MESSAGES_SEND_SUCCESS";
export const MESSAGES_SEND_ERROR = "MESSAGES_SEND_ERROR";

export const MESSAGES_GET_START = "MESSAGES_GET_START";
export const MESSAGES_GET_SUCCESS = "MESSAGES_GET_SUCCESS";
export const MESSAGES_GET_ERROR = "MESSAGES_GET_ERROR";

export const MESSAGES_USER_SELECT = "MESSAGES_USER_SELECT";

export const MESSAGES_CLEAR = "MESSAGES_CLEAR";

export function messageSend(msg) {
  return dispatch => {
    dispatch({ type: MESSAGES_SEND_START });
    axios
      .post("/msg/send", { msg })
      .then(data => {
        console.log("msg data", data.data);
        dispatch({ type: MESSAGES_SEND_SUCCESS, data });
      })
      .catch(e => dispatch({ type: MESSAGES_SEND_ERROR, data: e }));
  };
}

export function messagesGet() {
  return dispatch => {
    dispatch({ type: MESSAGES_GET_START });
    axios
      .post("/messages")
      .then(data => {
        dispatch({ type: MESSAGES_GET_SUCCESS, data: [] });
      })
      .catch(e => {
        dispatch({ type: MESSAGES_GET_ERROR, data: e });
      });
  };
}

export function userSelect(user) {
  return { type: MESSAGES_USER_SELECT, data: user };
}

export function clearMessage() {
  return { type: MESSAGES_CLEAR };
}

const initialState = {
  data: null,
  error: null,
  loading: null,
  loadingGet: null,
  errorGet: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case MESSAGES_SEND_START: {
      return { ...state, loading: Process.Start };
    }
    case MESSAGES_SEND_SUCCESS: {
      return { ...state, loading: Process.Success };
    }
    case MESSAGES_SEND_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    case MESSAGES_USER_SELECT: {
      return { ...state, user: action.data };
    }
    case MESSAGES_CLEAR: {
      return { ...state, loading: null, error: null };
    }
    case MESSAGES_GET_START: {
      return { ...state, loadingGet: Process.Start };
    }
    case MESSAGES_GET_SUCCESS: {
      return { ...state, loadingGet: Process.Success };
    }
    case MESSAGES_GET_ERROR: {
      return { ...state, loading: Process.Error, errorGet: action.data };
    }
    default: {
      return state;
    }
  }
};

reducerRegistry.register("messages", reducer);
