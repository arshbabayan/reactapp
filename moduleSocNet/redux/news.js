import axios from "../../axios";
import reducerRegistry from "../../redux/reducerRegister";
import { Process } from "../../helpers/types";

export const NEWS_GET_START = "NEWS_GET_START";
export const NEWS_GET_SUCCESS = "NEWS_GET_SUCCESS";
export const NEWS_GET_ERROR = "NEWS_GET_ERROR";

export const getNews = () => {
  return dispatch => {
    dispatch({ type: NEWS_GET_START });
    axios
      .post("/news")
      .then(data => {
        dispatch({ type: NEWS_GET_SUCCESS, data: data.data });
      })
      .catch(e => {
        dispatch({ type: NEWS_GET_ERROR, data: e });
      });
  };
};

const initialState = {
  error: null,
  loading: null,
  data: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case NEWS_GET_START: {
      return { ...state, loading: Process.Start };
    }
    case NEWS_GET_SUCCESS: {
      return { ...state, loading: Process.Success };
    }
    case NEWS_GET_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    default: {
      return state;
    }
  }
};
reducerRegistry.register("news", reducer);
