import axios from "../../axios";
import reducerRegistry from "../../redux/reducerRegister";
import { Process } from "../../helpers/types";

export const GET_FIELDS_START = "GET_FIELDS_START";
export const GET_FIELDS_SUCCESS = "GET_FIELDS_SUCCESS";
export const GET_FIELDS_ERROR = "GET_FIELDS_ERROR";

export const SAVE_FIELDS_START = "SAVE_FIELDS_START";
export const SAVE_FIELDS_SUCCESS = "SAVE_FIELDS_SUCCESS";
export const SAVE_FIELDS_ERROR = "SAVE_FIELDS_ERROR";

export const SAVE_CONTACTFIELDS_START = "SAVE_CONTACTFIELDS_START";
export const SAVE_CONTACTFIELDS_SUCCESS = "SAVE_CONTACTFIELDS_SUCCESS";
export const SAVE_CONTACTFIELDS_ERROR = "SAVE_CONTACTFIELDS_ERROR";

export const PROFILE_UPLOAD_AVATAR_START = "PROFILE_UPLOAD_AVATAR_START";
export const PROFILE_UPLOAD_AVATAR_SUCCESS = "PROFILE_UPLOAD_AVATAR_SUCCESS";
export const PROFILE_UPLOAD_AVATAR_ERROR = "PROFILE_UPLOAD_AVATAR_ERROR";

export const PROFILE_CLEAR_LOADING = "PROFILE_CLEAR_LOADING";

export function getFields() {
  return dispatch => {
    dispatch({ type: GET_FIELDS_START });
    axios
      .post("/profile/getFields")
      .then(data => data.data)
      .then(data => {
        dispatch({ type: GET_FIELDS_SUCCESS, data });
      })
      .catch(e => dispatch({ type: GET_FIELDS_ERROR, data: e.response.data }));
  };
}

export function saveFields(data) {
  return dispatch => {
    dispatch({ type: SAVE_FIELDS_START });
    axios
      .post("/profile/saveBaseInfo", data)
      .then(data => data.data)
      .then(data => dispatch({ type: SAVE_FIELDS_SUCCESS, data }))
      .catch(e => dispatch({ type: SAVE_FIELDS_ERROR, data: e }));
  };
}

export function saveFieldsContact(data) {
  return dispatch => {
    dispatch({ type: SAVE_CONTACTFIELDS_START });
    axios
      .post("/profile/saveContactInfo", data)
      .then(data => data.data)
      .then(data => dispatch({ type: SAVE_CONTACTFIELDS_SUCCESS, data }))
      .catch(e => dispatch({ type: SAVE_CONTACTFIELDS_ERROR, data: e.response.data }));
  };
}

export function uploadImg(data) {
  return dispatch => {
    dispatch({ type: PROFILE_UPLOAD_AVATAR_START });
    let formData = new FormData();
    formData.append("avatar", data);
    axios
      .post("/profile/uploadImg", formData)
      .then(data => data.data)
      .then(data => dispatch({ type: PROFILE_UPLOAD_AVATAR_SUCCESS, data }))
      .catch(e => dispatch({ type: PROFILE_UPLOAD_AVATAR_ERROR, data: e }));
  };
}

export function clearLoading() {
  return { type: PROFILE_CLEAR_LOADING };
}

let initialState = {
  loading: null,
  loadingSaveContact: null,
  loadingAvatar: null,
  loadingSave: null,
  error: null,
  fields: [],
  contactFields: [],
  avatar: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_FIELDS_START: {
      return { ...state, loading: Process.Start };
    }

    case GET_FIELDS_SUCCESS: {
      return {
        ...state,
        loading: Process.Success,
        fields: action.data.fields,
        contactFields: action.data.contactFields,
        avatar: action.data.avatar
      };
    }

    case GET_FIELDS_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }

    case SAVE_FIELDS_START: {
      return { ...state, loadingSave: Process.Start };
    }

    case SAVE_FIELDS_SUCCESS: {
      return { ...state, loadingSave: Process.Success, fields: action.data.fields, avatar: action.data.avatar };
    }

    case SAVE_FIELDS_ERROR: {
      return { ...state, loadingSave: Process.Error, error: action.data };
    }

    case SAVE_CONTACTFIELDS_START: {
      return { ...state, loadingSaveContact: Process.Start };
    }

    case SAVE_CONTACTFIELDS_SUCCESS: {
      return { ...state, loadingSaveContact: Process.Success, contactFields: action.data.contactFields };
    }

    case SAVE_CONTACTFIELDS_ERROR: {
      return { ...state, loadingSaveContact: Process.Error, data: action.data };
    }

    case PROFILE_CLEAR_LOADING: {
      return { ...state, loadingSave: null, loading: null, loadingSaveContact: null };
    }

    case PROFILE_UPLOAD_AVATAR_START: {
      return { ...state, loadingAvatar: Process.Start };
    }

    case PROFILE_UPLOAD_AVATAR_SUCCESS: {
      return { ...state, loadingAvatar: Process.Success, avatar: action.data.avatar };
    }

    case PROFILE_UPLOAD_AVATAR_ERROR: {
      return { ...state, loadingAvatar: Process.Error, error: action.data };
    }

    default: {
      return state;
    }
  }
};

reducerRegistry.register("profile", reducer);
