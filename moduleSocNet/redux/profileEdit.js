import axios from "../../axios";
import { setAuth } from "../../redux/user";
import { Process } from "../../helpers/types";
import reducerRegistry from "../../redux/reducerRegister";

export const PROFILE_EDIT_GET_START = "PROFILE_EDIT_GET_START";
export const PROFILE_EDIT_GET_SUCCESS = "PROFILE_EDIT_GET_SUCCESS";
export const PROFILE_EDIT_GET_ERROR = "PROFILE_EDIT_GET_ERROR";

export const PROFILE_EDIT_SAVE_BASEINFO_START = "PROFILE_EDIT_SAVE_BASEINFO_START";
export const PROFILE_EDIT_SAVE_BASEINFO_SUCCESS = "PROFILE_EDIT_SAVE_BASEINFO_SUCCESS";
export const PROFILE_EDIT_SAVE_BASEINFO_ERROR = "PROFILE_EDIT_SAVE_BASEINFO_ERROR";

export const PROFILE_EDIT_CLEAR_BASEINFO = "PROFILE_EDIT_CLEAR_BASEINFO";

export function profileEdit() {
  return dispatch => {
    dispatch({ type: PROFILE_EDIT_GET_START });
    axios
      .post("/profile/edit")
      .then(data => data.data)
      .then(data => {
        console.log("profile edit before set auth", data);
        try {
        } catch (e) {
          console.log(e);
        }
        console.log("profile edit after set auth");
        dispatch({ type: PROFILE_EDIT_GET_SUCCESS, data });
      })
      .catch(e => dispatch({ type: PROFILE_EDIT_GET_ERROR, data: e }));
  };
}

export function saveBaseInfo(data) {
  return dispatch => {
    dispatch({ type: PROFILE_EDIT_SAVE_BASEINFO_START });
    axios
      .post("/profile/saveBaseInfo", data)
      .then(data => data.data)
      .then(data => dispatch({ type: PROFILE_EDIT_SAVE_BASEINFO_SUCCESS, data }))
      .catch(e => dispatch({ type: PROFILE_EDIT_SAVE_BASEINFO_ERROR, data: e }));
  };
}

export function clearBaseInfo() {
  return { type: PROFILE_EDIT_CLEAR_BASEINFO };
}

const initialState = {
  loading: null,
  error: null,
  fields: [],
  avatar: null,
  user: null,
  saveBaseInfo: { loading: null, error: null }
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PROFILE_EDIT_GET_START: {
      return { ...state, loading: Process.Start };
    }
    case PROFILE_EDIT_GET_SUCCESS: {
      return {
        ...state,
        loading: Process.Success,
        fields: action.data.fields,
        avatar: action.data.avatar,
        user: action.data.user
      };
    }
    case PROFILE_EDIT_GET_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    case PROFILE_EDIT_SAVE_BASEINFO_START: {
      let saveBaseInfo = { loading: Process.Start, error: null };
      return { ...state, saveBaseInfo };
    }
    case PROFILE_EDIT_SAVE_BASEINFO_SUCCESS: {
      let saveBaseInfo = { loading: Process.Success, error: null };
      return { ...state, saveBaseInfo };
    }
    case PROFILE_EDIT_SAVE_BASEINFO_ERROR: {
      let saveBaseInfo = { loading: Process.Error, error: action.data.error };
      return { ...state, saveBaseInfo };
    }
    case PROFILE_EDIT_CLEAR_BASEINFO: {
      let saveBaseInfo = { loading: null, error: null };
      return { ...state, saveBaseInfo };
    }
    default:
      return state;
  }
};

reducerRegistry.register("profileEdit", reducer);
