import axios from "../../axios";
import reducerRegistry from "../../redux/reducerRegister";
import { Process } from "../../helpers/types";

const PROFILE_INFO_GET_START = "PROFILE_INFO_GET_START";
const PROFILE_INFO_GET_SUCCESS = "PROFILE_INFO_GET_SUCCESS";
const PROFILE_INFO_GET_ERROR = "PROFILE_INFO_GET_ERROR";

export function getProfileInfo(id) {
  return dispatch => {
    dispatch({ type: PROFILE_INFO_GET_START });
    axios
      .post(`/profile/info/${id}`)
      .then(data => data.data)
      .then(data => {
        dispatch({ type: PROFILE_INFO_GET_SUCCESS, data });
      })
      .catch(e => {
        dispatch({ type: PROFILE_INFO_GET_ERROR, data: e });
      });
  };
}

const initialState = {
  info: null,
  error: null,
  loading: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PROFILE_INFO_GET_START: {
      return { ...state, loading: Process.Start };
    }

    case PROFILE_INFO_GET_SUCCESS: {
      return { ...state, loading: Process.Success, info: action.data.info };
    }

    case PROFILE_INFO_GET_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }

    default: {
      return state;
    }
  }
};

reducerRegistry.register("profileInfo", reducer);
