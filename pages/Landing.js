import * as React from 'react'
import { connect } from 'react-redux'
import Header from '../layouts/Header'
import { setPath, setPathImg } from '../helpers/functions'
import './Landing.scss'
import { Link } from 'react-router-dom'
import Footer from '../layouts/Footer'
import Page from '../components/Page'
import landing from '../redux/landing'
import Loader from '../ui/Loader'
import { Process } from '../helpers/types'

class Landing extends React.Component {
  constructor (props) {
    super(props)
  }

  componentDidMount () {
    console.log('component did mount landing');
    this.props.landing()
  }

  render () {
    if (this.props.loading === Process.Start) {
      return (<Loader />)
    }

    return (
      <Page>
        <Header/>
        <div id='landing'>
          <div className='main-section'>
            <div className=''>
              <img width="100%" src={setPathImg('/main.svg')}/>
            </div>
            <div className='container'>
              <div className='d-inline-block text-center'>
                <h1 className='page-title'>LeoPays</h1>

                <div className='text-title'>
                  <div className='text-center'>социальная сеть</div>
                  <div className='text-center'>с автомайнингом</div>
                </div>
                <div>* не требует установки приложений и программ</div>

                <div className='row'>
                  <div className='col'>
                    {this.props.guest &&
                    <Link className='btn btn-main' to='/register'>Регистрация</Link>
                    }
                  </div>
                  <div className='col'>
                    {this.props.guest &&
                    <Link className='btn btn-main' to='/login'>Вход</Link>
                    }
                  </div>
                </div>
              </div>
            </div>
            <div className=''>
              <img width="100%" src={setPathImg('/main2.svg')}/>
            </div>
          </div>
          <div className='container'>
            <div className='row'>
              <h2 className='mx-auto text-center'>Автоматический социальный майнинг</h2>
            </div>
            <div className='line'></div>
            <div className='row'>
              <h3 className='mx-auto text-center'>Уникальная функция социальной сети не требует:</h3>
            </div>
            <div className='row'>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/invetno.png')}/>
                </div>
                <div className='addons-column'>
                  <div>Начальных</div>
                  <div>инвестиций</div>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/dom.png')}/>
                </div>
                <div className="addons-column">
                  <div>Аренды</div>
                  <div>помещений</div>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/rabot.png')} alt=""/>
                </div>
                <div className="addons-column">
                  <div>Наемных</div>
                  <div>работников</div>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/electrik.png')} alt=""/>
                </div>
                <div className="addons-column">
                  <div>Электро-</div>
                  <div>энергии</div>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/regacom.png')} alt=""/>
                </div>
                <div className="addons-column">
                  <div>Регистрации</div>
                  <div>бизнеса</div>
                </div>
              </div>
              <div className="col-md-2">
                <div>
                  <img src={setPath('/img/srok.png')} alt=""/>
                </div>
                <div className="addons-column">
                  <div>Срока</div>
                  <div>окупаемости</div>
                </div>
              </div>
            </div>
            <div className="row d-none d-md-block">
              <h3 className='mx-auto text-center'>Схема работы социального майнинга</h3>
              <img width='100%' src={setPath('/img/sociallmnings.png')} alt=""/>
            </div>
            <div className="row text-center">
                            <span className='main-text'>В результате работы протокола <strong>PoSa</strong>
                                (proof of social activity) система в круглосуточном режиме отслеживает и учитывает активность пользователей социальной сети:
                                лайки, комментарии, размещение постов, время проведенное на платформе
                        </span>
            </div>
            <div className='row'>
              <h3 className='text-center'>Социальный майнинг доступен абсолютно каждому</h3>
              <p className='font-weight-bold text-center'>
                Играй в игры, смотри видео ролики, читай ленту, развивай группы, ставь лайки,
                коментируй интересные посты и получай за свою активность криптомонету LeoPaysCoin.
              </p>
            </div>
            <div className="row">
              <a href='#' style={{whiteSpace: 'normal'}} className='btn btn-info font-weight-bold'>Подробнее о функции
                                                                                                   "Социальный майнинг"</a>
            </div>

            <div className="row">
              <h3 className='mx-auto'>Программа бонусных вознаграждений</h3>
            </div>

            <div className="row">
              <p className='font-weight-bold mx-auto'>Для пользователей социальной сети работает 10-уровневая
                                                      партнерская программа.</p>
            </div>

            <div className="row">
              <h2 className='mx-auto'>Инструменты автоматизации</h2>
            </div>
            <div className="line"></div>
            <div className="row align-center">
              <div className="col-md-6">
                <div className='text-right'>
                  <div>
                    <h4 className='text-uppercase'>Автоматизация E-mail</h4>
                  </div>
                  <div className='main-text'>Серии писем, автоответчик, рандомизация писем</div>
                  <Link to='#' className='btn btn-primary text-uppercase'>Перейти на сервис</Link>
                </div>

              </div>
              <div className='col-md-6'>
                <img className='rounded' src={setPathImg('/maillp.png')} alt=""/>
              </div>
            </div>
            <div className="row align-center">
              <div className="col-md-6">
                <img className='rounded' src={setPathImg('/iglp.png')} alt=""/>
              </div>
              <div className="col-md-6">
                <div>
                  <h4 className='text-uppercase'>Автоматизация INSTAGRAM</h4>
                </div>
                <div className='main-text'>
                  Автопостинг, лайкинг, фоловинг, директ-сообщения и т.д.
                </div>
                <Link to='#' className='text-uppercase btn btn-primary'>Перейти на сервис</Link>
              </div>
            </div>

            <div className="row align-center">
              <div className="col-md-6">
                <div className='text-right'>
                  <div>
                    <h4 className='text-uppercase'>Автоматизация Facebook</h4>
                  </div>
                  <div className='main-text'>Автопостинг, добавления в друзья, вступления в группы и т.д.</div>
                  <Link to='#' className='btn btn-primary text-uppercase'>Перейти на сервис</Link>
                </div>
              </div>
              <div className="col-md-6">
                <img className='rounded' src={setPathImg('/fblp.png')} alt=""/>
              </div>
            </div>

            <div className="row align-center">
              <div className="col-md-6">
                <img className='rounded' src={setPathImg('/hostlp.png')} alt=""/>
              </div>
              <div className="col-md-6">
                <div>
                  <h4 className='text-uppercase'>Хостинг, домены, SSL</h4>
                </div>
                <div className='main-text'>
                  Хостинг 5 Гб + 10 доменов + FREE SSL .
                </div>
                <Link to='#' className='text-uppercase btn btn-primary'>Перейти на сервис</Link>
              </div>
            </div>

            <div className="row">
              <h2 className='text-uppercase mx-auto'>Последние новости</h2>
            </div>
            <div className="line"></div>
            <div className='row'>
              <div className="col-md-4">
                <h4 className='text-uppercase last-news-title'>
                  <Link to='#'>Кошелёк “Staking Wallet” - возможности и перспективы.</Link>
                </h4>
                <div className='last-news-date'>08 октября 2018</div>
              </div>
              <div className="col-md-4">
                <h4 className='text-uppercase last-news-title'>
                  <Link to='#'>Статистика "Бессрочного промоушена"</Link>
                </h4>
                <div className='last-news-date'>04 октября 2018</div>
              </div>
              <div className="col-md-4">
                <h4 className='text-uppercase last-news-title'>
                  <Link to='#'>Технические работы на платформе</Link>
                </h4>
                <div className='last-news-date'>26 сентября 2018</div>
              </div>
            </div>

            <div className="row">
              <h2 className='text-uppercase mx-auto'>Рассказать о нас</h2>
            </div>
            <div className="line"></div>
          </div>
          <div className="" style={{backgroundImage: 'url("/img/footer.svg")'}}>
            <div className='soc-wrap d-flex mx-auto '>
              <div className='d-inline mx-auto'>
                <div className='soc-item fb'>
                  <a href='#'><i className='fa fa-facebook'></i></a>
                </div>
                <div className='soc-item tw'>
                  <a href='#'><i className='fa fa-twitter'></i></a>
                </div>
                <div className='soc-item gp'>
                  <a href='#'><i className='fa fa-google-plus'></i></a>
                </div>
                <div className='soc-item li'>
                  <a href='#'><i className='fa fa-linkedin-square'></i></a>
                </div>
                <div className='soc-item pi'>
                  <a href='#'><i className='fa fa-pinterest'></i></a>
                </div>
                <div className='soc-item tu'>
                  <a href='#'><i className='fa fa-tumblr'></i></a>
                </div>
                <div className='soc-item gp'>
                  <a href='#'><i className='fa fa-get-pocket'></i></a>
                </div>
                <div className='soc-item rd'>
                  <a href='#'><i className='fa fa-reddit'></i></a>
                </div>
                <div className='soc-item vk'>
                  <a href='#'><i className='fa fa-vk'></i></a>
                </div>
              </div>
            </div>
          </div>
          {/* <div className='row'>
                    <img width="100%" src={setPathImg('/footer.svg')} />
                </div> */}
          <Footer/>
        </div>

      </Page>
    )
  }
}

const mapState2Props = (state) => {
  return {
    guest: state.user.guest,
    loading: state.landing.loading
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    landing: () => dispatch(landing())
  }
}

export default connect(mapState2Props, mapDispatchToProps)(Landing)
