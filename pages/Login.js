import * as React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import Loader from "../ui/Loader";
import FormLogin from "../components/forms/FormLogin";
import Page from "../components/Page";
import { showLogin } from "../redux/login";
import { Process } from "../helpers/types";
import { Col, Row, Alert } from "reactstrap";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null
    };
  }

  componentDidMount() {
    this.props.login();
  }

  render() {
    return (
      <Page>
        <Row>
          {this.props.loading === Process.Start ? (
            <div className="mx-auto mt-5">
              <Loader />
            </div>
          ) : (
            <Col xs="12" lg="7" className="mx-auto">
              {this.state.error && <Alert color="danger">{this.state.error}</Alert>}
              {this.props.loading === Process.Success && <FormLogin captcha={recaptcha} />}
            </Col>
          )}
        </Row>
      </Page>
    );
  }
}

const mapState2Props = state => {
  return {
    guest: state.user.guest,
    loading: state.login.loading
  };
};

const mapDispatchToProps = dispatch => {
  return {
    login: () => dispatch(showLogin())
    // clearLoading: () => dispatch(clearLoading())
  };
};

Login.propTypes = {
  loading: PropTypes.number,
  login: PropTypes.func
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(Login);
