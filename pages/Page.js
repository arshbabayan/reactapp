import * as React from 'react';
import { connect } from 'react-redux';

class Page extends React.Component{
    render() {
        return (
            <>
                {this.props.children}
            </>
        );
    }
}

const mapState2Props = (state) => {
    return {
    };
};

export default connect(mapState2Props)(Page);
