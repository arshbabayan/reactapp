import * as React from "react";
import PropTypes from 'prop-types';
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { setUserPartner } from "../redux/user";

class Partner extends React.Component {
  componentWillMount() {
    console.log('set partner props', this.props);
    this.props.setPartner(this.props.match.params.name);
  }

  render() {
    return <Redirect to="/register" />;
  }
}

const mapState2Props = state => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    setPartner: name => dispatch(setUserPartner(name))
  };
};

Partner.typeProps = {
  setPartner: PropTypes.func,
  match: PropTypes.object
}

export default connect(
  mapState2Props,
  mapDispatchToProps
)(Partner);
