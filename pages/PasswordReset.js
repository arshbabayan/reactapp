import * as React from "react";
import Input from "../ui/Input";
import { Form, Alert, Button, Col, Card, CardBody, Row } from "reactstrap";
import { connect } from "react-redux";
import { passwordReset, passwordResetClear } from "../redux/passwordReset";
import Loader from "../ui/Loader";
import Page from "../components/Page";
import { Process } from "../helpers/types";
import {getDataForm} from "../helpers/functions";
class PasswordReset extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
  }

  submit(e) {
    e.preventDefault();
    let data = getDataForm(this.form);
    this.props.passwordReset(data);
  }

  render() {
    return (
      <Page>
        <Row className="justify-content-cener">
          <Col md="8" className="mx-auto">
            {this.props.loading === Process.Start ? (
              <Loader />
            ) : (
              <Card>
                <CardBody>
                  {this.props.loading === Process.Success ? (
                    <Alert color="success">Ваш пароль восстановлен</Alert>
                  ) : (
                    <Form innerRef={form => (this.form = form)} onSubmit={this.submit}>
                      <input type="hidden" name="token" value={this.props.match.params.token} />
                      <Input filter={{ mail: "" }} type="text" id="email" label="Почта" name="email" />

                      <Input type="password" label="Пароль" name="password" id="password" />

                      <Input type="password" label="Повторите пароль" name="password_confirmation" id="password_confirm" />
                      <Button color="primary" className="mx-auto d-block">Подтвердить</Button>
                    </Form>
                  )}
                </CardBody>
              </Card>
            )}
          </Col>
        </Row>
      </Page>
    );
  }
}

const mapState2Props = state => {
  return {
    loading: state.passwordReset.loading,
    error: state.passwordReset.error,
    guest: state.user.guest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    passwordReset: data => {
      dispatch(passwordReset(data));
    },
    passwordResetClear: () => {
      dispatch(passwordResetClear());
    }
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(PasswordReset);
