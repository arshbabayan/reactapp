import * as React from "react";
import * as ReactDOM from "react-dom";
import Page from "../components/Page";
import Main from "../layouts/Main";

export default class RegistrConfirm extends React.Component {
  constructor(props) {}

  render() {
    return (
      <Page>
        <>
          <h1 className="page-header">Регистрация</h1>
          <div className="row justify-content-center">
            <div className="col-md-8">
              <h3 className="mt-3">Спасибо за регистрацию</h3>
              <div className="alert alert-success" role="alert">
                Вам было отправлено письмо. Для подтверждения регистрации перейдите по ссылке, указанной в письме
              </div>
            </div>
          </div>
        </>
      </Page>
    );
  }
}
