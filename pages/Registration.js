import * as React from "react";
import PropTypes from "prop-types";
import axios from "../axios";
import Input from "../ui/Input";
import { getDataForm, setPath } from "../helpers/functions";
import { setAuth } from "../redux/user";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import Page from "../components/Page";
import { Card, CardBody, CardHeader } from "reactstrap";

class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.submit = this.submit.bind(this);
    this.state = {
      error: "",
      redirectSubmit: false,
      isQuery: false
    };
    this.inputChange = this.inputChange.bind(this);
  }

  submit(e) {
    e.preventDefault();
    console.log("registration");
    let data = getDataForm(this.form);
    if (data.password !== data.password_confirmation) {
      this.setState({ error: "Пароли не совпадают" });
      return false;
    }

    for (let name in data) {
      if (data[name] === "" && name !== "partner") {
        this.setState({ error: `Поле ${name} не может быть пустым` });
        return false;
      }
    }
    this.setState({ isQuery: true });
    console.log("data registration", data);
    axios.post("/register", data).then(data => {
      console.log("/register", data);
      if (data.data.error !== undefined) {
        this.setState({ error: data.data.error, isQuery: false });
      } else {
        let { guest } = data.data;
        this.props.setAuth(guest);
        this.setState({ redirectSubmit: true, isQuery: false });
      }
    });
    // .catch(e => {
    //   if (e === undefined) {
    //     console.log("e undefined");
    //     this.setState({ isQuery: false });
    //     return;
    //   }
    //   console.error(e);
    //   let { errors } = e.response.data;
    //   let dataErr = { isQuery: false };
    //   for (let field in errors) {
    //     if (field in fields) {
    //       dataErr.error = fields[field] + ": " + parseErrors(errors[field]);
    //     }
    //   }
    //   this.setState(dataErr);
    // });
  }

  inputChange() {
    if (this.state.error !== "") {
      this.setState({ error: "" });
    }
    // console.log('registr form', e.target.name, e.target.value);
  }

  render() {
    if (this.props.guest === false) {
      return <Redirect to="/" />;
    }
    return (
      <Page>
        {this.state.redirectSubmit && <Redirect to="/register/confirm" />}
        <div className="row">
          {this.state.isQuery ? (
            <div className="mx-auto mt-4">
              <img src={setPath("/img/spinner.svg")} />
            </div>
          ) : (
            <div className="col-12 col-lg-8 mx-auto mt-4">
              {this.state.error && (
                <div className="alert alert-danger" role="alert">
                  {this.state.error}
                </div>
              )}

              <Card width={800}>
                <CardHeader>
                  <h3>Регистрация</h3>
                </CardHeader>
                <CardBody>
                  <form ref={form => (this.form = form)} onSubmit={this.submit}>
                    <Input filter={{ min: 3, max: 20, reg: /[а-яА-Яa-zA-z]{3,20}/ }} change={this.inputChange} type="text" id="input-name" name="firstname" label="Имя" />
                    <Input filter={{ min: 3, max: 80, reg: /[а-яА-Яa-zA-z]{3,80}/ }} change={this.inputChange} type="text" id="input-lastname" name="lastname" label="Фамилия" />
                    <Input
                      filter={{ min: 3, max: 20, reg: /[a-zA-Z_0-9]{3,20}/ }}
                      change={this.inputChange}
                      type="text"
                      id="input-login"
                      name="name"
                      label="Логин *"
                      msg="Используйте только английский язык, логин без пробелов."
                    />
                    <Input
                      filter={{ mail: null }}
                      id="email"
                      type="text"
                      name="email"
                      label="Email"
                      msg="Временно не уходят письма на mail.ru, bk.ru. Не используйте почту @i.ua, @ua.fm, @gmail.ru."
                    />
                    <Input
                      change={this.inputChange}
                      id="input-partner-login"
                      type="text"
                      name="partner"
                      disabled
                      value={this.props.partner}
                      label="Логин партнера"
                      msg="Логин человека, который Вас пригласил."
                    />
                    <Input filter={{ min: 6, max: 20 }} change={this.inputChange} id="password" type="password" name="password" label="Пароль" />
                    <Input
                      filter={{ min: 6, max: 20 }}
                      change={this.inputChange}
                      id="password-confirm"
                      type="password"
                      name="password_confirmation"
                      label="Повторите пароль *"
                      msg="* - Согласен со всеми правилами пользовательского соглашения."
                    />
                    <div className="form-group row mb-0">
                      <div className="com-md-8 offset-md-4">
                        <button className="btn btn-primary" type="submit">
                          Зарегистрироваться
                        </button>
                      </div>
                    </div>
                  </form>
                </CardBody>
              </Card>
            </div>
          )}
        </div>
      </Page>
    );
  }
}

Registration.propTypes = {
  guest: PropTypes.bool,
  setAuth: PropTypes.func,
  partner: PropTypes.string
};

const mapState2Props = state => {
  return {
    partner: state.user.partner.name,
    guest: state.user.guest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setAuth: guest => dispatch(setAuth(guest))
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(Registration);
