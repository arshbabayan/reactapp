import * as React from "react";
import * as ReactDOM from "react-dom";
import Input from "../ui/Input";
import { Card, CardHeader, CardBody, Row, Form, Button } from "reactstrap";
import { connect } from "react-redux";
import { sendEmail, sendEmailClear } from "../redux/passwordReset";
import Loader from "../ui/Loader";
import { Redirect } from "react-router-dom";
import { Process } from "../helpers/types";

class ResetEmail extends React.Component {
  constructor(props) {
    super(props);
    this.change = this.change.bind(this);
    this.submit = this.submit.bind(this);
    this.state = { email: "" };
  }

  submit(e) {
    e.preventDefault();
    this.props.sendEmail(this.state)
  }

  change(name, value) {
    this.setState({ email: value });
    console.log(name, value);
  }

  render() {
    return (
      <>
        <div id="err" />
        {this.props.loading === Process.Start ? (
          <Loader />
        ) : (
          <Card style={{ width: 500 }} className="mx-auto">
            <CardHeader>
              <h3>Восстановление пароля</h3>
            </CardHeader>
            <CardBody>
              <Row>
                {this.props.loading === Process.Success ? (
                  <div className="alert alert-primary">Вам на почту выслано письмо, перейдите по ссылке для восстановления пароля</div>
                ) : (
                  <Form onSubmit={this.submit} className="w-100">
                    <Input change={this.change} filter={{ mail: "" }} type="string" name="mail" id="mail" label="Email" value={this.state.email} />
                    <Button className="mx-auto d-block" color="primary">Восстановить пароль</Button>
                  </Form>
                )}
              </Row>
            </CardBody>
          </Card>
        )}
      </>
    );
  }
}

const mapState2Props = state => {
  return {
    loading: state.passwordReset.loading,
    error: state.passwordReset.error,
    guest: state.user.guest
  };
};

const mapDispatchToProps = dispatch => {
  return {
    sendEmail: data => {
      dispatch(sendEmail(data));
    },
    sendEmailClear: () => {
      dispatch(sendEmailClear());
    }
  };
};

export default connect(
  mapState2Props,
  mapDispatchToProps
)(ResetEmail);
