import * as React from "react";
import * as ReactDOM from "react-dom";
import axios from "../axios";
import Main from "../layouts/Main";
import Page from "../components/Page";

export default class Verified extends React.Component {
    constructor(props) {

        return (
            <Page>
                <>
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">Account verified</div>
                            </div>
                        </div>
                    </div>
                </>
            </Page>
        );
    }
}


