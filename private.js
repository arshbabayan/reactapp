import * as React from "react";
import * as ReactDOM from "react-dom";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from "./configurStore";
import Login from "./pages/Login";
import ResetEmail from "./pages/ResetEmail";
import PasswordReset from "./pages/PasswordReset";
import Registration from "./pages/Registration";
import Partner from "./pages/Partner";
import Verified from "./pages/Verify";
import RegistrConfirm from "./pages/RegistrConfirm";
import Landing from "./pages/Landing";
import Profile from "./pages/Profile";
import Friends from "./pages/SocNet/Friends";
import ProfileEdit from "./pages/ProfileEdit";
import Cabinet from "./pages/cabinet/Cabinet";
import RefLinks from "./pages/cabinet/RefLinks";
import Investor from "./pages/cabinet/Investor";
import Income from "./pages/cabinet/Income";
import Team from "./pages/cabinet/Team";
import Binar from "./pages/cabinet/Binar";
import Vip from "./pages/cabinet/Vip";
import ProfileInfo from "./pages/SocNet/ProfileInfo";
import { ConnectedRouter } from "connected-react-router";
import { createBrowserHistory } from "history";
import Main from "./layouts/Main";
import News from "./pages/SocNet/News";
import Messages from "./pages/SocNet/Messages";
import Groups from "./pages/SocNet/Groups";

let store = configureStore({
    friends: { data: [], loading: null, error: null },
    dataQuery: { data: { captcha: "" } },
    profileEdit: { loading: null, saveBaseInfo: { loading: null, error: null }, user: null, saveContactInfo: { loading: null, error: null } },
    news: { loading: null, error: null },
    user: { guest: null },
    page: null,
    messages: { data: "", loading: null, error: null, loadingGet: null, errorGet: null },
    cabinet: { loading: null }
});

class Public extends React.Component<any, any> {
    render() {
        const history = createBrowserHistory();
        return (
            <Router basename="/">
                <ConnectedRouter history={history}>
                    <>
                        <Route exact path="/" component={Landing} />
                        <Main>
                            <Switch>
                                {/* Cabinet */}
                                <Route path="/cabinet/reflinks" component={RefLinks} />
                                <Route path="/cabinet/investor" component={Investor} />
                                <Route path="/cabinet/income" component={Income} />
                                <Route path="/cabinet/team" component={Team} />
                                <Route path="/cabinet/binar" component={Binar} />
                                <Route path="/cabinet/vip" component={Vip} />
                                <Route path="/cabinet" component={Cabinet} />
                                <Route path="/profile/edit" component={ProfileEdit} />
                                <Route path="/profile/info/:id" component={ProfileInfo} />
                                <Route path="/profile" component={Profile} />
                                {/* SocNet */}
                                <Route path="/friends/:page?" component={Friends} />
                                <Route path="/news" component={News} />
                                <Route path="/messages" component={Messages} />
                                <Route path="/partner/:name" component={Partner} />
                                <Route path="/groups" component={Groups} />
                                {/* Auth */}
                                <Route path="/password/reset/:token" component={PasswordReset} />
                                <Route path="/resetemail" component={ResetEmail} />
                                <Route path="/register/confirm" component={RegistrConfirm} />
                                <Route path="/register" component={Registration} />
                                <Route path="/email/verify" component={Verified} />
                                <Route path="/login" component={Login} />

                                <Route
                                    path="/"
                                    render={() => {
                                        return (
                                            <div>
                                                <h1>Страница не найдена</h1>
                                            </div>
                                        );
                                    }}
                                />
                            </Switch>
                        </Main>
                    </>
                </ConnectedRouter>
            </Router>
        );
    }
}

if (document.getElementById("app")) {
    ReactDOM.render(
        <Provider store={store}>
            <Public />
        </Provider>,
        document.getElementById("app")
    );
}
