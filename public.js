import React, { Component, Suspense } from "react";
import * as ReactDOM from "react-dom";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import { Provider } from "react-redux";
import configureStore from "./configureStore";
import Login from "./pages/Login";
import ResetEmail from "./pages/ResetEmail";
import PasswordReset from "./pages/PasswordReset";
import Registration from "./pages/Registration";
import RegistrConfirm from "./pages/RegistrConfirm";
import Partner from "./pages/Partner";
import Verified from "./pages/Verify";
import Landing from "./pages/Landing";
import { ConnectedRouter, connectRouter } from "connected-react-router";
import { createBrowserHistory } from "history";
import Main from "./layouts/Main";
import Loader from "./ui/Loader";
import RouteAdmin from "./routeAdmin";
import Error from "./components/Error";

const Cabinet = React.lazy(() => import("./moduleCabinet/Index"));
const SocNet = React.lazy(() => import("./moduleSocNet/Index"));

export const store = configureStore({
  login: { loading: null, loadingLogin: null, error: null },
  landing: { loading: null, error: null },
  // friends: { data: [], loading: null, error: null },
  dataQuery: { data: { captcha: "" } },
  cabinet: { loading: null, error: null },
  vip: { loading: null, loadingRefresh: null, data: [], error: null, accaunts: [] },
  profileEdit: {
    loading: null,
    saveBaseInfo: { loading: null, error: null },
    user: null,
    saveContactInfo: { loading: null, error: null }
  },
  profileInfo: { loading: null, info: null, error: null },
  news: { loading: null, error: null },
  user: { guest: null, partner: { name: "" } },
  page: null,
  messages: { data: "", loading: null, error: null },
  paymentHistory: { history: [] },
  "admin.forms": { loading: null, error: null, forms: null, fields: null }
});

class Public extends React.Component {
  render() {
    const history = createBrowserHistory();
    return (
      <>
        <Router basename="/">
          <ConnectedRouter history={history}>
            <Suspense fallback={<Loader />}>
              <Switch>
                <Route exact path="/" component={Landing} />
                {/* Cabinet */}
                {/*<Route path="/cabinet/income" component={Income}/>*/}

                {/* SocNet */}
                <Route path="/admin" render={props => <RouteAdmin {...props} />} />
                <Main>
                  <Route path="/socnet" component={props => <SocNet {...props} />} />
                  <Route
                    path="/cabinet"
                    component={props => {
                      console.log("cabinet route store", store.getState());
                      return (
                        <Error>
                          <Cabinet {...props} />
                        </Error>
                      );
                    }}
                  />
                  <Route path="/partner/:name" component={Partner} />

                  <Route path="/password/reset/:token" component={PasswordReset} />
                  <Route path="/resetemail" component={ResetEmail} />
                  <Route path="/register/confirm" component={RegistrConfirm} />
                  <Route path="/register" component={Registration} />
                  <Route path="/email/verify" component={Verified} />
                  <Route path="/login" component={Login} />
                </Main>
                <Route path="/" component={() => <div>page not found</div>} />
              </Switch>
            </Suspense>
          </ConnectedRouter>
        </Router>
      </>
    );
  }
}

if (document.getElementById("app")) {
  ReactDOM.render(
    <Provider store={store}>
      <Public />
    </Provider>,
    document.getElementById("app")
  );
}
