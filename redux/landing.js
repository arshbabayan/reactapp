import axios from '../axios'
import { setAuth } from './user'
import reducerRegistry from './reducerRegister'
import {Process} from "../helpers/types";

export const LANDING_GET_START = 'LANDING_GET_START'
export const LANDING_GET_SUCCESS = 'LANDING_GET_SUCCESS'
export const LANDING_GET_ERROR = 'LANDING_GET_ERROR'

export default function landing () {
  return (dispatch) => {
    dispatch({type: LANDING_GET_START})
    axios.post('/').then(data => {
      dispatch(setAuth(data.data.guest))
      dispatch({type: LANDING_GET_SUCCESS, data: data.data})
    }).catch(e => {
      dispatch({type: LANDING_GET_ERROR, data: e})
    })
  }
}

const initialState = {
  loading: null,
  error: null
}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LANDING_GET_START:
      return {...state, loading: Process.Start}
    case LANDING_GET_SUCCESS:
      return {...state, loading: Process.Success}
    case LANDING_GET_ERROR:
      return {...state, loading: Process.Error}
    default:
      return state
  }
}

reducerRegistry.register('landing', reducer);