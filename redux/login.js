import axios from "../axios";
import { setAuth } from "./user";
import reducerRegistry from "./reducerRegister";
import { Process } from "../helpers/types";

export const LOGIN_SET_START = "LOGIN_SET_START";
export const LOGIN_SET_SUCCESS = "LOGIN_SET_SUCCESS";
export const LOGIN_SET_ERROR = "LOGIN_SET_ERROR";

export const LOGIN_START = "LOGIN_START";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";

export const LOGIN_SHOW_START = "LOGIN_SHOW_START";
export const LOGIN_SHOW_SUCCESS = "LOGIN_SHOW_SUCCESS";
export const LOGIN_SHOW_ERROR = "LOGIN_SHOW_ERROR";

export const LOGIN_CLEAR_LOADING = "LOGIN_CLEAR_LOADING";

// export function login() {
//     return (dispatch) => {
//         console.log('login start');
//         dispatch({ type: LOGIN_SET_START });
//         axios.post('/login').then(data => {
//             console.log('login axios', data);
//             dispatch(setAuth(data.data.guest));
//             dispatch({ type: LOGIN_SET_SUCCESS, data: data.data });
//         }).catch(e => {
//             dispatch({ type: LOGIN_SET_ERROR, data: e });
//         });
//     };
// }

export function showLogin() {
  return dispatch => {
    dispatch({ type: LOGIN_SHOW_START });
    axios
      .post("/loginshow")
      .then(data => data.data)
      .then(data => {
        console.log("show login ", data);
        dispatch(setAuth(data.guest));
        dispatch({ type: LOGIN_SHOW_SUCCESS, data });
      })
      .catch(e => {
        dispatch({ type: LOGIN_SHOW_ERROR, data: e });
      });
  };
}

export const loginPost = login => {
  return dispatch => {
    dispatch({ type: LOGIN_START });
    axios
      .post("/login", login)
      .then(data => data.data)
      .then(data => {
        console.log("axios login", data);
        dispatch(setAuth(data.guest));
        dispatch({ type: LOGIN_SUCCESS });
      })
      .catch(e => {
        console.log("axios login error", e);
        dispatch({ type: LOGIN_ERROR, data: e });
      });
  };
};

export const clearLoading = () => {
  return { type: LOGIN_CLEAR_LOADING };
};

const initialState = {
  loading: null,
  loadingLogin: null,
  error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN_SHOW_START: {
      return { ...state, loading: Process.Start };
    }
    case LOGIN_SHOW_SUCCESS: {
      return { ...state, loading: Process.Success };
    }
    case LOGIN_SHOW_ERROR: {
      return { ...state, loading: Process.Error, error: action.data };
    }
    case LOGIN_START: {
      return { ...state, loadingLogin: Process.Start };
    }
    case LOGIN_SUCCESS: {
      return { ...state, loadingLogin: Process.Success };
    }
    case LOGIN_ERROR: {
      return { ...state, loadingLogin: Process.Error, data: action.data };
    }
    case LOGIN_CLEAR_LOADING: {
      return { ...state, loadingLogin: null };
    }
    default:
      return state;
  }
};

reducerRegistry.register("login", reducer);
