import reducerRegistry from "./reducerRegister";

const SET_LPC = 'SET_LPC';

export const setLpc = (lpc) => {
  return { type: SET_LPC, val: lpc };
}

const reducer = (state = { val: null }, action) => {
  switch(action.type) {
    case SET_LPC:
      return { ...state, val: action.val }
    default: {
      return state;
    }
  }
}

reducerRegistry.register('lpc', reducer);
