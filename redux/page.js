import ReducerRegistry from './reducerRegister';
const SET_PAGE_DATA = 'SET_PAGE_DATA';

export const setPageData = (data) => {
    return { type: SET_PAGE_DATA, data };
};


const reducer = (state = {}, action) => {
  switch (action.type) {
    case SET_PAGE_DATA: {
      let item = {};
      item[action.data.path] = action.data.data;
      return { ...state, ...item };
    }
    default: {
      return state;
    }
  }
}

ReducerRegistry.register('page', reducer);
