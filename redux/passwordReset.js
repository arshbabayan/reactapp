import axios from '../axios'
import ReducerRegistry from './reducerRegister';
import {Process} from "../helpers/types";

export const SEND_EMAIL_START = 'SEND_EMAIL_START'
export const SEND_EMAIL_SUCCESS = 'SEND_EMAIL_SUCCESS'
export const SEND_EMAIL_ERROR = 'SEND_EMAIL_ERROR'
export const SEND_EMAIL_CLEAR = 'SEND_EMAIL_CLEAR'
export const RESET_PASSWORD_START = 'RESET_PASSWORD_START'
export const RESET_PASSWORD_SUCCESS = 'RESET_PASSWORD_SUCCESS'
export const RESET_PASSWORD_ERROR = 'RESET_PASSWORD_ERROR'
export const RESET_PASSWORD_CLEAR = 'RESET_PASSWORD_CLEAR'

export function sendEmail (data) {
  console.log('send email data', data)
  return (dispatch) => {
    dispatch(sendEmailStart())
    axios.post('/password/email', data)
      .then(data => data.data)
      .then(data => {
        dispatch(sendEmailSuccess(data))
      }).catch(error => {
      dispatch(sendEmailError(error))
    })
  }
}

export function sendEmailStart () {
  return {type: SEND_EMAIL_START}
}

export function sendEmailSuccess (data) {
  return {type: SEND_EMAIL_SUCCESS, data}
}

export function sendEmailError (error) {
  return {type: SEND_EMAIL_ERROR, data: error}
}

export function sendEmailClear () {
  return {type: SEND_EMAIL_CLEAR}
}

export function passwordReset (data) {
  return (dispatch) => {
    dispatch(passwordResetStart())
    axios.post('/password/reset', data).then(data => {
      dispatch(passwordResetSuccess())
    }).catch(e => {
      dispatch(passwordResetError(e.response.data))
    })
  }
}

export function passwordResetStart () {
  return {type: RESET_PASSWORD_START}
}

export function passwordResetSuccess () {
  return {type: RESET_PASSWORD_SUCCESS}
}

export function passwordResetError (data) {
  return {type: RESET_PASSWORD_ERROR, data}
}

export function passwordResetClear () {
  return {type: RESET_PASSWORD_CLEAR}
}

const reducer = (state = {loading: null, error: null}, action) => {
  switch (action.type) {
    case SEND_EMAIL_START: {
      return {...state, loading: Process.Start}
    }

    case SEND_EMAIL_SUCCESS: {
      return {...state, res: action.data, loading: Process.Success}
    }

    case SEND_EMAIL_ERROR: {
      return {...state, error: action.data, loading: Process.Error}
    }

    case SEND_EMAIL_CLEAR: {
      return {...state, loading: null}
    }

    case RESET_PASSWORD_START: {
      return {...state, loading: Process.Start}
    }

    case RESET_PASSWORD_SUCCESS: {
      return {...state, loading: Process.Success}
    }

    case RESET_PASSWORD_ERROR: {
      return {...state, loading: Process.Error, error: action.data}
    }

    case RESET_PASSWORD_CLEAR: {
      return {...state, loading: null}
    }

    default:
      return state
  }
}

ReducerRegistry.register('passwordReset', reducer);
