import axios from "../axios";
import reducerRegistry from "./reducerRegister";
import { Process } from "../helpers/types";

export const SET_USER = "SET_USER";
export const SET_USER_AUTH_START = "SET_USER_AUTH_START";
export const SET_USER_AUTH_SUCCESS = "SET_USER_AUTH_SUCCESS";
export const SET_USER_AUTH_ERROR = "SET_USER_AUTH_ERROR";
export const SET_USER_AUTH = "SET_USER_AUTH";
export const SET_USER_PARTNER = "SET_USER_PARTNER";

export const USER_LOGOUT = "USER_LOGOUT";

export const logout = () => {
  return dispatch => {
    axios
      .post("/logout")
      .then(data => data.data)
      .then(data => {
        location.href = "/login";
      });
  };
};

// export const setAuthUser = (auth): {type: string, data} => {
//     return { type: SET_USER_AUTH, data: auth };
// };

export const setUserPartner = name => {
  return { type: SET_USER_PARTNER, data: name };
};

// export function setAuthUser() {
//   let isLoad = false;
//   return dispatch => {
//     dispatch({ type: SET_USER_AUTH_START });
//     axios
//       .post(window.location.pathname)
//       .then(data => data.data)
//       .then(data => {
//         isLoad = true;
//         dispatch(setDataQuery(data));
//         dispatch({ type: SET_USER_AUTH_SUCCESS, data: data.guest });
//         return true;
//       })
//       .catch(e => {
//         !isLoad && dispatch({ type: SET_USER_AUTH_ERROR, data: e });
//       });
//   };
// }

export const setAuth = guest => {
  return { type: SET_USER_AUTH, data: guest };
};

export const setUser = user => {
  return { type: SET_USER, data: user };
};

const initialState = { partner: null, guest: null, loading: null, loadingLogin: null, error: null };

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_USER_PARTNER: {
      return { ...state, partner: { name: action.data } };
    }

    case SET_USER_AUTH_START: {
      return { ...state, loading: Process.Start };
    }

    case SET_USER_AUTH_SUCCESS: {
      return { ...state, loading: Process.Success, guest: action.data };
    }

    case SET_USER_AUTH_ERROR: {
      if (action.data === {}) {
        return state;
      }
      return { ...state, loading: Process.Error, guest: false, error: action.data };
    }

    case SET_USER_AUTH: {
      return { ...state, guest: action.data };
    }

    case SET_USER: {
      return { ...state, user: action.data.user };
    }
    case USER_LOGOUT: {
      return { ...state, guest: action.data.guest };
    }
    default:
      return state;
  }
};

reducerRegistry.register("user", reducer);
