import * as React from 'react'
import axios from './axios'
import {Route, Redirect} from 'react-router-dom'
import {setUserId, setUsersList} from "./moduleAdmin/redux/users";
import {connect} from "react-redux";


const NotAccess = (props) => {
  return (
    <div>
      <h1 className="text-danger">Доступ закрыт</h1>
    </div>
  )
}

const Obj = (props) => {
  return (
    <div/>
  );
}

 class RouteAdmin extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      component: Obj
    }
  }

  componentDidMount() {

    if(this.props.router.location.pathname === "/admin" || this.props.router.location.pathname === "/admin/") {
      this.props.history.push('/admin/main')
    }

    axios.post('/admin').then(data => data.data).then(data => {
      if (data.auth === true) {
        const Admin = React.lazy(() => import('./moduleAdmin/Index'))
        this.setState({component: Admin})
      } else {
        this.setState({component: NotAccess})
      }

    })
  }

  render() {

    const Component = this.state.component
    return (
      <Component {...this.props} />
    )
  }
}

const mapStateToProps = (state) => {
  // console.log(state)
  return {
    router: state.router
  };
};


const mapDispatchToProps = (dispatch) => {
  return {
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(RouteAdmin);
