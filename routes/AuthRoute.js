import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import axios from '../axios';
import { setAuthUser } from '../redux/user';
import { isNull } from 'util';

class AuthRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            guest: null
        };
    }

    componentDidMount() {
        this.props.auth();
    }

    componentDidUpdate(nextProps, nextState) {
        if (nextProps.path !== this.props.path) {
            this.props.auth();
        }
    }

    redirect() {
        return <Redirect to="/login" />;
    }

    render() {
        if (this.props.guest !== null && this.props.guest === true) {
            return this.redirect();
        }

        let { canAccess, component, path, name, exact, strict } = this.props;
        let routeProps = {
            path,
            component,
            name,
            exact,
            strict
        };
        if (this.props.guest === false) {
            return (
                <Route {...routeProps} />
            );
        } else {
            return false;
        }

    }
}

const mapState2Props = (state) => {
    return {
        guest: state.user.guest
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        auth: () => dispatch(setAuthUser())
    };
};

export default connect(mapState2Props, mapDispatchToProps)(AuthRoute);
