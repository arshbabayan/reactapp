import * as React from 'react';
import { connect } from 'react-redux';
import { Redirect, Route } from 'react-router-dom';
import axios from 'axios';
import { setAuthUser } from '../redux/user';
import { isNull } from 'util';
import {Process} from "../helpers/types";

class PublicRoute extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            guest: null,
            path: null,
            query: props.query
        };
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        console.log('route nextProps, props', nextProps, this.props);
        return nextProps.guest !== this.props.guest;
    }

    render() {
        console.log('route render');
        let { canAccess, component, path, name, exact, strict } = this.props;
        let routProps = {
            path,
            component,
            name,
            exact,
            strict
        };
        if (this.props.loading === Process.Success) {
            return (
                <Route {...routProps} />
            );
        } else {
            return false;
        }
    }
}

const mapState2Props = (state) => {
    return {
        guest: state.user.guest
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};



export default connect(mapState2Props, mapDispatchToProps)(PublicRoute);
