import React, {PureComponent} from "react";

export default class ButtonMenu extends PureComponent {
  click() {
    $("#left-menu").toggleClass("active");
    $(".container").toggleClass("toggledContainer");
    $(".link-header").toggleClass("removeContent");
    $(".base-footer").toggleClass("toggledFooter");
  }

  render() {
    return (
      <button className="btn btn-outline-light" onClick={this.click}>
        <i style={{fill: "#fff"}} className="oi oi-menu"/>
      </button>
    );
  }
}
