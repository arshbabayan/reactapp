import * as React from "react";
import * as ReactDOM from "react-dom";

export default class Card extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        let style = this.props.style;
        if (this.props.width) {
            style = { ...this.props.style, width: this.props.width };
        }
        return (
            <div style={style} className={'card ' + this.props.className}>
                {this.props.title !== undefined &&
                    <div className='card-header'>{this.props.title}</div>
                }
                <div className='card-body'>{this.props.children}</div>
                {this.props.footer !== undefined &&
                    <div className='card-footer'>{this.props.footer}</div>
                }
            </div>
        );
    }
}
