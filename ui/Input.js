import * as React from "react";
import PropTypes from "prop-types";
import { Input, FormGroup, Label, Col } from "reactstrap";
import { filter as f } from "../helpers/Filter";

class InputGroup extends React.Component {
  constructor(props) {
    super(props);
    this.change = this.change.bind(this);
    this.blur = this.blur.bind(this);
    let msg = props.msg === undefined ? "" : props.msg;
    this.state = {
      error: "",
      borderError: "",
      msg,
      classMsg: "",
      value: props.value
    };
  }

  change(e, name) {
    this.setState({ error: "", borderError: "form-control", value: e.target.value });
    if (this.props.change !== undefined) {
      this.props.change(name, e.target.value);
    }
  }

  blur(e) {
    let { filter } = this.props;
    if (filter !== undefined) {
      let error = f(e.target.value, filter);
      if (typeof error === "string") {
        this.setState({ error: error, borderError: "border border-danger", classMsg: "text-danger" });
      }
    }
  }

  render() {
    return (
      <FormGroup row>
        <Label for={this.props.id} sm="4" size="lg" className="text-md-right">
          {this.props.label}
        </Label>
        <Col sm="6">
          <Input
            onChange={e => this.change(e, this.props.name)}
            type={this.props.type}
            onBlur={this.blur}
            value={this.state.value}
            disabled={this.props.disabled}
            name={this.props.name}
            id={this.props.id}
          />
          <small className="text-danger">{this.state.error}</small>
          <small className={"text-muted " + this.state.classMsg}>{this.state.msg}</small>
        </Col>
      </FormGroup>
    );
  }
}

InputGroup.propTypes = {
  type: PropTypes.string,
  disabled: PropTypes.bool,
  name: PropTypes.string,
  id: PropTypes.string,
  value: PropTypes.string
};

export default InputGroup;
