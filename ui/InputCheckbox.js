import * as React from "react";
import * as ReactDOM from "react-dom";
import { IFilter, filter as f } from "../helpers/Filter";

export default class InputCheckbox extends React.Component{
    constructor(props) {
        super(props);
        let msg = props.msg === undefined ? '' : props.msg;
        this.state = {
            error: '',
            borderError: '',
            msg,
            classMsg: '',
            checked: props.checked
        };
        this.change = this.change.bind(this);
    }

    change(e) {
        this.props.change(this.props.id, !this.state.checked);
        this.setState({ checked: !this.state.checked });
    }

    render() {
        return (
            <div className='custom-control custom-checkbox custom-control-inline'>
                <input checked={this.state.checked}
                    type="checkbox" onChange={this.change}
                    id={this.props.id} name={this.props.id}
                    className="custom-control-input" />
                <label className="custom-control-label" htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        );
    }
}


