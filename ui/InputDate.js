import * as React from "react";
import * as ReactDOM from "react-dom";
import { IFilter, filter as f } from "../helpers/Filter";
import * as $ from 'jquery';
import 'jqueryui';

export default class InputDate extends React.Component{
    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
        this.blur = this.blur.bind(this);
        let msg = props.msg === undefined ? '' : props.msg;
        this.state = {
            error: '',
            borderError: '',
            msg,
            classMsg: '',
            value: props.value
        };

    }

    change(e) {
        this.setState({ error: '', borderError: 'form-control', value: e.target.value });
        if (this.props.change !== undefined) {
            this.props.change(e);
        }
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        if (nextProps.value !== prevState.value) {
            prevState.value = nextProps.value;
            return prevState;
        }
        return null;
    }

    blur(e) {
        let { filter } = this.props;
        if (filter !== undefined) {
            let error = f(e.target.value, filter);
            if (typeof error === 'string') {
                this.setState({ error: error, borderError: 'border border-danger', classMsg: 'text-danger' });
            }
        }
    }

    componentDidMount() {
        $(`#${this.props.id}`).datepicker({
            yearRange: "-90:+0",
            dateFormat: "dd/mm/yy",
            changeMonth: true,
            changeYear: true,
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            monthNamesShort: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб']
        });
    }

    render() {
        return (
            <div className='form-group row'>
                <label htmlFor={this.props.id} className="col-md-4 col-form-label text-md-right">{this.props.label}</label>
                <div className="col-md-3">
                    <input onBlur={this.blur} onChange={this.change}
                        className={'form-control ' + this.state.borderError} id={this.props.id}
                        disabled={this.props.disabled}
                        value={this.state.value}
                        type={this.props.type} name={this.props.name}
                    />
                    {this.state.error !== "" &&
                        <div>
                            <small className='text-danger' role='alert'>
                                {this.state.error}
                            </small>
                        </div>
                    }
                    <small className={'text-muted' + ' ' + this.state.classMsg}>{this.state.msg}</small>
                </div>
            </div>
        );
    }
}


