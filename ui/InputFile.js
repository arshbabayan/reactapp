import * as React from "react";
import * as ReactDOM from "react-dom";
import { IFilter, filter as f } from "../helpers/Filter";
import './InputFile.scss';

export default class InputFile extends React.Component {

    constructor(props) {
        super(props);
        this.change = this.change.bind(this);
        this.blur = this.blur.bind(this);
        let msg = props.msg === undefined ? '' : props.msg;
        this.state = {
            error: '',
            borderError: '',
            msg,
            classMsg: '',
            value: props.value
        };
    }

    change(e) {
        this.setState({ error: '', borderError: 'form-control', value: e.target.value });
        if (this.props.change !== undefined) {
            this.props.change(e.target.value);
        }
    }

    blur(e) {
        let { filter } = this.props;
        if (filter !== undefined) {
            let error = f(e.target.value, filter);
            if (typeof error === 'string') {
                this.setState({ error: error, borderError: 'border border-danger', classMsg: 'text-danger' });
            }
        }
    }

    render() {
        return (

            <div className='custom-file'>
                <input id={this.props.id} onBlur={this.blur} ref={input => this.fileInput = input} name={this.props.name} onChange={this.change}
                    type='file' className={'custom-file-input ' + this.state.borderError} />
                <label className='custom-file-label' htmlFor='customFile'>{this.state.value}</label>

                {this.state.error !== "" &&
                    <div>
                        <small className='text-danger' role='alert'>
                            {this.state.error}
                        </small>
                    </div>
                }
                <small className={'text-muted' + ' ' + this.state.classMsg}>{this.state.msg}</small>
            </div>
        );
    }
}


