import * as React from "react";
import * as ReactDOM from "react-dom";
import { IFilter, filter as f } from "../helpers/Filter";

export default class InputRadio extends React.Component<InputRadioProps, InputRadioState> {
    constructor(props) {
        super(props);
        let msg = props.msg === undefined ? '' : props.msg;
        this.state = {
            error: '',
            borderError: '',
            msg,
            classMsg: ''
        };
    }

    change(e) {
        this.props.change(e);
    }

    render() {
        return (
            <div className='custom-control custom-radio custom-control-inline'>
                <input type="radio" onChange={this.change} id={this.props.id} name={this.props.name} className="custom-control-input" />
                <label className="custom-control-label" htmlFor={this.props.id}>{this.props.label}</label>
            </div>
        );
    }
}
