import * as React from 'react';
import { setPath } from '../helpers/functions';
import { string } from 'prop-types';

export default class Loader extends React.Component {


    render() {
        let width = this.props.width ? { width: this.props.width } : 200 ;
        return (
            <div className={'text-center ' + this.props.className}>
                <div style={{ display: 'inline-block' }}>
                    <img {...width} src={setPath('/img/spinner.svg')} />
                </div>
            </div>

        );
    }
}
