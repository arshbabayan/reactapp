import * as React from 'react';

export default class Modal extends React.PureComponent {
    constructor(props) {
        super(props);
        this.onBtnOkClick = this.onBtnOkClick.bind(this);
        this.onClose = this.onClose.bind(this);
    }

    componentDidMount() {
        $(this.modal).on('hide.bs.modal', this.onClose);
    }

    onBtnOkClick() {
        this.props.onBtnOk();
    }

    onClose() {
        if (typeof this.props.close !== undefined) {
            this.props.close();
        }

    }

    render() {
        return (
            <div ref={modal => this.modal = modal} className="modal fade" id={this.props.id} tabIndex={-1} role="dialog"
                aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title w-100" id="exampleModalLongTitle">{this.props.title}</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        {this.props.isFooter === true &&
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal">Закрыть</button>
                            {this.props.okBtn &&
                            <button type="button" onClick={this.onBtnOkClick} className="btn btn-primary">{this.props.save}</button>
                            }
                        </div>
                        }
                    </div>
                </div>
            </div>
        );
    }
}
