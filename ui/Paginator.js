import * as React from 'react';
import {withRouter, RouteComponentProps} from 'react-router';
import {Link} from 'react-router-dom';

class Paginator extends React.Component {
  constructor(props) {
    super(props);

    this.create = this.create.bind(this);
  }

  create() {
    let pagination = [];
    let curPage = parseInt((this.props.match.params)['page'], 10);
    curPage = isNaN(curPage) ? 1 : curPage;
    let countPages = Math.floor(this.props.count / this.props.limit);
    let counterBegin = curPage < 5 ? 1 : curPage - 4;
    let counterEnd = curPage + 5 >= countPages ? countPages : curPage + 4;
    if (counterBegin > 1) {
      pagination.push(<li key={1} className={'page-item '}>
        <Link className='page-link' to={`${this.props.path}/${1}`}>
          {1}
        </Link></li>);
    }
    for (let i = counterBegin; i <= counterEnd; i++) {
      let addClass = curPage === i ? 'active' : '';
      pagination.push(<li key={i} className={'page-item ' + addClass}>
        <Link className='page-link' to={`${this.props.path}/${i}`}>{i}
        </Link>
      </li>);
    }
    if (counterEnd < countPages) {
      pagination.push(<li key={countPages} className={'page-item '}>
        <Link className='page-link' to={`${this.props.path}/${countPages}`}>
          {countPages}
        </Link></li>);
    }
    console.log('cur page', curPage);
    return pagination;
  }

  render() {
    console.log('paginator props', this.props);
    return (
      <nav>
        <ul className="pagination justify-content-center">
          {this.create()}
        </ul>
      </nav>
    );
  }

}

export default withRouter(Paginator);
