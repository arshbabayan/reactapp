import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { setPathImg } from "../helpers/functions";
import "./Profile.scss";

class Profile extends PureComponent {
  constructor(props) {
    super(props);
    this.click = this.click.bind(this);
  }

  componentDidMount() {
    if (this.props.type !== null) {
      this.img.addEventListener("click", this.click);
    }
  }

  componentWillUnmount() {
    this.img.removeEventListener("click", this.click, false);
  }

  click() {
    // this.props.click(this.props.id);
  }

  render() {
    const img = this.props.img ? setPathImg(this.props.img) : setPathImg("/profile.png");
    let muted = this.props.type === 0 ? "base-acc font-weight-bold" : "text-muted text-success";
    return (
      <div className="d-inline-block profile">
        <div className="avatar-wrap">
          <Link to={`/cabinet/vip/${this.props.id}`}>
            <img ref={img => (this.img = img)} className="notnull" src={img} />
          </Link>
        </div>
        <div className="payments text-primary">
          <strong>{this.props.payments}</strong>
        </div>
        <div className="acc-id text-primary">
          <strong>{this.props.id}</strong>
        </div>
        <div className="user-id text-primary">
          <strong>{this.props.uid}</strong>
        </div>
        <div className={muted}>
          <Link to={`/socnet/profile/info/${this.props.uid}`}>{this.props.name}</Link>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  img: PropTypes.string,
  name: PropTypes.string,
  type: PropTypes.number,
  id: PropTypes.number,
  click: PropTypes.func
};

export default Profile;
