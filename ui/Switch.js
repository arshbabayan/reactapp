import * as React from "react";
import "./Switch.scss";

export default class Switch extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: props.checked
        };
        this.change = this.change.bind(this);
    }

    // static getDerivedStateFromProps(nextProps: SwitchProps, prevState) {
    //     console.log()
    //     if (nextProps.checked !== prevState.checked) {
    //         prevState.checked = nextProps.checked;
    //         return prevState;
    //     }
    //     return false;
    // }

    change() {
        console.log('change switch');
        if (this.props.change !== undefined) {
            this.props.change(!this.state.checked);
        }
        this.setState((state) => {
            state.checked = !state.checked;
            return state;
        });
    }

    render() {
        console.log()
        return (
            <label className="switch">
                <input checked={this.state.checked} onChange={this.change} type="checkbox" />
                <span className="slider" />
            </label>
        );
    }
}
