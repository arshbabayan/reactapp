import * as React from "react";
import * as _ from 'lodash';
import { setPathImg } from "../helpers/functions";

export default class TableObject extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: props.data,
            sort: {}
        };
        this.sort = this.sort.bind(this);
        this.renderArrow = this.renderArrow.bind(this);
    }

    componentDidMount() {
        let self = this;
        if (this.props.sort === true) {
            $(this.head).find('th').each(function() {
                $(this).css({ cursor: 'pointer' });
                $(this).click(() => {
                    self.sort($(this).data('name'));
                    console.log('click header', $(this).data('name'));
                });
            });
        }
    }

    sort(name) {
        let data = this.state.data;
        let sort = (this.state.sort[name] === undefined) ? -1 : this.state.sort[name];
        data.sort((a, b) => {
            if (a[name] < b[name]) {
                return sort;
            }
            if (a[name] > b[name]) {
                return -sort;
            }
            return 0;
        });
        let newSort = { ...this.state.sort, [name]: -sort };
        this.setState({ data, sort: newSort });
    }

    renderArrow(name) {
        if (this.props.sort === false) {
            return null;
        }
        if (this.state.sort[name] === undefined || this.state.sort[name] === -1) {
            return <img src={setPathImg('/icons/arrow_up.svg')} />;
        } else {
            return <img src={setPathImg('/icons/arrow_down.svg')} />;
        }
    }

    render() {
        return (
            <table className='table'>
                <thead className='' ref={head => this.head = head}>
                    <tr>
                        {Object.keys(this.props.labels).map((item, key) => {
                            return (
                                <th data-name={item} key={key}>
                                    {this.props.labels[item]}
                                    {this.renderArrow(item)}
                                </th>
                            );
                        })}
                    </tr>
                </thead>
                <tbody>
                    {this.props.data.map((item, key) => {
                        return (
                            <tr key={key}>
                                {Object.keys(this.props.labels).map((label, key) => {
                                    return (
                                        <td key={key}>{item[label]}</td>
                                    );
                                })}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        );
    }
}
