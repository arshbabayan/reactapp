
import * as React from "react";
import * as ReactDOM from "react-dom";

export default class MenuItemHeader extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div className='link-header '>
        <a data-toggle='collapse' data-target={'#collapse' + this.props.id}
           aria-expanded="true" aria-controls={'collapse' + this.props.id}>
          <i className={`${this.props.icon}`} />
          <span>{this.props.title}</span>
        </a>
      </div>
    );
  }
}

