import * as React from "react";
import * as ReactDOM from "react-dom";
import {Link} from "react-router-dom";

export default class MenuItemsContainer extends React.Component {
  constructor(props) {
    super(props);

  }

  render() {
    return (
      <div id={'collapse' + this.props.id} className='collapse' aria-labelledby={'heading' + this.props.id}
           data-parent='#accordion'>
        <div className='link-body'>
          {this.props.children}
        </div>
      </div>
    );
  }
}
