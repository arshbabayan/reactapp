import * as React from "react";

export default class TabContent extends React.Component {
    render() {
        const active = this.props.active === true ? 'show active' : '';
        return (
            <div className={`tab-pane ${active}`} id={this.props.id} role='tabpanel'>
                {this.props.children}
            </div>
        );
    }
}
