import * as React from "react";
import TabContent from "./TabContent";

export default class Tabs extends React.Component {
    render() {
        return (
            <>
                <ul className='nav nav-tabs' id={this.props.id} role='tablist'>
                    {this.props.tabs.map((item, key) => {
                        let active = key === 0 ? 'active' : '';
                        let selected = key === 0 ? 'true' : 'false';
                        return (
                            <li className='nav-item' key={key}>
                                <a className={`nav-link ${active}`}
                                    data-toggle="tab"
                                    id={`${item.id}-tab`}
                                    href={`#${item.id}`}
                                    area-selected={selected}
                                    role='tab' aria-selected={true}>
                                    {item.title}
                                </a>
                            </li>
                        );
                    })}
                </ul>
                <div className='tab-content'>
                    {this.props.content}
                </div>
            </>
        );
    }
}
